<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Clients']?></title>
    <?php $this->load->view('common/headerscript');?>
</head>
<body class="no-skin-config">
<div class="loader"></div>
    <div id="wrapper">   
        <div id="page-wrapper" class="gray-bg">  
        <div class="row border-bottom"></div>      
            <div class="row wrapper line_buttom white-bg">
                <div class="col-lg-10">
                    <h2><?php echo $this->session->userdata('languageArray')['Clients']?></h2>
                </div>
                <div class="col-lg-2" style="vertical-align:bottom;">
                <h2>					
                <a class="btn btn-primary" href="<?php echo base_url();?>Menus/client_details"><?php echo $this->session->userdata('languageArray')['Add a new client']?></a></h2>
                </div>
            </div>           
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">                  
                    <div class="ibox-content">
                    <table class="table table-bordered table-hover dataTables-example" >          
                    <thead>
                    <tr>
                        <th><?php echo $this->session->userdata('languageArray')['Organization']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Contact Person']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Email']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Phone No']?></th>                        
                        <th><?php echo $this->session->userdata('languageArray')['Actions']?></th>
                    </tr>
                    </thead>
                    <tbody>
                   <?php foreach($results as $records){?>
                    <tr>
                        <td><?php echo $records->organization_name;?></td>
                        <td><?php echo $records->contact_person ;?></td>
                        <td><?php echo $records->email ;?></td>
                        <td class="center"><?php echo $records->phone_no;?></td>                        
                        <td>                         
                         <a href="<?php echo base_url();?>Menus/view_client_by_id/<?php echo $records->client_id ;?>"><button type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit" aria-label="Left Align">
  							<span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></a>
                         <a href="<?php echo base_url();?>Menus/view_client_by_id/<?php echo $records->client_id ;?>/view"><button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="View" aria-label="Left Align">
                            <span class="fa fa-eye" aria-hidden="true"></span></button></a>
						<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="deleteClient('<?php echo $records->client_id ;?>');" aria-label="right Align">
 						<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
						</button>						
						</td>
                    </tr>
                  <?php }?>  
                    </tbody>                   
                    </table>
                    </div>
                </div>
            </div>
            </div>     
        </div>
    </div>
<?php $this->load->view('common/footerscript');?>
<script type="text/javascript">
    function deleteClient(name)
    {       
        var result=confirm("Do you really want to delete the client ?");
        if(result==true)
            {
            location.href="<?php echo base_url();?>Menus/delete_client/"+name;
            }
        else
            {
                return false;
            }
        return;
    }
</script>
</body>
</html>