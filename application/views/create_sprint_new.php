<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>SCRUM | Create Sprint</title>
<?php //$this->load->view('common/headerscript');?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" media="screen"/>
<link href="<?php echo base_url();?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/Angular/css/style.css" media="screen"/>
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<!-- Data Tables -->
<link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/steps/jquery.steps.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
</head> 

<body class="no-skin-config">
    <div id="wrapper">
    <div id="page-wrapper" class="gray-bg"> 
    <div class="row border-bottom"></div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>Create Sprint</h2>
            </div>                
        </div>        
        <?php include 'projectmenu.php';?>       
            <div class="col-lg-12 animated fadeInRight">
            <div class="ibox-content">
            <?php 
            if ($error_count==TRUE)
            {
            ?>
            <div class="alert alert-danger">
                Sprint already exists for the time frame
            </div>
            <?php }?>
            <form class="form-horizontal" id="form-horizontal" role="form" method="POST" action="<?php echo base_url();?>Scrum/create_sprints">
                <?php 
                    $csrf = array(
            			'name' => $this->security->get_csrf_token_name(),
            			'hash' => $this->security->get_csrf_hash()
    				);
                ?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>"/>
                <div class="form-group">
                <label class="col-lg-4 control-label">Sprint Id *</label>
                	<div class="col-lg-6">
                	<?php 
	                    foreach($sprintId as $sprint){?>
                    <input type="sprint_id" name="sprint_id" class="form-control" value="<?php echo ($sprint->sprint_id+1) ;?>" required="required"/>
                    <?php }?>
                    </div>
                </div>           
                <div class="form-group">
                <label class="col-lg-4 control-label">Sprint Title *</label>
                	<div class="col-lg-6">
                    <input type="title" name="title" class="form-control" required="required"/>
                    </div>
                </div>               
              <!--   <div class="form-group">
                    <label class="col-lg-4 control-label">Select Issues/ Stories to be included in the sprint</label>
                	<div class="col-xs-3 col-md-3 col-sm-3 col-lg-3">           
                        <select id="sd" multiple="multiple" class="sortable-list form-control" name="issues[]">
                            <option value="">--Drag Issues/Stories Here--</option>
                        </select>                    
                    </div> 
                    <div class="col-xs-3 col-md-3 col-sm-3 col-lg-3 white-bg">          
                        <ul class="sortable-list agile-list"> -->
                        <?php
                        // print_r($results);                                           
                      //  foreach($issueList as $records){
                        ?>
                           <!--  <li id="<?php // echo $records->issue_id;?>">
                            <a href="#"><?php // echo $records->summary; ?></a>                
                            </li> -->                                       
                        <?php
                          // }               
                        ?>                                               
                        </ul>                    
                    </div> 
                </div>                         
                <div class="form-group" id="data_1">
                    <label class="col-lg-4 control-label">Start Date *</label>
                	<div class="input-group date col-lg-6">
                	 	<input id="stdate" type="stdate" name="stdate" class="form-control" required="required"/>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>                   
                </div>
                <div class="form-group" id="data_1">
                    <label class="col-lg-4 control-label">End Date *</label>
                	<div class="input-group date col-lg-6">
                	  <input id="enddate" name="enddate" class="form-control" required="required">
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>                                       
                </div>   
                
                <div class="form-group" id="data_1">
                    
                	<div class="input-group date col-lg-10">
                	  <input type="radio" name="actionchoice1" class="form-control" group="group1" required="required">
                	  <input type="radio" name="actionchoice2" class="form-control" group="group1" required="required">
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>                                       
                </div>   
                
                                            
                <div class="form-group form-controlcenter">                
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-primary">Cancel</button>                 
                </div>       
            </form>
        </div>                
        </div>
        </div>
        <div class="footer"></div>
        </div>
        </div>   
       <!-- Mainly scripts -->
<script src="<?php echo base_url();?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="<?php echo base_url();?>assets/js/SCRUM.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.tableTools.min.js"></script>
<!--Drag drop-->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui-1.8.custom.min.js"></script>

<script src="<?php echo base_url();?>jassets/Angular/js/plugins/ui-sortable/sortable.js"></script>

<script src="<?php echo base_url();?>assets/js/datepickr.min.js"></script>
<!-- Data picker -->
<script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script> 
 $('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
</script>
<script>            
    datepickr('.from-calendar-icon',{ altInput: document.getElementById('enddate') }); 
    datepickr('[title="parseMe"]');          
</script>  
<script>            
   datepickr('.from-calendar-icon',{altInput: document.getElementById('startdate')});
   datepickr('.to-calendar-icon',{altInput: document.getElementById('enddate') });      
   datepickr('[title="parseMe"]');           
</script> 
<!-- Example jQuery code (JavaScript)  -->
<script type="text/javascript">
$(document).ready(function(){
    $('#page-wrapper .sortable-list').sortable({
        connectWith: '#page-wrapper .sortable-list',
        placeholder: 'placeholder',
    });
});
var $sortable = $(".sortable-list");  
    $sortable.sortable({    
        stop: function (event, ui){         
        issue_id = ui.item.closest('li').attr('id');                    
        issue_name = ui.item.closest('li').text();                 
        //alert(issue_id);     
       // alert(issue_name);     
        $("#sd").append('<option value="'+issue_id+'" selected="selected">'+issue_name+'</option>'); 
        $("select#sd li:contains('"+issue_name+"')").remove();
        }
    }); 
</script>   
</body>
</html>