<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Retrospective']?> </title>
    <?php $this->load->view('common/headerscript');?>
</head>
<body class="no-skin-config">
	<!--<div class="loader"></div>-->
	<div id="wrapper">
		<div id="page-wrapper" class="gray-bg">
			<div class="row border-bottom"></div>
 <?php include 'projectmenu.php';?> 
			<div class="row wrapper line_buttom white-bg">
				<div class="col-lg-12">
					<h2><?php echo $this->session->userdata('languageArray')['Retrospective']?></h2>
				</div>
			</div>
			<div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 white-bg">
				
				
				<h3><span class="label label-primary"><i class="fa fa-hand-o-up"></i> </span> &nbsp;&nbsp;<?php echo $this->session->userdata('languageArray')['What went well']?></h3>
				
				
				<p class="small">
					<button id="goodadd" class="btn btn-default btn-block m-t"><?php echo $this->session->userdata('languageArray')['Add More']?></button>
				</p>
			</div>
			<div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 white-bg">
				<h3><span class="label label-danger"><i class="fa fa-hand-o-up"></i> </span> &nbsp;&nbsp;<?php echo $this->session->userdata('languageArray')['Needs Improvement']?></h3>
				
				<p class="small">
					<button id="improvadd" class="btn btn-default btn-block m-t"><?php echo $this->session->userdata('languageArray')['Add More']?></button>
				</p>
			</div>
			<div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 white-bg">
				<h3><span class="label label-success"><i class="fa fa-hand-o-up"></i> </span> &nbsp;&nbsp;<?php echo $this->session->userdata('languageArray')['Action Items']?> </h3>
				
				<p class="small">
					<button id="actionadd" class="btn btn-default btn-block m-t"><?php echo $this->session->userdata('languageArray')['Add More']?></button>
				</p>
			</div>

			<div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 white-bg">
				<div id="good-texta" align="right">
					<textarea class="form-control"
						style="resize: none; overflow: hidden; border: 3px solid #1ab394"
						onkeyup="resizeTextarea('InputTextArea0')" id="InputTextArea0"></textarea>
					<button id="submitgood" class="btn btn-info btn-circle" onclick="submitGoodFunction('0','1','InputTextArea')"
						type="button">
						<i class="fa fa-check"></i>
					</button>
					<button id="deletegood" class="btn btn-danger btn-circle" 
						type="button">
						<i class="fa fa-trash"></i>
					</button>
				</div>
				<div id="goodPointsListStart"></div>
				<ul class="sortable-list agile-list" id="goodPointsList"
					style="height: 100%;">
           
           
          
           <?php $this->load->view('view_good_retro');?>
        
                                                     
			</ul>
				<div id="goodPointsListEnd"></div>
			</div>
			<div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 white-bg">

				<div id="improv-textb" align="right">
					<textarea class="has-error form-control "
						style="resize: none; overflow: hidden; border: 3px solid #ec4758"
						onkeyup="resizeTextarea('InputTextAreaImprov0')"
						id="InputTextAreaImprov0"></textarea>
					<button id="submitimprov" class="btn btn-info btn-circle"  onclick="submitGoodFunction('0','2','InputTextAreaImprov')"
						type="button">
						<i class="fa fa-check"></i>
					</button>
					<button id="deleteimprov" class="btn btn-danger btn-circle"
						type="button">
						<i class="fa fa-trash"></i>
					</button>
				</div>
				<div id="improvPointsListStart"></div>
				<ul class="sortable-list agile-list" id="improvementPointList"
					style="height: 100%;">
           
          
           <?php $this->load->view('view_improvement_retro');?>
           </ul>
				<div id="improvPointsListEnd"></div>
			</div>
             
            
            <div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 white-bg">

				<div id="action-textc" align="right">
					<textarea class="form-control"
						style="resize: none; overflow: hidden; border: 3px solid #1A7BB9"
						onkeyup="resizeTextarea('InputTextAreaAction0')"
						id="InputTextAreaAction0"></textarea>
					<button id="submitaction" class="btn btn-info btn-circle" onclick="submitGoodFunction('0','3','InputTextAreaAction')"
						type="button">
						<i class="fa fa-check"></i>
					</button>
					<button id="deleteaction" class="btn btn-danger btn-circle"
						type="button">
						<i class="fa fa-trash"></i>
					</button>
				</div>
				<div id="actionPointsListStart"></div>
				<ul class="sortable-list agile-list" id="actionPointList"
					style="height: 100%;">
           
           
          
           <?php $this->load->view('view_action_retro');?>
           </ul>
				<div id="actionPointsListEnd"></div>
			</div> 
             
            

		</div>
	</div>
	<!-- Mainly scripts -->
<?php $this->load->view('common/footerscript');?>
<!--Drag drop-->
	 <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"   integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="   crossorigin="anonymous"></script>
	<!-- Example jQuery code (JavaScript)  -->
	<script type="text/javascript">
	function toggleTextArea(textid,id)
	{
		 var x = document.getElementById(textid);
		 var y = document.getElementById(id);
		    x.style.display = "block";
		    y.style.display = "none";
	

		}
	
	
function resizeTextarea (id) {
	  var a = document.getElementById(id);
	  a.style.height = 'auto';
	  a.style.height = a.scrollHeight+'px';
	}

$(document).ready(function(){

	 $("#good-texta").hide();
	 
	  $("#deletegood").click(function(){
		$('#InputTextArea0').val('');
	    $("#good-texta").hide();
	  });
	  $("#goodadd").click(function(){

		    $("#good-texta").show();
	  });


	  $("#improv-textb").hide();
		 
	  $("#deleteimprov").click(function(){
		$('#InputTextAreaImprov0').val('');
	    $("#improv-textb").hide();
	  });
	  $("#improvadd").click(function(){

		    $("#improv-textb").show();
	  });


	  $("#action-textc").hide();
		 
	  $("#deleteaction").click(function(){
		$('#InputTextAreaAction0').val('');
	    $("#action-textc").hide();
	  });
	  $("#actionadd").click(function(){

		    $("#action-textc").show();
	  });
	


	
	$('#page-wrapper .sortable-list').sortable({
		connectWith: '#page-wrapper .sortable-list',
		placeholder: 'placeholdersd',
	});
});

var $sortable = $(".sortable-list");     	            
//console.log($sortable);
  	$sortable.sortable({  	
    	stop: function (event, ui){      	
      	status = ui.item.closest('ul').attr('id');
      	issue_id = ui.item.closest('div').attr('id'); 
      	var p = $('#'+issue_id);
      	var position = p.offset();
		var topposition=position.top;
	 	//alert("status="+status);
	 	 var count=0;
	 	 var divs='';
	 	 var divPosition='';
	 	 var markerVar='';
	 	
     // 	var order = $("ul#goodPointsList").sortable("serialize");
      	$('ul.sortable-list.agile-list.ui-sortable').each(function (){
      		var j = $(this).attr('id');

      		
      		});
      		
      	 $('div').each(function () {
      		var divId = $(this).attr('id');
      		if(divId !='')
            {
			//alert(divId);
         	
         	if(status =='goodPointsList')
         	{
         	 
                 	if(divId == 'goodPointsListEnd')
                 	 {
                		return false;
                 	 }
        
                 	 if(markerVar == 'goodPointsListStart' )
                  	 {
                     
                        divs=divs+divId+'~';
                        divPosition=divPosition+count+'~';
                        
                     
                  	 } 
                  	        
                 count++;
                 if(divId == 'goodPointsListStart')
              	 {
             		markerVar='goodPointsListStart';
              	 }
                 
         	} 
         	else if(status =='improvementPointList')
         	{

                     		if( divId == 'improvPointsListEnd')
                        	 {
                       		return false;
                        	 }
               
                        	 if( markerVar == 'improvPointsListStart')
                         	 {
                            
                               divs=divs+divId+'~';
                               divPosition=divPosition+count+'~';
                            
                         	 } 
                         	        
                        count++;
                        if(divId == 'improvPointsListStart')
                        {
                       	 markerVar='improvPointsListStart';
               
                            }
                	}         
         	else if(status =='actionPointList')
         	{
         		//alert('divs=='+divs+"======="+divPosition);
                     		if( divId == 'actionPointsListEnd')
                        	 {
                       		return false;
                        	 }
               
                        	 if( markerVar == 'actionPointsListStart')
                         	 {
                            
                               divs=divs+divId+'~';
                               divPosition=divPosition+count+'~';
                            
                         	 } 
                         	        
                        count++;
                        if(divId == 'actionPointsListStart')
                        {
                       	 markerVar='actionPointsListStart';
               
                            }
                	}      

             	}        
         
            
      	});
     
      

        var url = "<?php echo site_url('Scrum/update_retro_status');?>"; 
       
         	$.ajax({
                type: "post",
                url: url,
                datatype:'json',
                data: {issue_id:issue_id,status:status,divids:divs,divposition:divPosition},
                success: function(data) {
                	
                       if(status =='goodPointsList')
                    {
				       	 $('#InputTextArea0').val('');
                         $('#goodPointsList').fadeIn().html(data);
                         $('#good-texta').hide();
                    }
                    else if(status =='improvementPointList')  
                    {
                    	 $('#InputTextAreaImprov0').val('');
                         $('#improvementPointList').fadeIn().html(data);
                         $('#improv-textb').hide();
                    }
                    else if(status =='actionPointList')  
                    {
						
                   	 	$('#InputTextAreaAction0').val('');
                         $('#actionPointList').fadeIn().html(data);
                         $('#action-textc').hide();
                    }        
                    
                }
                
        	});  
    	}
	}); 

  	
  
	  
  	function submitGoodFunction(id,retro_type,textareaid) {
//alert("edit"+retro_type);
  	  	//	alert( $(this).attr("id"));
  	
  	var descText=document.getElementById(textareaid+id).value;
	//alert(descText);
     // var id = $('#submit-buttons').val();
       var url = "<?php echo site_url('Scrum/update_good_retro');?>"; 

      	
        $.ajax({
        	 url: url,
        	 async: false,
             type: "POST",
             data: {issue_id:id,retro_type_id:retro_type,InputTextArea: descText },
             dataType: "html",
             success: function(data) {
            	 $('#'+textareaid+id).val('');

            	 if(retro_type == '1')
            	 {
            		
                   $('#goodPointsList').fadeIn().html(data);
                   $('#good-texta').hide();
            	 }
            	 else  if(retro_type == '2')
            	 {
            		
                     $('#improvementPointList').fadeIn().html(data);
                     $('#improv-textb').hide();
              	 }
            	 else  if(retro_type == '3')
            	 {
                     $('#actionPointList').fadeIn().html(data);
                     $('#action-textc').hide();
              	 }
             }
 		})
   }

  	function deleteGoodFunction(id,retro_type)
  	{

  	var confirmDelete=confirm("Are you sure you want to delete ?");
  	if(confirmDelete)
  	{
  	     // var id = $('#submit-buttons').val();
  	       var url = "<?php echo site_url('Scrum/delete_retro');?>"; 

  	      	
  	        $.ajax({
  	        	 url: url,
  	        	 async: false,
  	             type: "POST",
  	             data: {retro_id:id,retro_type_id:retro_type },
  	             dataType: "html",
  	             success: function(data) {
  	            	 if(retro_type == '1')
  	            	 {
  	            		$('#InputTextArea'+id).val('');
    	               $('#goodPointsList').fadeIn().html(data);
  	                   $('#good-texta').hide();
  	            	 }
  	            	 else  if(retro_type == '2')
  	            	 {
  	            		 $('#InputTextAreaImprov'+id).val('');

  	                     $('#improvementPointList').fadeIn().html(data);
  	                     $('#improv-textb').hide();
  	              	 }
  	            	 else  if(retro_type == '3')
  	            	 {
  	            		$('#InputTextAreaAction'+id).val('');
  	                     $('#actionPointList').fadeIn().html(data);
  	                     $('#action-textc').hide();
  	              	 }


 	  	            	
  	             }
  	 		})

  	      }	
  	  	}
  	
	$('#submitimprov').click(function () {
       var url = "<?php echo site_url('Scrum/update_improv_retro');?>"; 
        $.ajax({
        	 url: url,
        	 async: false,
             type: "POST",
             data: { InputTextAreaImprov: InputTextAreaImprov.value },
             dataType: "html",
             success: function(data) {
            	 $('#InputTextAreaImprov').val('');
               $('#improvementPointList').fadeIn().html(data);
               
               $('#improv-textb').hide();
             }
 		})
   });
	
</script>
</body>
</html>