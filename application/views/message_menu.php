<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">                
                <div class="form-group form-controlcenter">                                        
                    <a class="btn btn-primary" href="<?php echo base_url();?>Menus/add_messages"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><?php echo $this->session->userdata('languageArray')['Compose']?>  </a>
                    <a class="btn btn-primary" href="<?php echo base_url();?>Menus/messages"><i class="fa fa-envelope-o"></i><?php echo $this->session->userdata('languageArray')['Inbox']?>  </a>
                    <a class="btn btn-primary" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/stories"><i class="fa fa-share-square-o" aria-hidden="true"></i>
                     <?php echo $this->session->userdata('languageArray')['Sent mail']?> </a> 
                    <a class="btn btn-primary" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/sprint"><i class="fa fa-trash" aria-hidden="true"></i><?php echo $this->session->userdata('languageArray')['Trash']?> </a>
                </div>                  
            </div>
        </div>