<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Add User']?> </title>
<?php $this->load->view('common/headerscript');?>
</head>
<body>
<div class="loader"></div>
	<div id="wrapper">
		<div id="page-wrapper" class="gray-bg">	]
		<div class="row border-bottom"></div>		
			<div class="row wrapper line_buttom white-bg page-heading">
				<div class="col-lg-12">
					<h2><?php echo $this->session->userdata('languageArray')['Add User']?></h2>
				</div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-12 animated fadeInRight">
						<div class="ibox-content">
							<form class="form-horizontal" id="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="<?php echo base_url();?>Projects/save_user">
								<div class="form-group">
									<label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Name']?> *</label>
									<div class="col-lg-6">
										<input type="fullname" name="fullname" class="form-control"
											required="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['User Name']?> *</label>
									<div class="col-lg-6">
										<input type="username" name="username" class="form-control"
											required="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Password']?> *</label>
									<div class="col-lg-6">
										<input type="password" name="password" class="form-control"
											required="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Organization']?> *</label>
									<div class="col-lg-6">
										<input type="organization" name="organization"
											class="form-control" required="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Email']?></label>
									<div class="col-lg-6">
										<input type="email" name="email" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Role']?> *</label>
									<div class="col-lg-6">
										<select name="role" id="role" class="form-control" onchange="ViewClient()" required="required">
											<option value="">--Select--</option>
											<option value="1">Admin</option>
											<option value="3">Client</option>
											<option value="2">User</option>
										</select>
									</div>
								</div>
								<div class="form-group" id="clientiddiv" style="display: none;">
									<label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Client Name']?></label>
									<div class="col-lg-6">
									<select name="clientid" class="form-control">
									<option value="">--Select--</option>
                   					<?php foreach ( $clients as $clientdetails ) { ?> 
		                    			<option value="<?php echo $clientdetails->client_id?>"> <?php echo $clientdetails->organization_name?> </option>
		                    		<?php }?>		                    
	              	 				</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Photo']?></label>
									<div class="col-lg-6">
										<input type="file" name="userfile" class="form-control">
									</div>
								</div>
								<?php 
								$csrf = array(
								    'name' => $this->security->get_csrf_token_name(),
								    'hash' => $this->security->get_csrf_hash()
								);
								?>
								<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />							
				                <div class="form-group form-controlcenter">                
				                    <button type="submit" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Submit']?></button>
				                    <button type="reset" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Cancel']?></button>  
				                </div> 				               
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php $this->load->view('common/footerscript');?>
</body>
</html>