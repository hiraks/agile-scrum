<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php echo $this->session->userdata('languageArray')['Invoice Details']?> </title>
<?php $this->load->view('common/headerscript');?>  
<script type="text/javascript">
function total()
{
	var totalPrice=(document.getElementById("quantity").value)*(document.getElementById("unitprice").value);
  var tax_amt = totalPrice*((document.getElementById("tax_k").value)/100);
  var gtotal=totalPrice+tax_amt;
  document.getElementById("totalprice").value=gtotal.toFixed(2);
	document.getElementById("tax_amt").value=tax_amt.toFixed(2);
}
</script>
</head>
<body  class="no-skin-config">
<div class="loader"></div>
    <div id="wrapper">    
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom"></div>
          <div class="row wrapper line_buttom white-bg page-heading">
                <div class="col-lg-10">
                     <?php 
                     //echo $mode;
                     //dumpEx($data);
                     if($mode=="edit"){ ?>
                    <h2><?php echo $this->session->userdata('languageArray')['Edit Invoices']?></h2>
                    <?php } else{?>
                     <h2><?php echo $this->session->userdata('languageArray')['View Invoice']?></h2>
                     <?php }?>
                </div>
                <div class="col-lg-2"></div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">           
          <div class="row">           
            <div class="col-lg-12 animated fadeInRight">
            <div class="ibox-content">
              <div class="row">
                <div class="col-sm-6">
                    <h5>From:</h5>
                    <address>
                      <?php foreach($company as $companyInfo){?>
                      <img alt="image" class="img-circle-bs" src="<?php echo(base_url()."./uploads/".$companyInfo->logo);?>" /><br/>
                      <strong><?php echo($companyInfo->company_name);?></strong><br>
                      <?php
                       $address=explode("," , $companyInfo->company_address);
                      for($count=0;$count<sizeof($address);$count++)
                      {
                       echo($address[$count]);?><br>
                      <?php }?>                      
                      <strong>Ph:- </strong> <?php echo($companyInfo->phone);?>
                      <?php }?>
                    </address>
                </div>
                <div class="col-sm-6 text-right">
                <address> 
                <?php foreach($client as $clientInfo){?>
                <img class="img-circle-bs" src="<?php echo(base_url().UPLOAD_PATH_URL."client/".$clientInfo->c_logo);?>"/><br/>
                    <h4>Invoice No. :- <span class="text-navy"> INV-0<?php echo $invoiceId ;?></span></h4>                                 
                      <strong>To:- <?php echo($clientInfo->organization_name);?></strong><br> 
                    <?php
                     $address=explode("," , $clientInfo->address);
                    for($count=0;$count<sizeof($address);$count++)
                    {
                     echo($address[$count]);?>
                     <?php }?>                                
                      <strong>Ph:- </strong> <?php echo $clientInfo->phone_no ;?><br/>
                    <?php }?>                                    
                      <?php foreach($invoiceDates as $invoiceInfoDates){?>
                      <strong>Invoice Date:</strong> <?php echo $invoiceInfoDates->created_on?><br/>
                      <strong>Due Date:</strong> <?php echo $invoiceInfoDates->due_date?>
                      <?php }?> 
                      </address>
                </div>
              </div>
              <div class="table-responsive m-t">
                  <table border="1" class="table invoice-table pretty">
                      <thead>
                      <tr>
                          <th><?php echo $this->session->userdata('languageArray')['Item List']?></th>
                          <th><?php echo $this->session->userdata('languageArray')['Quantity']?></th>
                          <th><?php echo $this->session->userdata('languageArray')['Unit Price']?></th>
                          <th><?php echo $this->session->userdata('languageArray')['Tax(%)']?></th>
                          <th><?php echo $this->session->userdata('languageArray')['Tax Amount']?></th>                          
                          <th><?php echo $this->session->userdata('languageArray')['Total Price']?></th>
                          <th><?php echo $this->session->userdata('languageArray')['Action']?></th>
                      </tr>
                      </thead>
                      <tbody>
                   <?php foreach($invoice as $invoiceInfo){?>                          
                      <tr>
                        <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url();?>Projects/delete_invoice_row">
                          <td><div><?php echo($invoiceInfo->items);?></div>
                          <td><?php echo($invoiceInfo->Quantity);?></td>
                          <td><?php echo number_format(($invoiceInfo->unit_price),2);?></td>
                          <td>                          
                            <div class="col-lg-12">                              
                            <select class="form-control" name="tax_id">
                              <option value="">--Select Tax--</option>
                              <?php
                                foreach ($tax as $td) {
                              ?>
                                <option value="<?php echo $td->tax_id;?>" <?php echo ($invoiceInfo->tax_id==$td->tax_id)?'selected="selected"':'' ?>><?php echo $td->tax_name."[".$td->tax_percentage."]";?></option>                
                             <?php
                                }
                              ?> 
                            </select> 
                              <?php
                                foreach ($tax as $td) {
                              ?>
                                <input type="hidden" value="<?php echo $td->tax_percentage?>" id="taxp_<?php echo $td->tax_id?>"/>              
                             <?php
                                }
                              ?>                               
                          </div> 
                          </td>                          
                          <td><?php echo number_format(($invoiceInfo->Quantity*$invoiceInfo->unit_price)*($invoiceInfo->tax/100),2);?></td>
                          <td><?php echo number_format(($invoiceInfo->row_total),2);?></td>

                          <?php  if($mode=="edit"){ ?>
                          <td>
                          <?php 
                            $csrf = array(
                  					'name' => $this->security->get_csrf_token_name(),
                  					'hash' => $this->security->get_csrf_hash()
          				        );
                          ?>
				                  <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>"/>
                          <button type="submit" class="btn btn-default" aria-label="Left Align" title="Delete" data-placement="bottom" data-toggle="tooltip" type="button">
                          <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                          </button>
                          </td>
                          <?php }?>                          
                          <input type="hidden" name="invoice_list_id" value="<?php echo $invoiceInfo->list_id ;?>"/>
                          <input type="hidden" name="invoice_id" value="<?php echo $invoiceId ;?>"/>
                        </form>
                      </tr>
                     <?php }?>
                     <?php 
                     if($mode=="edit"){ ?>
                      <tr>
                      <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url();?>Projects/create_invoice_details">  
                        <td><div class="col-lg-13"><input type="text" class="form-control" name="itemname" id="itemname" required="required"/></div></td>
                        <td align="right"><div class="col-lg-10"><input type="text" class="form-control" name="quantity" id="quantity" required="required" onblur="total()" onkeyup="total()"/></div></td>
                        <td align="right"><div class="col-lg-10"><input type="text" class="form-control" name="unitprice" id="unitprice" required="required" onblur="total()" onkeyup="total()"/></div></td>
                        <?php foreach($company as $invoiceInfoDates){?>
                        <td align="right">
                          <div class="col-lg-8">                              
                            <select class="form-control" name="tax_id" id="tax_id_k">
                              <option value="">--Select Tax--</option>
                              <?php
                                foreach ($tax as $td) {
                              ?>
                                <option value="<?php echo $td->tax_id;?>"><?php echo $td->tax_name."[".$td->tax_percentage."]";?></option>                
                             <?php
                                }
                              ?> 
                            </select> 
                              <?php
                                foreach ($tax as $td) {
                              ?>
                                <input type="hidden" value="<?php echo $td->tax_percentage?>" id="taxp_<?php echo $td->tax_id?>"/>              
                             <?php
                                }
                              ?>                               
                          </div>                           
                          <div class="col-lg-4"><input type="text" class="form-control" name="tax" id="tax_k" onblur="total()" onkeyup="total()" readonly="readonly"/></div></td>
                          <td><input type="text" class="form-control" name="tax_amt" id="tax_amt"/></td>
                        <?php }?>
                        <td align="right"><div class="col-lg-10"><input type="text" class="form-control" name="totalprice" id="totalprice" readonly="readonly"/></div></td>
                        <?php 
                          $csrf = array(
                  					'name' => $this->security->get_csrf_token_name(),
                  					'hash' => $this->security->get_csrf_hash()
                				  );
                        ?>
			                  <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                        <td><button type="submit" class="btn btn-default" aria-label="Left Align" title="Delete" data-placement="bottom" data-toggle="tooltip" type="button">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                        </button></td>                                        
                        <input type="hidden" name="invoice_id" value="<?php echo $invoiceId ;?>"/>
                      <?php }?>     
                    </form>
                    </tr>                                 
                  </tbody>
                </table>
              </div><!-- /table-responsive -->
              <table class="table invoice-total">
                <tbody>
                  <tr>
                    <td><strong><?php echo $this->session->userdata('languageArray')['Total Price']?>  :</strong></td>
                    <?php  
                      $totalAmt=0;
                      foreach($invoice as $invoiceInfo){
                      $totalAmt = $totalAmt + floatval($invoiceInfo->row_total);
                      }
                      ?>                      
                    <td><?php echo number_format($totalAmt,2); ?></td>
                  </tr>                                            
                </tbody>
              </table>
              <?php //if($mode=="view"){ ?>
              <form method="post" action="<?php echo base_url();?>Projects/initiate_payment">
                <?php foreach($invoiceDates as $invoiceInfoDates){?>
                <input type="hidden" name="invoice_id" value="<?php echo $invoiceInfoDates->invoice_id?>">
                <input type="hidden" name="total_amt" value="<?php echo $invoiceInfoDates->total_amt?>">                            
                <?php }?>
                <div class="text-right">
                <input type="submit" class="btn btn-primary" value="Make A Payment"/>
                <!--<button class="btn btn-primary" type="submit">Make A Payment</button>-->
                </div>
              </form>
                <?php //}?>
              </div>
            </div>                 
        </div>
    </div>
  </div>
</div>
<?php $this->load->view('common/footerscript');?>
<script>      
    $(document).ready(function(){
      $("#tax_id_k").change(function () {
        var selectedValue = $(this).val();
        var taxp = $("#taxp_"+selectedValue).val();
        //alert(taxp);  
        $("#tax_k").val(taxp);
      });
   });
</script>   
</body>
</html>