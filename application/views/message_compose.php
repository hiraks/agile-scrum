<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Compose Message']?> </title>
<?php $this->load->view('common/headerscript');?>
</head>
<body class="no-skin-config">
<div class="loader"></div>
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">  
    <div class="row border-bottom"></div>       
        <div class="row wrapper line_buttom white-bg page-heading">
            <div class="col-lg-12">
                <h2><?php echo $this->session->userdata('languageArray')['Compose Message']?></h2>
            </div>
        </div>
        <?php include('common/message_menu.php');?>      
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">                  
                    <div class="ibox-content">
            <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url();?>Scrum/message_send" id="form-horizontal" enctype="multipart/form-data">
                <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Send To']?> *</label>
                    <div class="col-lg-6">
                        <select class="form-control" required="required" name="message_send_to" id="message_send_to">
                            <option value="">--Select--</option>
                            <?php foreach($username as $userdetails){?> 
                            <option value="<?php echo $userdetails->user_id?>"><?php echo $userdetails->username?></option>
                            <?php }?>                        
                        </select>
                    </div>  
                </div>
                <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Message Title']?> *</label>
                	<div class="col-lg-6">
                    <input type="text" name="message_title" class="form-control" required="required">
                    </div>
                </div>                           
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Message Description']?> *</label>
                	<div class="col-lg-6">
                    <textarea class="form-control" name="message_descrription"></textarea>     
                    </div> 
                </div>   
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Document Attached']?></label>
                    <div class="col-lg-6">                    
                    <input type="file" name="userfile" class="form-control">   
                    </div> 
                </div>            
                <?php 
                    $csrf = array(
            			'name' => $this->security->get_csrf_token_name(),
            			'hash' => $this->security->get_csrf_hash()
    				);
                ?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <div class="form-group form-controlcenter">                
                    <button type="submit" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Submit']?></button>
                    <button type="reset" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Cancel']?></button>                 
                </div>      
            </form>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>    
<?php $this->load->view('common/footerscript');?>
</body>
</html>