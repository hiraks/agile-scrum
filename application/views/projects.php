<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Projects']?> </title>
<?php $this->load->view('common/headerscript');?>
</head>
<body class="no-skin-config">
<!--<div class="loader"></div>-->
    <div id="wrapper">   
        <div id="page-wrapper" class="gray-bg"> 
        <div class="row border-bottom"></div>       
            <div class="row wrapper line_buttom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?php echo $this->session->userdata('languageArray')['Projects']?></h2>                    
                </div>
                <div class="col-lg-2" style="vertical-align:bottom;">                
                <h2><a class="btn btn-primary " href="<?php echo base_url();?>Menus/add_project"><?php echo $this->session->userdata('languageArray')['Add a new project']?></a></h2>
                </div>
            </div>           
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">                  
                    <div class="ibox-content">
                    <table class="table table-bordered table-hover dataTables-example" >         
                    <thead>
                    <tr>
                        <th><?php echo $this->session->userdata('languageArray')['Project Name']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Client']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Allocated Resources']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Start Date']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['End Date']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Actions']?></th>
                    </tr>
                    </thead>
                    <tbody>
                   <?php foreach($results as $records){?>
                    <tr >
                        <td><a href="<?php echo base_url();?>Menus/SubMenu/<?php echo $records->project_id ;?>/dashboard?active=dashboard" ><?php echo $records->Name ;?></a></td>
                        <td><?php echo $records->client_name ;?>
                        </td>
                        <td>
                        <?php 
                        $userids = explode(',', $records->resources_allocatted);                 
                        $ss = count($userids);                        
                        $i=0;
                    ?>
                    <div class="team-members">
                    <?php
                        for($i=0;$i<$ss;$i++){                            
                            $rr = userDetails($userids[$i]);                
                            foreach ($rr as $userpic) 
                            {                                                              
                    ?>
                            <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$userpic['filename']);?>"/>&nbsp;
                    <?php
                            }                                 
                        }                    
                    ?>  
                    </div>                          
                        </td>
                        <td class="center"><?php echo $records->start_date ;?></td>
                        <td class="center"><?php echo $records->end_date ;?></td>
                        <td>                         
                        <a href="<?php echo base_url();?>Menus/edit_project/<?php echo $records->project_id ;?>"><button type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->session->userdata('languageArray')['Edit']?>" aria-label="Left Align"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></a>
                        <a href="<?php echo base_url();?>Menus/edit_project/<?php echo $records->project_id ;?>"><button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->session->userdata('languageArray')['View']?>" aria-label="Left Align"><span class="fa fa-eye" aria-hidden="true"></span></button></a>
                        <button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->session->userdata('languageArray')['Delete']?>" onclick="deleteClient('<?php echo $records->project_id ;?>');" aria-label="right Align"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>                   
                        </td>
                    </tr>
                  <?php }?>  
                     </tbody>
                    <tfoot>                   
                    </tfoot>
                    </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
<?php $this->load->view('common/footerscript');?>
</body>
</html>