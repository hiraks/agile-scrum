<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $this->session->userdata('languageArray')['Invoices']?> </title>
<?php $this->load->view('common/headerscript');?>    
</head>
<body class="no-skin-config">
<div class="loader"></div>
	<div id="wrapper">
		<div id="page-wrapper" class="gray-bg">
		<div class="row border-bottom"></div>
			<div class="row wrapper line_buttom white-bg">
				<div class="col-lg-10">
					<h2><?php echo $this->session->userdata('languageArray')['Invoices']?></h2>
				</div>
				<div class="col-lg-2">
				<h2>
				<?php 
				//echo $user_type; 
				if($user_type==1)
				{
				?>
				<a class="btn btn-primary"
					href="<?php echo base_url();?>Menus/add_invoice"><?php echo $this->session->userdata('languageArray')['Create Invoice']?></a>
				<?php
				}
				?>				
				</h2>
				</div>
			</div>			
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-content">
								<table class="table table-bordered table-hover dataTables-example">
									<thead>
										<tr>
											<th><?php echo $this->session->userdata('languageArray')['Invoice Id']?></th>
											<th><?php echo $this->session->userdata('languageArray')['Client']?></th>
											<th><?php echo $this->session->userdata('languageArray')['Status']?></th>
											<th><?php echo $this->session->userdata('languageArray')['Total Amt']?></th>
											<th><?php echo $this->session->userdata('languageArray')['Due Amount']?></th>
											<?php 
											 if($this->session->userdata('user_type_id')==1)
											 {
											?>
											<th><?php echo $this->session->userdata('languageArray')['Actions']?></th>
											<?php 
											 }			
											?>											
										</tr>
									</thead>
									<tbody>
                   					<?php foreach($results as $records){?>
                    				<tr>
										<td><?php echo $records->invoice_id ;?></td>
										<td><?php echo $records->organization_name ;?></td>
										<td><?php echo (($records->pay_status=='Paid')?'Paid':'Unpaid') ;?></td>
										<td class="center"><?php echo $records->total_amt;?></td>
										<td class="center"><?php echo (intval($records->total_amt)-intval($records->paid_amt)) ;?></td>
										<?php 
											 if($this->session->userdata('user_type_id')==1)
											 {
											?>
										<td>
											<a href="<?php echo base_url();?>Projects/edit_invoice/<?php echo $records->invoice_id ;?>">
											<button type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit"aria-label="Left Align"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
											</button></a>
											<a href="<?php echo base_url();?>Projects/view_invoice_details/<?php echo $records->invoice_id ;?>">
											<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="Edit"aria-label="Left Align"><span class="fa fa-eye" aria-hidden="true"></span>
											</button></a>
											<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete"onclick="deleteUser('<?php echo $records->invoice_id ;?>');"aria-label="right Align"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
											</button>
										</td>
										<?php 
											 }			
											?>	
									</tr>
                  					<?php }?>  
                    				</tbody>									
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>    
</body>
<?php $this->load->view('common/footerscript');?>
    <script type="text/javascript">
	function deleteUser(id)
	{		
		var result=confirm("Do you really want to delete the invoice ?");
		if(result==true)
			{
			location.href="<?php echo base_url();?>Menus/delete_invoice/"+id;

			}
		else
			{
				return false;
			}
		return ;
	}
    </script>
</html>