<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $this->session->userdata('languageArray')['Message Details Dashboard']?> </title>
<?php $this->load->view('common/headerscript');?>
</head>
<body class="no-skin-config">
<div class="loader"></div>
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
    <div class="row border-bottom"></div>
        <div class="row wrapper line_buttom white-bg page-heading">
            <nav class="navbar" role="navigation">
                <div class="navbar-header">
                    <h2>&nbsp;<?php echo $this->session->userdata('languageArray')['Message Details Dashboard']?></h2>
                </div>
            </nav>
        </div>  
        <br/>
        <?php include('common/message_menu.php');?>    
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">                  
                    <div class="ibox-content">  

                         <?php foreach($results as $records){?>                         
                        <ul class="" style="padding: 0">
                            <li class="list-group-item"><?php echo $this->session->userdata('languageArray')['Message Title']?>  <span class="label pull-right"><?php echo $records->message_title;?></span> </a></li>
                    	</ul>                    
                        <ul class="" style="padding: 0">
                            <li class="list-group-item"><?php echo $this->session->userdata('languageArray')['Issue Type']?>  <span class="label pull-right"><?php echo $records->message_descrription;?></span> </a></li>
                    	</ul>                    
                        <ul class="" style="padding: 0">
                            <li class="list-group-item"><?php echo $this->session->userdata('languageArray')['Attachments']?>  <span class="label pull-right"><a target="_blank" href="<?php echo(base_url()."./uploads/".$records->message_attachment);?>"><?php echo $records->message_attachment;?></span></a></li>
                    	</ul> 
                        <?php } ?>             
                   </div>
                </div>
            </div>
            </div>
            <div class="row"></div>
        <div class="footer"></div>
        </div>
    </div>
<?php $this->load->view('common/footerscript');?>
</body>
</html>