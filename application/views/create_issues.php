<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Create Story / Issues']?> </title>
  <?php $this->load->view('common/headerscript');?>
</head>
<body class="no-skin-config">
<div class="loader"></div>
    <div id="wrapper">
    <div id="page-wrapper" class="gray-bg"> 
    <div class="row border-bottom"></div>
        <?php include 'projectmenu.php';?> 
        <div class="row wrapper line_buttom white-bg page-heading">
            <div class="col-lg-12">
                <h2><?php echo $this->session->userdata('languageArray')['Create Story / Issues']?></h2>                
            </div>                
        </div>     
        <div class="col-lg-12 animated fadeInRight">
            <div class="ibox-content">
                <form class="form-horizontal" id="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="<?php echo base_url();?>Scrum">              
                <?php 
                    $csrf = array(
            			'name' => $this->security->get_csrf_token_name(),
            			'hash' => $this->security->get_csrf_hash()
    				);
                ?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />    
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Issue Type']?> *</label>
                	<div class="col-lg-6">
                    <select name="issue_type_id" class="form-control" required="required">
                    <option value="">--Select--</option>
                    <?php                    
                    	if($navs=='1')
                    	{                    	           	
                    ?>
	                    <option value="1"><?php echo $this->session->userdata('languageArray')['Bug']?></option>
	                    <option value="2"><?php echo $this->session->userdata('languageArray')['Story']?></option>
	                    
	                    <?php }
	                    else if ($navs=='2')
	                    {
	                    ?>
	                    <option value="1"><?php echo $this->session->userdata('languageArray')['Bug']?></option>
	                    <option value="2"><?php echo $this->session->userdata('languageArray')['Story']?></option>
	                    
	                    <?php }
	                    ?>
                    </select>
                 	</div>
                </div>                
                <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Title']?> *</label>
                	<div class="col-lg-6">
                    <input type="title" name="title" class="form-control" required="required" maxlength="27">
                    </div>
                </div>
                <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Description']?> *</label>
                	<div class="col-lg-6">
                    <textarea type="description" name="description" class="form-control"  required="required" maxlength="490"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Priority']?> *</label>
                	<div class="col-lg-6">
                    <select name="priority" class="form-control" required="required">
                        <option value="">--Select--</option>
	                    <option value="Major"> <?php echo $this->session->userdata('languageArray')['Major']?> </option>
	                    <option value="Minor"> <?php echo $this->session->userdata('languageArray')['Minor']?> </option>
	                    <option value="Trivial" > <?php echo $this->session->userdata('languageArray')['Trivial']?> </option>
	                    <option value="Critical"> <?php echo $this->session->userdata('languageArray')['Critical']?>  </option>
                    </select>
                 	</div>
                </div>                           
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Reported By']?> *</label>
                	<div class="col-lg-6">
                    <select name="reported_by" class="form-control" required="required">
                        <option value="">--Select--</option>
	                    <?php foreach($results as $records){?>                   
	                    <option value="<?php echo $records->username ;?>"> <?php echo $records->username ;?> </option>
	                   <?php }?>
                    </select>
                   </div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Assigned To']?> *</label>
                	<div class="col-lg-6">
                    <select name="assigned_to" class="form-control" required="required">
                        <option value="">--Select--</option>
	                    <?php foreach($results as $records){?>         
	                    <option value="<?php echo $records->username ;?>"> <?php echo $records->username ;?> </option>
	                   <?php }?>
                    </select>
                   </div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Time Estimate']?> *</label>
                	<div class="col-lg-6">
                    <input type="text" name="time" class="form-control" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Attachments']?></label>
                	<div class="col-lg-6">
                    <input type="file" name="userfile" class="form-control">
                    </div>
                </div>
                <div class="form-group form-controlcenter">                
                    <button type="submit" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Submit']?></button>
                    <button type="reset" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Cancel']?></button>                 
                </div>       
            </form>
        </div>                
        </div>
        </div>        
    </div> 
<?php $this->load->view('common/footerscript');?>   
</body>
</html>