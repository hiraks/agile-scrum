<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Timesheets']?> </title>
    <?php $this->load->view('common/headerscript');?>
</head>
<body class="no-skin-config">
<div class="loader"></div>
    <div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom"></div> 
            <?php include 'projectmenu.php';?>
            <div class="row wrapper line_buttom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?php echo $this->session->userdata('languageArray')['Timesheets']?></h2>
                </div>               
            </div>         
    <div class="col-lg-12 animated fadeInRight">
        <div class="ibox float-e-margins">                  
            <div class="ibox-content">
                <table class="table table-bordered table-hover dataTables-example">    
                    <thead>
                    <tr>
                        <th><?php echo $this->session->userdata('languageArray')['Title']?></th>                        
                        <th><?php echo $this->session->userdata('languageArray')['Logged Hours']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Assigned To']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Priority']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Status']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Logged Hours']?></th>                       
                    </tr>
                    </thead>
                    <tbody>                 
                    <?php foreach($results as $records){?>
                    <tr >
                        <td><a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id ;?>" ><?php echo $records->summary ;?></a>
                        </td>                       
                        <td>
                        <?php echo $records->logged_estimate ;?>
                        <!-- <div class="progress progress-small">
    						<div class="progress-bar" style="width: 50%;"> <small class="pull-right">73%</small></div>
    					</div>-->                        
                        <?php //echo $records->reported_by ;?>
                        </td>
                        <td><?php echo $records->assigned_to ;?></td>
                        <td class="center"><?php echo $records->priority ;?></td>                 
                        <?php if( $records->status=='Progress') {?>
                        <td class="center"><span class="badge badge-warning"><?php echo $this->session->userdata('languageArray')['In Progress']?></span></td>                         
                        <?php }                        
                        else if( $records->status=='Resolved') {?>
                        <td class="center"><span class="badge badge-primary"><?php echo $this->session->userdata('languageArray')['Resolved']?></span></td>
                        <?php }                        
                        else if( $records->status=='Reopened') {?>
                        <td class="center"><span class="badge badge-info"><?php echo $this->session->userdata('languageArray')['Reopened']?></span></td>     
                        <?php }                        
                        else if( $records->status=='Closed') {?>
                        <td class="center"><span class="badge badge-success"><?php echo $this->session->userdata('languageArray')['Closed']?></span></td>    
                        <?php }                         
                        else {                        	
                        ?>
                        <td class="center"><span class="badge badge-danger"><?php echo $this->session->userdata('languageArray')['Not Started']?></span></td>
                        <?php }?>
                        <td>
                        <button class="btn btn-primary " type="button" onclick="test('<?php echo $records->summary; ?>','<?php echo $this->session->userdata('user_name');?>','<?php echo $records->issue_id ;?>','<?php echo $records->logged_estimate ;?>','<?php echo $records->sprint_id ;?>');" href="#modal-form" data-toggle="modal">
    					<i class="fa fa-paste"></i>
    					<span class="bold"><?php echo $this->session->userdata('languageArray')['Log Hours']?></span>
    					</button>
                        <!-- <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Details" aria-label="Left Align">
    							<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    						</button>

    					<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Delete" aria-label="right Align">
    						<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
    					</button>
    					 -->
    					</td>
                    </tr>
                    <?php }?>               
                    </tbody>                    
                </table>
            </div>
        </div>
    </div>           
    <div id="modal-form" class="modal fade" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row ">
                        <div class="col"><h3 class="m-t-none m-b border-bottom"><?php echo $this->session->userdata('languageArray')['Log Hours']?></h3>
                        <br/>
                        <form role="form" method="POST" class="form-horizontal" action="<?php echo base_url();?>Scrum/save_timesheet">
                            <?php 
                                $csrf = array(
        						'name' => $this->security->get_csrf_token_name(),
        						'hash' => $this->security->get_csrf_hash()
			                );?>
			                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <div class="form-group">
							<label class="col-lg-3 control-label"><?php echo $this->session->userdata('languageArray')['User']?></label>
								<div class="col-lg-9">
								<input type="text" id="userid" name="userid" disabled="disabled" class="form-control">
								</div>
							</div>														
							<input type="hidden" name="issue_id" id="issue_id"/>
                            <input type="hidden" name="logged_estimate" id="logged_estimate"/>
							<input type="hidden" name="sprint_id" id="sprint_id"/>
                            <div class="form-group">
								<label class="col-lg-3 control-label"><?php echo $this->session->userdata('languageArray')['Issue']?></label>
								<div class="col-lg-9">
								<input type="text" id="title" disabled="disabled" class="form-control">
								</div>
							</div>														
							<div class="form-group" id="data_1">
								<label class="col-lg-3 control-label"><?php echo $this->session->userdata('languageArray')['Date']?></label>
								<div class="col-lg-9">
								<div class="input-group date">
       								<input type="text" id="logged_date" name="logged_date" class="form-control" value="">
       								 <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
    							</div>
								</div>
							</div>                                                       
                            <div class="form-group">
								<label class="col-lg-3 control-label"><?php echo $this->session->userdata('languageArray')['Worked']?></label>
								<div class="col-lg-9">
								<input type="text"  class="form-control" id="logged_hours" name="logged_hours">
								</div>
							</div>						
							<div class="form-group">
								<label class="col-lg-3 control-label"><?php echo $this->session->userdata('languageArray')['Description']?></label>
								<div class="col-lg-9">
								<textarea id="description" name="description" class="form-control"></textarea> 
								</div>
							</div>                                                    
                            <div class="form-group form-controlcenter">                
                                <button type="submit" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Submit']?></button>
                                <button type="reset" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Cancel']?></button>
                            </div>   
                        </form>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('common/footerscript');?>
</body>
</html>