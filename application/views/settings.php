<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SCRUM | Settings</title>
   <?php $this->load->view('common/headerscript');?>
</head>
<body>
    <div id="wrapper">    
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom"></div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Settings</h2>                    
                </div>
                <div class="col-lg-2"></div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">           
            <div class="row">            
            <div class="col-lg-12 animated fadeInRight">
            <div class="ibox-content">
            <form class="form-horizontal" id="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="<?php echo base_url();?>Projects/save_settings">         
               <?php 
                    $csrf = array(
            			'name' => $this->security->get_csrf_token_name(),
            			'hash' => $this->security->get_csrf_hash()
    				);
                ?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <?php foreach($results as $records){?>          
                <div class="form-group">
                    <label class="col-lg-4 control-label">Company Name *</label>
                	<div class="col-lg-6">
                   	<input type="fullname" name="name" class="form-control" value="<?php echo $records->company_name;?>" required="required">
                 	</div>
                </div>                
                <div class="form-group">
                <label class="col-lg-4 control-label">Company Address *</label>
                	<div class="col-lg-6">
                   <textarea name="address" class="form-control" required="required"><?php echo $records->company_address;?></textarea>                   
                    </div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label">Zipcode *</label>
                	<div class="col-lg-6">
                    <input type="zipcode" name="zipcode" class="form-control" value="<?php echo $records->zipcode;?>" required="required">
                 	</div>
                </div>                           
                <div class="form-group">
                    <label class="col-lg-4 control-label">Company Email *</label>
                	<div class="col-lg-6">
                    <input type="email" name="email" class="form-control" value="<?php echo $records->email;?>" required="required">
                    </div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label">Company Phone *</label>
                	<div class="col-lg-6">
                    <input type="phone" name="phone" class="form-control" value="<?php echo $records->phone;?>" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label">Company Registration</label>
                	<div class="col-lg-6">
                    <input type="registration" name="registration" class="form-control" value="<?php echo $records->registration;?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label">Company Vat</label>
                	<div class="col-lg-6">
                    <input type="vat" name="vat" class="form-control" value="<?php echo $records->vat;?>" >
                    </div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label">Vat/Tax %</label>
                	<div class="col-lg-6">
                    <input type="vatpercent" name="vatpercent" class="form-control" value="<?php echo $records->vat_percent;?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label">Company Logo</label>
                	<div class="col-lg-6">
                    <?php //echo ($records->error!=''?$records->error:''); ?>
                    <input type="file" id="logoimg" name="userfile" class="form-control" required="required" >
                    <img class="img-circle-bs" src="<?php echo(base_url()."./uploads/".$records->logo);?>"/>
                    </div>
                </div>
            <?php }?>  
            <br/> 
            <table id="table-data">
                <tr>
                    <td class="col-lg-4"><strong>Tax Name</strong></td>
                    <td class="col-lg-4"><strong>Tax Percentage</strong></td> 
                    <td class="col-lg-2"></td>
                    <td class="col-lg-2"></td>
                </tr>
                <?php                 
                if(!empty($tax))
                {
                    foreach($tax as $td){
                        $tax_name = $td->tax_name;                        
                        $tax_percentage = $td->tax_percentage;
                ?>
                <tr class="tr_clone">
                    <td class="col-lg-4"><input type="text" name="tax_name[]" value="<?php echo (!empty($tax_name))?$tax_name:''; ?>" class="form-control"></td>
                    <td class="col-lg-4"><input type="text" name="tax_percentage[]" value="<?php echo (!empty($tax_percentage))?$tax_percentage:''; ?>" class="form-control"></td>
                    <td class="col-lg-4"><input type="button" name="delete" value="Delete" class="tr_clone_remove btn btn-danger"></td>                              
                </tr>
                <?php 
                    } 
                }
                ?>
                <tr class="tr_clone">
                    <td class="col-lg-4"><input type="text" name="tax_name[]" class="form-control"></td>
                    <td class="col-lg-4"><input type="text" name="tax_percentage[]" class="form-control"></td>
                    <td class="col-lg-4"><input type="button" name="add" value="Add" class="btn btn-primary tr_clone_add">&nbsp;<input type="button" name="add" value="Delete" class="tr_clone_remove btn btn-danger"></td>                              
                </tr>
            </table> 
            <br/>              
            <div class="form-group form-controlcenter">                
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-primary">Cancel</button>  
            </div>        
            </form>
                </div>   
            </div>
        </div>        
    </div>
</div>
<script src="<?php echo base_url();?>assets/js/jquery-1.6.4.min.js"></script>
<script type="text/javascript">
    $("input.tr_clone_add").live('click', function() {
        var $tr = $(this).closest('.tr_clone');
        var $clone = $tr.clone();
        $clone.find(':text').val('');
        $tr.after($clone);
    });
    $("input.tr_clone_remove").live('click', function() {
        $(this).closest('tr').remove();
    });       
</script>
<?php $this->load->view('common/footerscript');?>
</body>
</html>