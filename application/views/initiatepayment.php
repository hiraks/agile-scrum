<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Initiate Payment']?> </title>
    <?php $this->load->view('common/headerscript');?> 
</head>
<body>
    <div id="wrapper">    
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom"></div>
            <div class="row wrapper line_buttom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?php echo $this->session->userdata('languageArray')['Initiate Payment']?></h2>                    
                </div>
                <div class="col-lg-2"></div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">           
        <div class="row">    
            <div class="col-lg-12 animated fadeInRight">
            <div class="ibox-content">
            <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url();?>Projects/pay_details">				
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Invoice Id']?></label>
                	<div class="col-lg-6">
                   	<input type="id" name="invoice_id" class="form-control" required="required" value=" <?php echo $invoice_id; ?>">
                 	</div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Total Amount']?></label>
                	<div class="col-lg-6">
                    <input type="amount" name="total_amt" class="form-control" required="required" value=" <?php echo $total_amt; ?>">
                    </div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Pay Mode']?></label>
                	<div class="col-lg-6">
                   		<select name="mode" id="mode" onchange="ViewClient()" class="form-control">
		                    <option value="Paypal">PayPal</option>
                            <option value="CreditCard">Credit Card</option>
		                    <option value="DebitCard">Debit Card</option>
                            <option value="BankTransfer">Bank Transfer</option>	                    
	              	 	</select>                 		
                    </div>
                </div>                            
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Pay Amount']?></label>
                	<div class="col-lg-6">
                     <input type="amount" name="pay_amount" class="form-control" required="required" value=" <?php echo $total_amt; ?>">
                   </div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Notes']?></label>
                	<div class="col-lg-6">
                    <input type="notes" name="notes" class="form-control"  >
                    </div>
                </div>                         
                <div class="form-group form-controlcenter">                
                    <button type="submit" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Pay']?></button>
                    <button type="reset" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Cancel']?></button>                 
                </div>      
            </form>
        </div>                 
        </div>
    </div>     
</div>
</div>
    <?php $this->load->view('common/footerscript');?>
</body>
</html>