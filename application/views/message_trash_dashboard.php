<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $this->session->userdata('languageArray')['Message Dashboard']?> </title>
<?php $this->load->view('common/headerscript');?>
</head>
<body class="no-skin-config">
<div class="loader"></div>
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">  
    <div class="row border-bottom"></div>       
        <div class="row wrapper line_buttom white-bg">
            <div class="col-lg-12">
                <h2><?php echo $this->session->userdata('languageArray')['Message Sent Dashboard']?></h2>
            </div>
        </div>
        <?php include('common/message_menu.php');?> 		 
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">                	             
                <div class="ibox-content">		
			<table class="table table-bordered table-hover dataTables-example" id="s">
				<thead>
					<tr>
						<th><?php echo $this->session->userdata('languageArray')['Title']?></th>
						<th><?php echo $this->session->userdata('languageArray')['Description']?></th>
						<th><?php echo $this->session->userdata('languageArray')['Send By']?></th>
						<th><?php echo $this->session->userdata('languageArray')['Action']?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($results as $records){?>
                    <tr>                        
                        <td id="solTitle"><a href="#<?php echo $records->message_id;?>"><?php echo $records->message_title;?></td>
                        <td><?php echo $records->message_descrription;?></td>
                        <td><?php 
                        $id = $records->message_send_by;
                        $user_dtl = userDetails($id);
                        //dump($user_dtl);                        
                        $user_name=$user_dtl[0];
                        echo $user_name['fullname'];                       
                        ?> </td>                          
                        <td>                       
						<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->session->userdata('languageArray')['Delete']?>" onclick="deletemessageinbox('<?php echo $records->message_id;?>');" aria-label="right Align">
 						<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
						</button>						
						</td>
                    </tr>
                  <?php }?> 
				 </tbody>
                    <tfoot>                   
                    </tfoot>
                    </table>
                    </div>
                </div>
             
            </div>         
            <div class="row"></div>
        <div class="footer"></div>
        </div>
    </div>
<?php $this->load->view('common/footerscript');?>
</body>
</html>