<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php echo $this->session->userdata('languageArray')['Dashboard']?> </title>
  <?php $this->load->view('common/headerscript');?>
 
   
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   
    <script type="text/javascript">

       
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],

          <?php
                  $datarows = '';
                  foreach($expense_data as $gc){          
                      $datarows = $datarows."["."'".$gc->Year."'".' , '.$gc->total_amt.' , '.$gc->exp_amount.'],';       
                    
                  }
                
                  echo $datarows;
         
                ?>
        /*  ['Jan-20',  1000,      400],
          ['Feb-20',  1170,      460],
          ['March-20',  660,       1120],
          ['April-20',  1030,      540],
          ['2008',  1035,      560],
          ['2009',  1130,      640],
          ['2010',  2030,      740],
          ['2011',  3030,      840],
          ['2012',  4030,      940],*/
        ]);

        var options = {
          title: '<?php echo $this->session->userdata('languageArray')['Company Performance']?>',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
   
    
</head>
<body class="no-skin-config">
<!--<div class="loader"></div>-->
  <div id="wrapper">
    <div id="page-wrapper" class="gray-bg"> 
      <div class="row border-bottom"></div>
        <div class="wrapper wrapper-content gray-bg">   
          <div class="row">   
            <div class="col-lg-3">
              <div class="widget style1 navy-bg">
                  <div class="row">
                      <div class="col-xs-4">
                          <i class="fa fa-th-list fa-5x"></i>
                      </div>
                      <div class="col-xs-8 text-right">
                          <span> <?php echo $this->session->userdata('languageArray')['Projects']?> </span>
                          <h2 class="font-bold"><?php echo $projectcount;?></h2>
                      </div>
                  </div>
              </div>   
            </div>  
            <div class="col-lg-3">
              <div class="widget style1 lazur-bg">
                  <div class="row">
                      <div class="col-xs-4">
                          <i class="fa fa-th-large fa-5x"></i>
                      </div>
                      <div class="col-xs-8 text-right">
                          <span><?php echo $this->session->userdata('languageArray')['Open Issues']?>  </span>
                          <h2 class="font-bold"><?php echo $openIssueCount;?></h2>
                      </div>
                  </div>
              </div>   
            </div> 
            <div class="col-lg-3">
              <div class="widget style1 yellow-bg">
                  <div class="row">
                      <div class="col-xs-4">
                          <i class="glyphicon glyphicon-ok-sign fa-5x"></i>
                      </div>
                      <div class="col-xs-8 text-right">
                          <span><?php echo $this->session->userdata('languageArray')['Closed Issues']?>  </span>
                          <h2 class="font-bold"><?php echo $closedIssueCount;?></h2>
                      </div>
                  </div>
              </div>   
            </div>  
            <div class="col-lg-3">
              <div class="widget style1 blue-bg">
                  <div class="row">
                      <div class="col-xs-4">
                          <i class="glyphicon glyphicon-time fa-5x"></i>
                      </div>
                      <div class="col-xs-8 text-right">
                          <span> <?php echo $this->session->userdata('languageArray')['Logged Hours']?> </span>
                          <h2 class="font-bold"><?php echo $loggedhours;?></h2>
                      </div>
                  </div>
              </div>   
            </div>         
          </div>       
           <input type="hidden" name="user_id" id="user_id" value="<?php $user_id = $this->session->userdata('user_id'); echo $user_id; ?>">
          <div class="row">  
            <div class="col-lg-8">
            <div class="row">
              <div class="ibox float-e-margins">                
               
                  <div class="ibox">
                    
                     <div id="curve_chart" style="width: 820px; height: 300px"></div>
                     
                      <!-- <img src="<?php //echo base_url();?>assets/img/dashboard_incomeexpense.png" width="183px;" height="195px;">-->
                    
                  </div>
             
                              
              </div>          
            <div class="col-lg-12 white-bg">            
              
               
                    <h3><?php echo $this->session->userdata('languageArray')['Agile Dashboard']?></h3>
                
                    
            </div>
            
            
            
            
            <div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 yellow-bg">
   		 	<h3><?php echo $this->session->userdata('languageArray')['To-do']?></h3>					
			<p class="small">
				<i class="fa fa-hand-o-up"></i> <?php echo $this->session->userdata('languageArray')['Tasks yet to be started']?>
			</p>
   		</div> 
   		<div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 maroon-bg">
   		 	<h3><?php echo $this->session->userdata('languageArray')['In Progress']?></h3>
			<p class="small">
				<i class="fa fa-hand-o-up"></i><?php echo $this->session->userdata('languageArray')['Tasks in progress']?>
			</p>
   		</div> 
   		
   		<div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 navy-bg">
   		 	<h3><?php echo $this->session->userdata('languageArray')['Completed']?></h3>
			<p class="small">
				<i class="fa fa-hand-o-up"></i> <?php echo $this->session->userdata('languageArray')['Tasks completed']?>
			</p>
   		</div>  
            
            
            <div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 light-yellow-bg">			
			<ul class="sortable-list agile-list" id="Not Started" style="height:100%;">
            <?php
            // print_r($results);											
            foreach($todolist as $records){
                $issue_story=($records->issue_type_id == '1') ? $this->session->userdata('languageArray')['Bug']: $this->session->userdata('languageArray')['Story'];
            // echo '============'.$records->status;
            if($records->status == 'Not Started'){						
           		if($records->priority == 'Minor'){?>
				<li class="info-element" id="<?php echo $records->issue_id;?>">
					<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>"><?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
					<div class="agile-detail-bs">
						<i class="fa fa-clock-o"></i><?php echo  substr($records->created_date,0,10) ?>
						 <span class="label badge-grey"><?php echo($issue_story);?></span>
					</div>
					
					<!--<div class="agile-detail-bs">Reported By:- <?php echo $records->reported_by;?></div>-->	
					<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-success"><?php echo $this->session->userdata('languageArray')['Minor']?></span>	</div>
					<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>
					<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
					<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>				
				</li>                               		
            <?php
               } 
               elseif($records->priority == 'Major'){
            ?>
                <li class="warning-element" id="<?php echo $records->issue_id;?>">
                <a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id ;?>"><?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
					<div class="agile-detail-bs"><i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>  <span class="label badge-grey"><?php echo($issue_story);?></span></div>
					
					<!--<div class="agile-detail-bs">Reported By:- -->	<?php //echo $records->reported_by;?><!--</div>-->	
					<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-warning"><?php echo $this->session->userdata('languageArray')['Major']?></span>	</div>
					<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>	
					<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
					<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>		
				</li>
            <?php															
               }elseif($records->priority == 'Critical'){
            ?>
                <li class="danger-element" id="<?php echo $records->issue_id;?>">
               		<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>">
                    <?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
					<div class="agile-detail-bs">
						<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
						 <span class="label badge-grey"><?php echo($issue_story);?></span>
					</div>
					
					<!--<div class="agile-detail-bs">Reported By:- -->	<?php //echo $records->reported_by;?><!--</div>-->	
					<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-danger"><?php echo $this->session->userdata('languageArray')['Critical']?></span>	</div>	
					<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>
					<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
					<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>					
				</li>
			<?php
                   	}
                }
            }
            ?>                                               
			</ul>							
		</div>
		<div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 light-maroon-bg">			
			<ul class="sortable-list agile-list" id="Progress"  style="height:100%;">
            <?php																				
            // print_r($results);	
            foreach ( $todolist as $records ){	
                $issue_story=($records->issue_type_id == '1') ? $this->session->userdata('languageArray')['Bug']: $this->session->userdata('languageArray')['Story'];
            //echo '============'.$records->status;
            if($records->status=='Progress'||$records->status=='Reopened'||$records->status=='Resolved'){
              if($records->priority=='Minor'){
            ?>
               <li class="info-element" id="<?php echo $records->issue_id;?>">
               	<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>">
            <?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
				<div class="agile-detail-bs">
					<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
					<span class="label badge-grey"><?php echo($issue_story);?></span>
				</div>
				<!--<div class="agile-detail-bs">Reported By:- -->	<?php //echo $records->reported_by;?><!--</div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div>	<div class=" pull-right"> <span class="label badge-success"><?php echo $this->session->userdata('languageArray')['Minor']?></span>	</div>
				<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>
				<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>	
				</li>
            <?php
            } 
            elseif ($records->priority == 'Major'){
            ?>                 		
				<li class="warning-element" id="<?php echo $records->issue_id;?>">
				<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>">
            <?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
				<div class="agile-detail-bs">
					<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
					<span class="label badge-grey"><?php echo($issue_story);?></span>
				</div>
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-warning"><?php echo $this->session->userdata('languageArray')['Major']?></span>	</div>
				<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>
				<!--<div class="agile-detail-bs"><?php //echo $records->priority;?></div>-->
				<div class="agile-detail-bs"><?php echo $records->status;?></div>
				</li>
            <?php
        	} elseif ($records->priority == 'Critical') {
        	?>              		
				<li class="danger-element" id="<?php echo $records->issue_id;?>">
				<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>">
                   		<?php echo $records->summary;?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
				<div class="agile-detail-bs">
					<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
					<span class="label badge-grey"><?php echo($issue_story);?></span>
				</div>
				<!--<div class="agile-detail-bs">Reported By:- <?php echo $records->reported_by;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-danger"><?php echo $this->session->userdata('languageArray')['Critical']?></span>	</div>
				<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>
				<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>	
				</li>
            <?php
                	}
                }
            }
            ?>	               	
			</ul>			
		</div>
		
		<div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 light-navy-bg">		
			<ul class="sortable-list agile-list" id="Closed" style="height:100%;">
            <?php
            // print_r($results);
            foreach($todolist as $records){
                $issue_story=($records->issue_type_id == '1') ? $this->session->userdata('languageArray')['Bug']: $this->session->userdata('languageArray')['Story'];
            // echo '============'.$records->status;
            	if ($records->status == 'Closed'){
            		if ($records->priority == 'Minor'){
            ?>
               	<li class="info-element" id="<?php echo $records->issue_id;?>">
               	<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>">
        		<?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
				<div class="agile-detail-bs">
					<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
					<span class="label badge-grey"><?php echo($issue_story);?></span>
				</div>
				<!--<div class="agile-detail-bs">Reported By:- <?php echo $records->reported_by;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div>	<div class=" pull-right"> <span class="label badge-success"><?php echo $this->session->userdata('languageArray')['Minor']?></span>	</div>
				<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>
				<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>	
				</li>
            <?php
            }
            elseif ($records->priority == 'Major'){
           	?>	
       		<li class="warning-element" id="<?php echo $records->issue_id;?>">
       		<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id ;?>">
        	<?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
			<div class="agile-detail-bs">
				<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
				<span class="label badge-grey"><?php echo($issue_story);?></span>
			</div>
			<!--<div class="agile-detail-bs">Reported By:- <?php echo $records->reported_by;?></div>-->	
			<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-warning"><?php echo $this->session->userdata('languageArray')['Major']?></span>	</div>	
			<div class=" pull-right"> &nbsp; <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>
			<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
			<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>	
			</li>
            <?php
        	} elseif ($records->priority == 'Critical'){
        	?>          		
			<li class="danger-element" id="<?php echo $records->issue_id;?>">
			<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>">
            <?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
			<div class="agile-detail-bs">
				<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
				<span class="label badge-grey"><?php echo($issue_story);?></span>
			</div>
			<!--<div class="agile-detail-bs">Reported By:- <?php echo $records->reported_by;?></div>-->	
			<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-danger"><?php echo $this->session->userdata('languageArray')['Critical']?></span>	</div>
			<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>	
			<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
			<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>	
			</li>
            <?php
            		}
            	}
            }
            ?> 	                
			</ul>									
		</div>
	
            
            
            
                                       
            
             <div class="row">
                    <div class="col-lg-12">    
                     <!-- <div class="ibox-title">
                    <h3>Events Calendar</h3>
                </div>   -->       
         <div id="modal-form" aria-hidden="true"></div>                            
                        <div class="ibox-content">
                       
                          <div id='calendarbs'> </div> 
                        </div>
         
          </div> 
           
          </div>
          </div>
          
          
          <div class="row">
          <div class="col-lg-12 white-bg"> 
          
                    <h3><?php echo $this->session->userdata('languageArray')['Payments Received']?></h3>
                   
           <div class="ibox-content">
                    <table class="table table-bordered table-hover dataTables-example">                  
                    <thead>
                        <tr>
                            <th><?php echo $this->session->userdata('languageArray')['Invoice Id']?></th>
                            <th><?php echo $this->session->userdata('languageArray')['Client']?></th>
                            <th><?php echo $this->session->userdata('languageArray')['Status']?></th>
                            <th><?php echo $this->session->userdata('languageArray')['Total Amt']?></th>
                            <th><?php echo $this->session->userdata('languageArray')['Due Amount']?></th>
                            <th><?php echo $this->session->userdata('languageArray')['Actions']?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($payments as $records){?>
                    <tr>
                        <td><?php echo $records->invoice_id ;?></td>
                        <td><?php echo $records->organization_name ;?></td>
                        <td><?php echo $records->pay_status ;?></td>
                        <td class="center"><?php echo '$'. $records->total_amt;?></td>
                        <td class="center"><?php echo '$'. (intval($records->total_amt)-intval($records->paid_amt));?></td>
                        <td>                        
                        <!--<a href="<?php //echo base_url();?>Projects/view_invoice/<?php //echo $records->invoice_id ;?>">-->
                        <a href="<?php echo base_url();?>Projects/view_invoice_details/<?php echo $records->invoice_id ;?>">
                        <button type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit"aria-label="Left Align"><span class="fa fa-eye" aria-hidden="true"></span></button></a>		
						</td>
                    </tr>
                  <?php }?>  
                    </tbody>
                    <tfoot>                   
                    </tfoot>
                    </table>
                    </div>
                    </div>
                    </div>
          </div> 
          
              
          <div class="col-lg-4 gray-bg"> 
          <h3><?php echo $this->session->userdata('languageArray')['Project Activity']?></h3>          
          <div class="content" chat-slim-scroll>      
          			
						<div class="col-lg-12 gray-bg">
								<div class="feed-activity-list">
								<?php //print_r($timeline_results);?>
								<?php foreach($timeline_results as $records){?>
									<div class="feed-element">
										<!-- <a href="#" class="pull-left"> <img alt="image"
											class="img-circle" src="img/a2.jpg">
										 -->	
									<a href="#" class="pull-left"> 	<img class="img-circle"  src="<?php echo(base_url()."./uploads/".$records->filename);?>"/>&nbsp;
											
										</a>	
										
										<div class="media-body ">
											<!-- <small class="pull-right"> --><strong><?php echo $records->fullname ;?></strong> &nbsp;<?php echo $records->created_date ;?><br><br><!-- </small> --> 
											<?php if($records->status == 'Not Started'){ echo('<span class="label badge-primary">Created </span>');}else{ echo('<span class="label badge-warning">Updated</span>');}  ;?>  &nbsp; <strong>Task: #<?php echo $records->issue_id ;?> -  </strong> <?php if(strlen($records->summary) < 20){echo $records->summary; } else { echo(substr($records->summary,0,14)."...");}?>. <br><br> <!-- <small
												class="text-muted">Today 2:10 pm - 12.06.2014</small> -->
												<strong>Status:</strong> <?php echo $records->status ;?>
											<!-- <span class="label badge-warning">Major</span>
											<span class="label badge-primary">10h </span> -->
									</div>
									
									</div>
									<?php }?>
									
								
							</div>
						</div>
						
						
						
					
          </div> 
          </div>
        <!--   <div class="col-lg-3  ">  
          
          <div class="ibox-content">           
                  <div id="donutchart"> </div>
                 </div>   
                </div>   -->
        </div>   
    </div>        
  </div>
</div>
<!--  <div id="eventedit"></div>      -->        
  <?php $this->load->view('common/footerscript');?> 
  
  
   
     <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"   integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="   crossorigin="anonymous"></script>
  
 
   <!-- 
  <script type="text/javascript" src="<?php// echo base_url();?>assets/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?php// echo base_url();?>assets/js/jquery-ui-1.8.custom.min.js"></script>
<script src="<?php //echo base_url();?>assets/Angular/js/plugins/ui-sortable/sortable.js"></script>-->
<!-- Example jQuery code (JavaScript)  -->
<script type="text/javascript">
 $(document).ready(function(){
	 $('#side-menu').metisMenu();
	$('#page-wrapper .sortable-list').sortable({
		connectWith: '#page-wrapper .sortable-list',
		placeholder: 'placeholdersd',
	});


	
}); 

var $sortable = $(".sortable-list");     	            

  	$sortable.sortable({  	
    	stop: function (event, ui){      	
      	status = ui.item.closest('ul').attr('id');
      	issue_id = ui.item.closest('li').attr('id');      	            
      	

        var url = "<?php echo site_url('Scrum/update_status');?>"; 
         	$.ajax({
                type: "post",
                url: url,
                datatype:'json',
                data: {issue_id:issue_id,status:status},
                success: function() {                                  
                
                }
        	});  
    	}
	}); 
</script>
  
  
  
  
  
  
<script>
   // jq13 = jQuery.noConflict(true);
</script>
  <script type="text/javascript">


  
       //alert(format(new Date(1513681074000).getTime(), 'MM/dd/yyyy HH:mm:ss'));

       
            /*   $('#calendarbs').fullCalendar({
                    //defaultDate: '2017-11-12',
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    events: function(start, end, timezone, callback) {
                      $.ajax({
                        type: "post",
                        url: url,
                        datatype:'json',
                        data: {user_id:user_id},
                        success: function(json) {
                          //alert(json);                             
                          var obj = jQuery.parseJSON(json);                             
                          var events = [];
                          //print_r(obj);
                          $.each(obj, function(index, value) {
                            //var kk = value['event_title'];
                            //alert(kk);
                            events.push({
                              id: value['event_id'],                              
                              title: value['event_title'],
                              start: value['event_startdate'],                                  
                              end: value['event_enddate'],
                              description: value['event_description'],
                              backgroundColor: value['event_color']                             
                            });                                                 
                          });
                          callback(events);
                        }
                });                 
            },                                
            eventClick: function(calEvent) {                        
               
                kk = calEvent.title;
                estartdate = calEvent.start;

                
                eenddate = calEvent.end;
                edescription = calEvent.description;
                color = calEvent.backgroundColor; 

              $("#modal-form").html('<div class="modal-dialog"><div class="modal-content"><div class="modal-body"><div class="row"><div class="col"><h3 class="m-t-none m-b border-bottom">Log Time</h3><br/><form class="form-horizontal" role="form" method="POST" action="<?php //echo base_url();?>Projects/events_update" id="form-horizontal"><input type="hidden" name="event_id" value="'+calEvent.id+'"><div class="form-group"><label class="col-lg-4 control-label">Event Title *</label><div class="col-lg-6"><input type="text" name="event_title" class="form-control" required="required" value="'+kk+'"></div></div><div class="form-group"><label class="col-lg-4 control-label">Event Start Date *</label><div class="input-group col-lg-6"><input type="text" id="event_startdate" name="event_startdate" class="form-control" required="required" value="'+format(new Date(calEvent.start).getTime(), 'yyyy/MM/dd HH:mm:ss')+'"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div></div><div class="form-group" id="data_1"><label class="col-lg-4 control-label">Event End Date *</label><div class="input-group date col-lg-6"><input type="text" id="event_enddate" name="event_enddate" class="form-control" required="required" value="'+format(new Date(calEvent.end).getTime(), 'yyyy/MM/dd HH:mm:ss')+'"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div></div><div class="form-group"><label class="col-lg-4 control-label">Description *</label><div class="input-group col-lg-6"><textarea id="event_description" name="event_description" class="form-control" required="required">'+edescription+'</textarea></div></div><div class="form-group"><label class="col-lg-4 control-label">Event Color</label><div class="input-group col-lg-6"><select id="event_color" name="event_color" class="form-control"><option value="">--Select Color--</option><option value="Green">Green</option><option value="Blue">Blue</option><option value="Red">Red</option><option value="Yellow">Yellow</option><option value="Black">Black</option><option value="Pink">Pink</option><option value="Orange">Orange</option></select></div></div><div class="form-group form-controlcenter"><button type="submit" class="btn btn-primary">Submit</button>&nbsp;<button type="reset" class="btn btn-primary">Cancel</button></div></form></div></div></div></div></div>'); 
              $('#event_color').val(color).find("option[value=" + color +"]").attr('selected', true);          
            },
            error: function(){                      
                alert('Error while request..');
            }
        })      
    });*/

/* function getFormattedDate(date) {
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear().toString().slice(2);
    var kkl = day + '-' + month + '-' + year;  
    //return kkl;    
    return day + '-' + month + '-' + year;             
}
$('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    }); */


</script>


<script>

  
   
  </script>   
</body>
</html>