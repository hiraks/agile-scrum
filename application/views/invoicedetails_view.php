<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php echo $this->session->userdata('languageArray')['Invoice Details']?> </title>
  <?php $this->load->view('common/headerscript');?>  
</head>
<body class="no-skin-config">
<div class="loader"></div>
  <div id="wrapper">    
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom"></div>
          <div class="row wrapper line_buttom white-bg page-heading">
              <div class="col-lg-12">                    
                   <h2><?php echo $this->session->userdata('languageArray')['View Invoice']?></h2>                  
              </div>
              <div class="col-lg-2"></div>
          </div>
        <div class="wrapper wrapper-content animated fadeInRight">           
          <div class="row">           
            <div class="col-lg-12 animated fadeInRight">
            <div class="ibox-content">
              <div class="row">
                <div class="col-sm-6">
                  <h5>From:</h5>
                    <address>
                      <?php foreach($company as $companyInfo){?>
                      <img alt="image" class="img-circle-bs" src="<?php echo(base_url()."./uploads/".$companyInfo->logo);?>" /><br/>
                      <strong><?php echo($companyInfo->company_name);?></strong><br>
                      <?php
                       $address=explode("," , $companyInfo->company_address);
                      for($count=0;$count<sizeof($address);$count++)
                      {
                       echo($address[$count]);?><br>
                      <?php }?>                      
                      <strong>Ph:- </strong> <?php echo($companyInfo->phone);?>
                      <?php }?>
                    </address>
                </div>
                <div class="col-sm-6 text-right">
                <address>
                <?php foreach($client as $clientInfo){?>
                <img class="img-circle-bs" src="<?php echo(base_url().UPLOAD_PATH_URL."client/".$clientInfo->c_logo);?>"/><br/>
                  <h4>Invoice No. :- <span class="text-navy"> INV-0<?php echo $invoiceId ;?></span></h4>               
                    <strong>To:- <?php echo($clientInfo->organization_name);?></strong><br>
                  <?php
                     $address=explode("," , $clientInfo->address);
                    for($count=0;$count<sizeof($address);$count++)
                    {
                     echo($address[$count]);?>
                     <?php }?>                                
                      <strong>Ph:- </strong> <?php echo $clientInfo->phone_no ;?><br/>
                    <?php }?>                                    
                      <?php foreach($invoiceDates as $invoiceInfoDates){?>
                      <strong>Invoice Date:</strong> <?php echo $invoiceInfoDates->created_on?><br/>
                      <strong>Due Date:</strong> <?php echo $invoiceInfoDates->due_date?>
                      <?php }?> 
                  </address>                
                </div>
              </div>
              <div class="table-responsive m-t">
                <table border="1" class="table invoice-table pretty">
                  <thead>
                    <tr>
                      <th><?php echo $this->session->userdata('languageArray')['Item List']?></th>
                      <th><?php echo $this->session->userdata('languageArray')['Quantity']?></th>
                      <th><?php echo $this->session->userdata('languageArray')['Unit Price']?></th>
                      <th><?php echo $this->session->userdata('languageArray')['Tax(%)']?></th>
                      <th><?php echo $this->session->userdata('languageArray')['Tax Amount']?></th>                          
                      <th><?php echo $this->session->userdata('languageArray')['Total Price']?></th>                        
                    </tr>
                  </thead>
                  <tbody>
                  <?php 
                  foreach($invoice as $invoiceInfo){
                    $invoice_id = $invoiceInfo->invoice_id;
                  ?>                          
                    <tr>                      
                      <td><?php echo $invoiceInfo->items;?></td> 
                      <td><?php echo $invoiceInfo->Quantity;?></td>
                      <td><?php echo number_format($invoiceInfo->unit_price,2);?></td>
                      <td>
                        <div class="col-lg-12">                              
                            <select class="form-control" name="tax_id" disabled="disabled">
                              <option value="">--Select Tax--</option>
                              <?php
                                foreach ($tax as $td) {
                              ?>
                                <option value="<?php echo $td->tax_id;?>" <?php echo ($invoiceInfo->tax_id==$td->tax_id)?'selected="selected"':'' ?>><?php echo $td->tax_name."[".$td->tax_percentage."]";?></option>               
                             <?php
                                }
                              ?> 
                            </select> 
                              <?php
                                foreach ($tax as $td) {
                              ?>
                                <input type="hidden" value="<?php echo $td->tax_percentage?>" id="taxp_<?php echo $td->tax_id?>"/>              
                             <?php
                                }
                              ?>                               
                          </div>  
                      </td>
                      <td><?php echo number_format(($invoiceInfo->Quantity*$invoiceInfo->unit_price)*($invoiceInfo->tax/100),2);?></td>
                      <td><?php echo number_format($invoiceInfo->row_total,2);?></td>      
                    </tr>
                    <?php }?>                     
                  </tbody>
                </table> 
                <table class="table invoice-total">
                <tbody>
                  <tr>
                    <td><strong><?php echo $this->session->userdata('languageArray')['Total Price']?> Total Price :</strong></td>
                    <?php  
                      $totalAmt=0;
                      foreach($invoice as $invoiceInfo){
                      $totalAmt = $totalAmt + floatval($invoiceInfo->row_total);
                      }
                      ?>                      
                    <td><?php echo number_format($totalAmt,2); ?></td>
                  </tr>                                   
                </tbody>
              </table>
                <div align="center"><a href="<?php echo base_url();?>Scrum/my_mPDF/<?php echo $invoice_id;?>" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size: 34px;"></i></a></div>     
              </div>                           
            </div>
            </div> 
        </div>        
    </div>
  </div>
</div>   
</body>
 <?php $this->load->view('common/footerscript');?>
</html>