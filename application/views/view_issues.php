<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $this->session->userdata('languageArray')['Issues']?> </title>
<?php $this->load->view('common/headerscript');?>
<body class="no-skin-config">
<!--<div class="loader"></div>-->
    <div id="wrapper">
    <div id="page-wrapper" class="gray-bg"> 
    <div class="row border-bottom"></div>
        <?php include 'projectmenu.php';?> 
        <div class="row wrapper line_buttom white-bg">
            <div class="col-lg-10">
                <h2><?php echo $this->session->userdata('languageArray')['Issues']?></h2>
            </div>
             <div class="col-lg-2" style="margin-top: 5px;">
                                   
                <a class="btn btn-primary " href="<?php echo base_url();?>Menus/add_issues/1/<?php echo $this->session->userdata('project_id')?>" ><?php echo $this->session->userdata('languageArray')['Add Issue / Story']?></a>
                </div>                
        </div>
   		<div class="col-xs-3 col-md-3 col-sm-3 col-lg-3 yellow-bg">
   		 	<h3>To-do</h3>					
			<p class="small">
				<i class="fa fa-hand-o-up"></i> <?php echo $this->session->userdata('languageArray')['Tasks yet to be started']?>
			</p>
   		</div> 
   		<div class="col-xs-3 col-md-3 col-sm-3 col-lg-3 blue-bg">
   		 	<h3>In Progress</h3>
			<p class="small">
				<i class="fa fa-hand-o-up"></i><?php echo $this->session->userdata('languageArray')['Tasks in progress']?>
			</p>
   		</div> 
   		<div class="col-xs-3 col-md-3 col-sm-3 col-lg-3 maroon-bg">
   		 	<h3>Testing</h3>
			<p class="small">
				<i class="fa fa-hand-o-up"></i><?php echo $this->session->userdata('languageArray')['Tasks in testing']?>
			</p>
   		</div> 
   		<div class="col-xs-3 col-md-3 col-sm-3 col-lg-3 navy-bg">
   		 	<h3>Completed</h3>
			<p class="small">
				<i class="fa fa-hand-o-up"></i><?php echo $this->session->userdata('languageArray')['Tasks completed']?> 
			</p>
   		</div>  
        <div class="col-xs-3 col-md-3 col-sm-3 col-lg-3 light-yellow-bg">			
			<ul class="sortable-list agile-list" id="Not Started" style="height:100%;">
            <?php
            // print_r($results);											
            foreach($results as $records){
                $issue_story=($records->issue_type_id == '1') ?'Bug':'Story';
            // echo '============'.$records->status;
            if($records->status == 'Not Started'){						
           		if($records->priority == 'Minor'){?>
				<li class="info-element" id="<?php echo $records->issue_id;?>">
					<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>"><?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
					<div class="agile-detail-bs">
						<i class="fa fa-clock-o"></i><?php echo  substr($records->created_date,0,10) ?>
						 <span class="label badge-grey"><?php echo($issue_story);?></span>
					</div>
					
					<!--<div class="agile-detail-bs">Reported By:- <?php echo $records->reported_by;?></div>-->	
					<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-success"><?php echo $this->session->userdata('languageArray')['Minor']?></span>	</div>
					<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>
					<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
					<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>				
				</li>                               		
            <?php
               } 
               elseif($records->priority == 'Major'){
            ?>
                <li class="warning-element" id="<?php echo $records->issue_id;?>">
                <a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id ;?>"><?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
					<div class="agile-detail-bs"><i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>  <span class="label badge-grey"><?php echo($issue_story);?></span></div>
					
					<!--<div class="agile-detail-bs">Reported By:- -->	<?php //echo $records->reported_by;?><!--</div>-->	
					<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-warning"><?php echo $this->session->userdata('languageArray')['Major']?></span>	</div>
					<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>	
					<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
					<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>		
				</li>
            <?php															
               }elseif($records->priority == 'Critical'){
            ?>
                <li class="danger-element" id="<?php echo $records->issue_id;?>">
               		<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>">
                    <?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
					<div class="agile-detail-bs">
						<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
						 <span class="label badge-grey"><?php echo($issue_story);?></span>
					</div>
					
					<!--<div class="agile-detail-bs">Reported By:- -->	<?php //echo $records->reported_by;?><!--</div>-->	
					<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-danger"><?php echo $this->session->userdata('languageArray')['Critical']?></span>	</div>	
					<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>
					<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
					<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>					
				</li>
			<?php
                   	}
                }
            }
            ?>                                               
			</ul>							
		</div>
		<div class="col-xs-3 col-md-3 col-sm-3 col-lg-3 light-blue-bg">			
			<ul class="sortable-list agile-list" id="Progress"  style="height:100%;">
            <?php																				
            // print_r($results);	
            foreach ( $results as $records ){	
                $issue_story=($records->issue_type_id == '1') ?'Bug':'Story';
            //echo '============'.$records->status;
            if($records->status=='Progress'||$records->status=='Reopened'||$records->status=='Resolved'){
              if($records->priority=='Minor'){
            ?>
               <li class="info-element" id="<?php echo $records->issue_id;?>">
               	<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>">
            <?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
				<div class="agile-detail-bs">
					<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
					<span class="label badge-grey"><?php echo($issue_story);?></span>
				</div>
				<!--<div class="agile-detail-bs">Reported By:- -->	<?php //echo $records->reported_by;?><!--</div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div>	<div class=" pull-right"> <span class="label badge-success"><?php echo $this->session->userdata('languageArray')['Minor']?></span>	</div>
				<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>
				<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>	
				</li>
            <?php
            } 
            elseif ($records->priority == 'Major'){
            ?>                 		
				<li class="warning-element" id="<?php echo $records->issue_id;?>">
				<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>">
            <?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
				<div class="agile-detail-bs">
					<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
					<span class="label badge-grey"><?php echo($issue_story);?></span>
				</div>
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-warning"><?php echo $this->session->userdata('languageArray')['Major']?></span>	</div>
				<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>
				<!--<div class="agile-detail-bs"><?php //echo $records->priority;?></div>-->
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?><?php echo $records->status;?></div>
				</li>
            <?php
        	} elseif ($records->priority == 'Critical') {
        	?>              		
				<li class="danger-element" id="<?php echo $records->issue_id;?>">
				<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>">
                   		<?php echo $records->summary;?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
				<div class="agile-detail-bs">
					<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
					<span class="label badge-grey"><?php echo($issue_story);?></span>
				</div>
				<!--<div class="agile-detail-bs">Reported By:- <?php echo $records->reported_by;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-danger"><?php echo $this->session->userdata('languageArray')['Critical']?></span>	</div>
				<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>
				<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>	
				</li>
            <?php
                	}
                }
            }
            ?>	               	
			</ul>			
		</div>
		<div class="col-xs-3 col-md-3 col-sm-3 col-lg-3 light-maroon-bg">		
			<ul class="sortable-list agile-list" id="Testing"  style="height:100%;">
            <?php																			
            // print_r($results);	
            foreach ( $results as $records ){	
                $issue_story=($records->issue_type_id == '1') ?'Bug':'Story';
            //echo '============'.$records->status;
            if($records->status=='Testing'){
              if($records->priority=='Minor'){
            ?>
            <li class="info-element" id="<?php echo $records->issue_id;?>">
               	<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>">
            <?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
				<div class="agile-detail-bs">
					<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
					<span class="label badge-grey"><?php echo($issue_story);?></span>
				</div>
				<!--<div class="agile-detail-bs">Reported By:- <?php echo $records->reported_by;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div>	<div class=" pull-right"> <span class="label badge-success"><?php echo $this->session->userdata('languageArray')['Minor']?></span>	</div>
				<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>
				<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>	
			</li>
            <?php
            } 
            elseif ($records->priority == 'Major'){
            ?>                 		
				<li class="warning-element" id="<?php echo $records->issue_id;?>">
				<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>">
            <?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
				<div class="agile-detail-bs">
					<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
					<span class="label badge-grey"><?php echo($issue_story);?></span>
				</div>
				<!--<div class="agile-detail-bs">Reported By:- <?php echo $records->reported_by;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-warning"><?php echo $this->session->userdata('languageArray')['Major']?></span>	</div>
				<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>	
				<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>	
				</li>
            <?php
        	} elseif ($records->priority == 'Critical') {
        	?>              		
				<li class="danger-element" id="<?php echo $records->issue_id;?>">
				<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>">
                   		<?php echo $records->summary;?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
				<div class="agile-detail-bs">
					<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
					<span class="label badge-grey"><?php echo($issue_story);?></span>
				</div>
				<!--<div class="agile-detail-bs">Reported By:- <?php echo $records->reported_by;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-danger"><?php echo $this->session->userdata('languageArray')['Critical']?></span>	</div>
				<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>	
				<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>	
				</li>
            <?php
                	}
                }
            }
            ?>              	
			</ul>								
		</div>
		<div class="col-xs-3 col-md-3 col-sm-3 col-lg-3 light-navy-bg">		
			<ul class="sortable-list agile-list" id="Closed" style="height:100%;">
            <?php
            // print_r($results);
            foreach($results as $records){
                $issue_story=($records->issue_type_id == '1') ?'Bug':'Story';
            // echo '============'.$records->status;
            	if ($records->status == 'Closed'){
            		if ($records->priority == 'Minor'){
            ?>
               	<li class="info-element" id="<?php echo $records->issue_id;?>">
               	<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>">
        		<?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
				<div class="agile-detail-bs">
					<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
					<span class="label badge-grey"><?php echo($issue_story);?></span>
				</div>
				<!--<div class="agile-detail-bs">Reported By:- <?php echo $records->reported_by;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div>	<div class=" pull-right"> <span class="label badge-success"><?php echo $this->session->userdata('languageArray')['Minor']?></span>	</div>
				<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>
				<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
				<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>	
				</li>
            <?php
            }
            elseif ($records->priority == 'Major'){
           	?>	
       		<li class="warning-element" id="<?php echo $records->issue_id;?>">
       		<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id ;?>">
        	<?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
			<div class="agile-detail-bs">
				<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
				<span class="label badge-grey"><?php echo($issue_story);?></span>
			</div>
			<!--<div class="agile-detail-bs">Reported By:- <?php echo $records->reported_by;?></div>-->	
			<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-warning"><?php echo $this->session->userdata('languageArray')['Major']?></span>	</div>	
			<div class=" pull-right"> &nbsp; <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>
			<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
			<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>	
			</li>
            <?php
        	} elseif ($records->priority == 'Critical'){
        	?>          		
			<li class="danger-element" id="<?php echo $records->issue_id;?>">
			<a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id;?>">
            <?php echo $records->summary; ?></a><div class="team-members pull-right"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/></div>
			<div class="agile-detail-bs">
				<i class="fa fa-clock-o"></i> <?php echo  substr($records->created_date,0,10) ?>
				<span class="label badge-grey"><?php echo($issue_story);?></span>
			</div>
			<!--<div class="agile-detail-bs">Reported By:- <?php echo $records->reported_by;?></div>-->	
			<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Assigned To']?>:- <?php echo $records->assigned_to;?></div><div class=" pull-right"> <span class="label badge-danger"><?php echo $this->session->userdata('languageArray')['Critical']?></span>	</div>
			<div class=" pull-right"> <span class="label badge-primary"><?php echo($records->original_estimate.'h'.' ');?></span>	</div>	
			<!--<div class="agile-detail-bs">Priority:- <?php echo $records->priority;?></div>-->	
			<div class="agile-detail-bs"><?php echo $this->session->userdata('languageArray')['Status']?>:- <?php echo $records->status;?></div>	
			</li>
            <?php
            		}
            	}
            }
            ?> 	                
			</ul>									
		</div>
	</div>
</div>
<!-- Mainly scripts -->
<?php $this->load->view('common/footerscript');?>
<!--Drag drop-->
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"   integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="   crossorigin="anonymous"></script>
<!-- Example jQuery code (JavaScript)  -->
<script type="text/javascript">
$(document).ready(function(){
	$('#page-wrapper .sortable-list').sortable({
		connectWith: '#page-wrapper .sortable-list',
		placeholder: 'placeholdersd',
	});
});
var $sortable = $(".sortable-list");     	            
//console.log($sortable);
  	$sortable.sortable({  	
    	stop: function (event, ui){      	
      	status = ui.item.closest('ul').attr('id');
      	issue_id = ui.item.closest('li').attr('id');      	            
      	//alert(status);      	
      	//alert(issue_id);      	
      	//$(this).addClass("ibox-title");    	

        var url = "<?php echo site_url('Scrum/update_status');?>"; 
         	$.ajax({
                type: "post",
                url: url,
                datatype:'json',
                data: {issue_id:issue_id,status:status},
                success: function() {                                  
                 // alert('Status Updated Succesfully');
                }
        	});  
    	}
	}); 
</script>	
</body>
</html>