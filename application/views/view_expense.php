<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Expenses']?> </title>
<?php $this->load->view('common/headerscript');?>
</head>
<body class="no-skin-config">
<div class="loader"></div>
    <div id="wrapper">   
        <div id="page-wrapper" class="gray-bg">  
        <div class="row border-bottom"></div>      
            <div class="row wrapper line_buttom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?php echo $this->session->userdata('languageArray')['Expenses']?></h2>                    
                </div>
                <div class="col-lg-2" style="vertical-align:bottom;">                
                <h2><a class="btn btn-primary " href="<?php echo base_url();?>Menus/view_expenses"><?php echo $this->session->userdata('languageArray')['Add New Expense']?></a></h2>
                </div>
            </div>           
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">                  
                    <div class="ibox-content">
                    <table class="table table-bordered table-hover dataTables-example" >         
                    <thead>
                    <tr>
                        <th><?php echo $this->session->userdata('languageArray')['Expense Purpose']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Amount']?></th>                                            
                        <th><?php echo $this->session->userdata('languageArray')['Expense Date']?></th>                   
                        <th><?php echo $this->session->userdata('languageArray')['Actions']?></th>
                    </tr>
                    </thead>
                    <tbody>
                   <?php foreach($results as $records){?>
                    <tr>
                        <td><?php echo $records->exp_purpose;?></td>
                        <td><?php echo $records->exp_amount;?></td>                       
                        <td class="center"><?php echo $records->exp_date ;?></td>                    
                        <td>                         
                        <a href="<?php echo base_url();?>Menus/edit_expenses/<?php echo $records->exp_id;?>">
                        <button type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit" aria-label="Left Align">
                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                        </button></a>
                        <a href="<?php echo base_url();?>Menus/show_expenses/<?php echo $records->exp_id ;?>"><button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="View" aria-label="Left Align"><span class="fa fa-eye" aria-hidden="true"></span></button></a>
                        <button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="deleteTicket('<?php echo $records->exp_id ;?>');" aria-label="right Align"> 
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        </button>                    
                        </td>
                    </tr>
                  <?php }?>  
                     </tbody>                   
                    </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
<?php $this->load->view('common/footerscript');?>
<script type="text/javascript">
function deleteTicket(ticket_id)
{       
    var result=confirm("Do you really want to delete the Ticket?");
    if(result==true)
        {
            location.href="<?php echo base_url();?>Menus/delete_ticket/"+ticket_id;
        }
    else
        {
            return false;
        }
    return;
}
</script>
</body>
</html>