<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Cancel']?></title>
<?php $this->load->view('common/headerscript');?>
</head>
<body>
<!-- <div class="loader"></div> -->
	<div id="wrapper">
		<div id="page-wrapper" class="gray-bg">	
		<div class="row border-bottom"></div>		
		<?php include 'projectmenu.php';?> 
			<div class="row wrapper line_buttom white-bg page-heading">
				<div class="col-lg-12">
					<h2><?php echo $this->session->userdata('languageArray')['Add Project Files']?></h2>
				</div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-12 animated fadeInRight">
						<div class="ibox-content">
							<form class="form-horizontal" id="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="<?php echo base_url();?>Projects/save_files">
								
								<div class="form-group">
									<label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Upload Project Files']?></label>
									<div class="col-lg-6">
										<input type="file" name="userfile" class="form-control">
									</div>
								</div>
								<?php 
								$csrf = array(
								    'name' => $this->security->get_csrf_token_name(),
								    'hash' => $this->security->get_csrf_hash()
								);
								?>
								<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />	
								
								<div class="form-group form-controlcenter">                
				                    <button type="submit" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Upload']?></button>
				                    <button type="reset" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Cancel']?></button>  
				                </div> 				               
							</form>
								
								<table class="table table-bordered table-hover dataTables-example" >         
                    <thead>
                    <tr>
                        <th><?php echo $this->session->userdata('languageArray')['Project Files']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Uploaded By']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Uploaded On']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Actions']?></th>
                    </tr>
                    </thead>
                    <tbody>
                   <?php foreach($results as $records){?>
                    <tr >
                        <td><a href='<?php echo base_url();?>Menus/downloadFile/<?=$records->file_name?>'><?php echo $records->file_name;?></a></td>
                        
                        
                        <td><?php echo $records->user_name ;?>
                        </td>
                        <td><?php echo $records->created_date ;?> </td>
                                                
                        <td>
                        <button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="deleteFiles('<?php echo $records->file_name ;?>');" aria-label="right Align"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>                   
                        </td>
                    </tr>
                  <?php }?>  
                     </tbody>
                    <tfoot>                   
                    </tfoot>
                    </table>
								
													
				                
						</div>
					</div>
				</div>
			</div>
		</div>
<?php $this->load->view('common/footerscript');?>
<script type="text/javascript">
    function deleteFiles(name)
    {       
        var result=confirm("Do you really want to delete the file "+ name+" ?");
        if(result==true)
            {
            location.href="<?php echo base_url();?>Menus/delete_files/"+name;
            }
        else
            {
                return false;
            }
        return;
    }
</script>
</body>
</html>