<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Edit User']?> </title>
    <?php $this->load->view('common/headerscript');?>
</head>
<body class="no-skin-config">
<div class="loader"></div>
    <div id="wrapper">    
        <div id="page-wrapper" class="gray-bg"> 
        <div class="row border-bottom"></div>       
            <div class="row wrapper line_buttom white-bg page-heading">
                <div class="col-lg-12">
                    <h2><?php echo $this->session->userdata('languageArray')['Add User']?></h2>                    
                </div>                
            </div>
    <div class="wrapper wrapper-content animated fadeInRight">           
        <div class="row">       
        <div class="col-lg-12 animated fadeInRight">
            <div class="ibox-content">
            <?php foreach($results as $user){?>      
        <form class="form-horizontal"  role="form" method="POST" enctype="multipart/form-data"  action="<?php echo base_url();?>Projects/edit_user/<?php echo $user->username; ?>">
            <?php 
               $csrf = array(
        				'name' => $this->security->get_csrf_token_name(),
        				'hash' => $this->security->get_csrf_hash()
				);
            ?>
		<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>"/>      
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Name']?> *</label>
                	<div class="col-lg-6">
                   	<input type="fullname" name="fullname" class="form-control" value="<?php echo $user->fullname ; ?>" required="required">
                 	</div>
                </div>                
                 <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['User Name']?> *</label>
                	<div class="col-lg-6">
                    <input type="username" name="username" class="form-control" value="<?php echo $user->username ; ?>" required="required">
                    </div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Password']?> *</label>
                	<div class="col-lg-6">
                    <input type="password" name="password" class="form-control" value="<?php echo $user->password ; ?>" required="required">
                 	</div>
                </div>                           
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Organization']?> *</label>
                	<div class="col-lg-6">
                     <input type="organization" name="organization" class="form-control" value="<?php echo $user->organization ; ?>" required="required">
                   </div>
                </div>                
                 <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Email']?></label>
                	<div class="col-lg-6">
                    <input type="email" name="email" class="form-control" value="<?php echo $user->email ; ?>">
                    </div>
                </div>   
                <?php
                $dtype = "";
                if($mode=="view") 
                {
                    $dtype = "disabled";
                }
                ?>              
                 <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Role']?> *</label>
                	<div class="col-lg-6">
                   		<select name="role" class="form-control" <?php echo $dtype;?>>
                   		<?php
                   		if($user->user_type_id == '1'){?>
  	                    <option value="1" selected="selected"> Admin </option>
	                    <option value="3" > Client </option>
	                    <option value="2" > User </option>
  	                  <?php } 
                  	   elseif ($user->user_type_id == '2'){?>
  	                    <option value="1" > Admin </option>
	                    <option value="3" > Client </option>
	                    <option value="2" selected="selected"> User </option>
  	                  <?php  }  
						else{?>
  	                    <option value="1" > Admin </option>
	                    <option value="3" selected="selected"> Client </option>
	                    <option value="2" > User </option>
  	                  <?php  }  ?>
	              	 	</select>                  		
                    </div>
                </div>
                <?php 
                if ($user->user_type_id == '3'){?>
                 <div class="form-group" id="clientiddiv" style="display:block;">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Client Name']?></label>
                	<div class="col-lg-6">
                   <select name="clientid">
                   	<?php 
	                    foreach($clients as $clientdetails){ 
	                    	if($user->client_id==$clientdetails->client_id)
	                    	{?>
		                    <option value="<?php echo $clientdetails->client_id?>" selected="selected"> <?php echo $clientdetails->organization_name?> </option>
		                    <?php }
		                    else{
		                    ?>
		                    <option value="<?php echo $clientdetails->client_id?>" > <?php echo $clientdetails->organization_name?> </option>
		                    <?php }}?>
	              	 	</select>
                    </div>
                </div> 
                <?php }?>
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Photo']?></label>
                	<div class="col-lg-6">
                    <input type="file" name="userfile" class="form-control"  >
                    </div>
                </div>
                <?php
                if($mode!="view") 
                {
                ?>              
                <div class="form-group form-controlcenter">                
                    <button type="submit" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Submit']?></button>
                    <button type="reset" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Cancel']?></button>  
                </div> 
                <?php
                }
                ?>          
            </form>
    <?php }?>
            </div>   
        </div>
    </div>        
</div>
</div>   
<?php $this->load->view('common/footerscript');?>   
<script>
$(document).ready(function(){
    $("#wizard").steps();
    $("#form").steps({
        bodyTag: "fieldset",
        onStepChanging: function (event, currentIndex, newIndex)
        {
            if (currentIndex > newIndex)
            {
                return true;
            }                
            if (newIndex === 3 && Number($("#age").val()) < 18)
            {
                return false;
            }
            var form = $(this);                    
            if (currentIndex < newIndex)
            {                        
                $(".body:eq(" + newIndex + ") label.error", form).remove();
                $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
            }                  
            form.validate().settings.ignore = ":disabled,:hidden";                   
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {                  
            if (currentIndex === 2 && Number($("#age").val()) >= 18)
            {
                $(this).steps("next");
            }                  
            if (currentIndex === 2 && priorIndex === 3)
            {
                $(this).steps("previous");
            }
        },
        onFinishing: function (event, currentIndex)
        {
            var form = $(this);
            form.validate().settings.ignore = ":disabled";                  
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            var form = $(this);                  
            form.submit();
        }
    }).validate({
        errorPlacement: function (error, element)
        {
            element.before(error);
        },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    });
});
</script>
</body>
</html>