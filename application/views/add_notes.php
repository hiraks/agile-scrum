<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Add Note']?> </title>
   <?php 
   $this->load->view('common/headerscript');
   if(!empty($notes_details))
    {
       foreach($notes_details as $records){
       $notes_id = $records->notes_id;       
       $notes_title = $records->notes_title;
       $note_description = $records->note_description;
       $priority = $records->priority;
       }
    }
   ?>
<body>
    <div class="loader"></div>
    <div id="wrapper">   
        <div id="page-wrapper" class="gray-bg">   
        <div class="row border-bottom"></div>                     
            <div class="row wrapper line_buttom white-bg page-heading">
                <div class="col-lg-12">
                    <h2><?php echo $this->session->userdata('languageArray')['Add Note']?> </h2>                                       
                </div>                
            </div>        
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12 animated fadeInRight">
                <div class="ibox float-e-margins">                  
                <div class="ibox-content">
            <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url();?>Menus/notes_add" id="form-horizontal">
            <input type="hidden" name="notes_id" class="form-control" required="required" value="<?php echo (!empty($notes_id))?$notes_id:''; ?>">
                <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Note Title']?>  *</label>
                	<div class="col-lg-6">
                    <input type="text" name="notes_title" class="form-control" required="required" value="<?php echo (!empty($notes_title))?$notes_title:''; ?>">
                    </div>
                </div>                          
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Description']?> *</label>                  
                    <div class="input-group col-lg-6">
                        <textarea id="event_description" name="note_description" class="form-control" required="required"><?php echo (!empty($note_description))?$note_description:''; ?></textarea>                
                    </div>                 
                </div> 
                 <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Priority']?> </label>                  
                    <div class="input-group col-lg-6">
                        <select id="event_color" name="priority" class="form-control">
                            <option value="">--Select Priority--</option>
                            <option value="Major" <?php echo (!empty($priority)=='Major')?'selected="selected"':''; ?>><?php echo $this->session->userdata('languageArray')['Major']?> Major</option>
                            <option value="Minor" <?php echo (!empty($priority)=='Minor')?'selected="selected"':''; ?>><?php echo $this->session->userdata('languageArray')['Minor']?> Minor</option>
                            <option value="Trivial" <?php echo (!empty($priority)=='Trivial')?'selected="selected"':''; ?>><?php echo $this->session->userdata('languageArray')['Trivial']?> </option>                            
                        </select>                                             
                    </div>                 
                </div>                        
                <?php 
                    $csrf = array(
            			'name' => $this->security->get_csrf_token_name(),
            			'hash' => $this->security->get_csrf_hash()
    				);
                ?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <div class="form-group form-controlcenter">                
                    <button type="submit" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Submit']?> </button>
                    <button type="reset" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Cancel']?> </button>                 
                </div>      
            </form>
                </div>
                </div>
            </div>
            </div>       
        </div>
    </div> 
</body>
<?php $this->load->view('common/footerscript');?>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>-->
<!--<script>
    $(document).ready(function(){

        $(window).load(function() {
            $(".loader").fadeOut("slow");
        });
 });
 </script>-->
</html>