<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Ceeate Invoice']?> </title>    
    <?php $this->load->view('common/headerscript');?>
</head>
<body>
<div class="loader"></div>
    <div id="wrapper">    
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom"></div>
            <div class="row wrapper line_buttom white-bg page-heading">
                <div class="col-lg-12">
                    <h2><?php echo $this->session->userdata('languageArray')['Ceeate Invoice']?></h2>                    
                </div>               
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">           
            <div class="row">          
                <div class="col-lg-12 animated fadeInRight">
                    <div class="ibox-content">
                    <form class="form-horizontal" id="form-horizontal" role="form" method="POST"  action="<?php echo base_url();?>Projects/create_invoice">                
                    <?php 
                        $csrf = array(
    						'name' => $this->security->get_csrf_token_name(),
    						'hash' => $this->security->get_csrf_hash()
    				    );
                    ?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />    
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Invoice Id']?> *</label>
                	<div class="col-lg-6">
                   	<?php 
	                    foreach($invoiceId as $invoice){?>
                    <input type="invoice_id" name="invoice_id" class="form-control" value="<?php echo ($invoice->invoice_id+1) ;?>"  required="required"/>
                    <?php }?>                   	
                 	</div>
                </div>                
                <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Client Name']?> *</label>
                	<div class="col-lg-6">
                	 <select name="client_id" class="form-control" required="required">	
                        <option value="">--Select--</option>				
                	<?php 
	                foreach($clientdetails as $client){?>
	                    <option value="<?php echo ($client->client_id);?>"><?php echo ($client->organization_name);?></option>                   
                    <?php }?>                	
                   </select>
                    </div>
                </div>                
                <div class="form-group" id="data_1">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Due Date']?> *</label>
                	<div class="input-group date col-lg-6">
                    <input id="enddate" type="enddate" name="enddate" class="form-control"  required="required">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                 	</div>               	
                </div>  
                <!--<div class="form-group">
                    <label class="col-lg-4 control-label">Tax Percent *</label>
                	<div class="col-lg-6">
                    <input type="tax" name="tax" class="form-control"  required="required">
                 	</div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label">Discount Percent *</label>
                	<div class="col-lg-6">
                    <input type="discount" name="discount" class="form-control"  required="required">
                 	</div>
                </div>-->                
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Default Currency']?> *</label>
                	<div class="col-lg-6">
                    <select name="currency" class="form-control" required="required">
                        <option value="">--Select--</option>
						<option value="USD">USD</option>
						<option value="EURO">EURO</option>
						<option value="GBP">GBP</option>
						<option value="AUD">AUD</option>						
					</select>                   
                 	</div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Notes']?>Notes</label>
                	<div class="col-lg-6">
                        <textarea name="notes" rows="3" class="form-control"></textarea>         
                 	</div>
                </div>                           
               <!--  <div class="form-group">
                    <label class="col-lg-3 control-label">Remaining Estimate</label>
                	<div class="col-lg-5">
                    <input type="rtime" name="rtime" class="form-control"  required="required">
                    </div>
                </div> -->               
                <div class="form-group form-controlcenter">                
                    <button type="submit" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Submit']?></button>
                    <button type="reset" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Cancel']?></button>  
                </div>         
            </form>
            </div>    
        </div>
    </div>
</div>
</div>
<?php $this->load->view('common/footerscript');?>
</body>
</html>