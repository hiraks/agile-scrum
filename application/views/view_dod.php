<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Definition Of Done']?> </title>
<?php  $this->load->view('common/headerscript');?>
</head>
<body class="no-skin-config">
<!-- <div class="loader"></div> -->
    <div id="wrapper">   
        <div id="page-wrapper" class="gray-bg">  
        <div class="row border-bottom"></div>      
            <div class="row wrapper line_buttom white-bg">
				<div class="col-lg-10">
					<h2><?php echo $this->session->userdata('languageArray')['Definition Of Done (DoD)']?></h2>
				</div>
				<div class="col-lg-2" style="vertical-align:bottom;">             
                <h2><a class="btn btn-primary " data-toggle="modal" data-target="#myModal5" ><?php echo $this->session->userdata('languageArray')['Closed']?>Add a DoD</a></h2>
               <!--  <h2><a class="btn btn-primary " data-toggle="modal" data-target="#myModal4" href="<?php // echo base_url();?>Menus/add_dod">Add a DoD</a></h2> -->
                </div>
			</div>
			
 <div  class="col-xs-2 col-md-2 col-sm-2 col-lg-2 white-bg">
				
			</div>
			
			
			 <div  id="results" class="col-xs-5 col-md-5 col-sm-5 col-lg-5 white-bg">
			 <div id="goodPointsListStart"></div>	
				<ul class="sortable-list agile-list" id="goodPointsList"
					style="height: 100%;">
             <?php $this->load->view('add_dod');?>
       
                                                     
			</ul>
			<div id="goodPointsListEnd"></div>		
			</div>
			
			
			
			<div  class="col-xs-4 col-md-4 col-sm-4 col-lg-4 white-bg">
				
			
			</div>
			
			
			
			 <div class="modal" id="myModal5" role="dialog" style="display: none; padding-right: 17px;">
                                <div class="modal-dialog">
                                    <div class="modal-content animated ">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">x</span><span class="sr-only"><?php echo $this->session->userdata('languageArray')['Close']?></span></button>
                                            
                                            <h4 class="modal-title"><?php echo $this->session->userdata('languageArray')['Add DoD']?></h4>
                                            
                                        </div>
                                        <div class="modal-body">
                                        <p>
                                     
                                           
                                            <form class="form-horizontal" id="form-horizontal" role="form" method="POST" enctype="multipart/form-data" >               
                  
                                                <div class="form-group">
                                                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['DoD Item / Description']?> *</label>
                                                    <div class="col-lg-8">
                                                    <textarea type="description" name="description" id="description" class="form-control" rows="5" cols="12" required="required" onkeyup="resizeTextarea('description')"><?php echo (!empty($description))?$description:''; ?></textarea>
                                                    </div>
                                                </div>
                                               
                                                <div class="form-group form-controlcenter">                
                                                    <button type="button" class="btn btn-primary" onclick="submitDod()"><?php echo $this->session->userdata('languageArray')['Submit']?></button>
                                                    <button type="reset" class="btn btn-warning"><?php echo $this->session->userdata('languageArray')['Cancel']?></button>  
                                                    
                                                           
                                                </div>       
                                           </form> 
        
                                           
                                           
                                           
                                            </p>
                                        </div>
                                        <!-- <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
			

		</div>
	</div>
	<!-- Mainly scripts -->
<?php $this->load->view('common/footerscript');?>
<!--Drag drop-->
	<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"   integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="   crossorigin="anonymous"></script>
	<!-- Example jQuery code (JavaScript)  -->
	<script type="text/javascript">
	function toggleTextArea(textid,id)
	{

		 var x = document.getElementById(textid);
		 var y = document.getElementById(id);
		    x.style.display = "block";
		    y.style.display = "none";
	

		}
	
	
function resizeTextarea (id) {
	  var a = document.getElementById(id);
	  a.style.height = 'auto';
	  a.style.height = a.scrollHeight+'px';
	}

$(document).ready(function(){

	 
	
	$('#page-wrapper .sortable-list').sortable({
		connectWith: '#page-wrapper .sortable-list',
		placeholder: 'placeholdersd',
	});
});

var $sortable = $(".sortable-list");     	            
//console.log($sortable);
  	$sortable.sortable({  	
    	stop: function (event, ui){      	
      	status = ui.item.closest('ul').attr('id');
      	issue_id = ui.item.closest('div').attr('id'); 
      	var p = $('#'+issue_id);
      	var position = p.offset();
		var topposition=position.top;
	 	//alert("status="+status);
	 	 var count=0;
	 	 var divs='';
	 	 var divPosition='';
	 	 var markerVar='';
	 	
     // 	var order = $("ul#goodPointsList").sortable("serialize");
      	$('ul.sortable-list.agile-list.ui-sortable').each(function (){
      		var j = $(this).attr('id');

      		
      		});
      		
      	 $('div').each(function () {
      		var divId = $(this).attr('id');
      		if(divId !='')
            {
      			
         	
         	if(status =='goodPointsList')
         	{
         	 
                 	if(divId == 'goodPointsListEnd')
                 	 {
                		return false;
                 	 }
        
                 	 if(markerVar == 'goodPointsListStart' )
                  	 {
                     
                        divs=divs+divId+'~';
                        divPosition=divPosition+count+'~';
                     
                  	 } 
                  	        
                 count++;
                 if(divId == 'goodPointsListStart')
              	 {
             		markerVar='goodPointsListStart';
              	 }
                 
         		} 
            }
            
      	});
     
      

        var url = "<?php echo site_url('Menus/save_dod_position');?>"; 
        
         	$.ajax({
                type: "post",
                url: url,
                datatype:'json',
                data: {divids:divs,divposition:divPosition},
                success: function(data) {
                  if(status =='goodPointsList')
                    {
                       	 //$('#InputTextArea0').val('');
                         $('#goodPointsList').fadeIn().html(data);
                        // $('#good-texta').hide();
                    }
                    
                    
                }
        	});  
    	}
	}); 

  	
  
	  
  	function submitGoodFunction(id,textareaid) {
      	var descText=document.getElementById(textareaid+id).value;
    	var url = "<?php echo site_url('Menus/update_dod');?>"; 

      	
        $.ajax({
        	 url: url,
        	 async: false,
             type: "POST",
             data: {dod_id:id,description: descText },
             dataType: "html",
             success: function(data) {
            	 $('#'+textareaid+id).val('');
			     $('#goodPointsList').fadeIn().html(data);
                 $('#good-texta').hide();
            	 
             }
 		})
   }

  	function deleteGoodFunction(id)
  	{

  	var confirmDelete=confirm("Are you sure you want to delete ?");
  	if(confirmDelete)
  	{
  	     var url = "<?php echo site_url('Menus/delete_dod');?>"; 
  	      	
  	        $.ajax({
  	        	 url: url,
  	        	 async: false,
  	             type: "POST",
  	             data: {dod_id:id },
  	             dataType: "html",
  	             success: function(data) {
  	            	
  	            		$('#InputTextArea'+id).val('');
    	               $('#goodPointsList').fadeIn().html(data);
  	                   $('#good-texta').hide();
  	            	 	  	            	
  	             }
  	 		})

  	      }	
  	  	}
  	
	
	function submitDod() {
	
		//jQuery.noConflict();
		if ($('#description').val() == '') {
		    $('#description').css('border-color', 'red');
		    return false;
		}
		 	
	  	var descText=document.getElementById("description").value;
		var url = "<?php echo site_url('Menus/save_dod');?>"; 

		        $.ajax({
		        	 url: url,
		        	 async: false,
		             type: "POST",
		             data: {description:descText },
		             dataType: "html",
		             success: function(data) {
		            	 
		            	 $('#description').val('');
		            	 
		                     $('#goodPointsList').fadeIn().html(data);
		                     $("#myModal5").modal("hide");
		                     $('body').removeClass('modal-open');
		                     $('.modal-backdrop').remove();
		             }  	
		 		})
		   }



	
</script>
</body>
</html>