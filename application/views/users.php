<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Users']?> </title>
    <?php $this->load->view('common/headerscript');?>    
</head>
<body class="no-skin-config">
<div class="loader"></div>
    <div id="wrapper">   
        <div id="page-wrapper" class="gray-bg">   
        <div class="row border-bottom"></div>     
            <div class="row wrapper line_buttom white-bg">
                <div class="col-lg-10">
                    <h2><?php echo $this->session->userdata('languageArray')['Users']?></h2>
                </div>
                <div class="col-lg-2">
				<h2><a class="btn btn-primary " href="<?php echo base_url();?>Menus/add_user" ><?php echo $this->session->userdata('languageArray')['Add User']?></a></h2>
                </div>
            </div>           
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">                  
                    <div class="ibox-content">
                    <table class="table table-bordered table-hover dataTables-example" >        
                   <thead>
                    <tr>
                        <th><?php echo $this->session->userdata('languageArray')['Full Name']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Username']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Role']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Created On']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Organization']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Actions']?></th>
                    </tr>
                    </thead>
                    <tbody>
                   <?php foreach($results as $records){?>
                    <tr >
                        <td><div class="team-members"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$records->filename);?>"/>&nbsp;<?php echo $records->fullname ;?></div></td>
                        <td><?php echo $records->username ;?>
                        </td>
                        <td><?php echo $records->user_role ;?></td>
                        <td class="center"><?php echo $records->created_date ;?></td>
                        <td class="center"><?php echo $records->organization ;?></td>
                        <td>                         
                        <a href="<?php echo base_url();?>Menus/view_user_by_username/<?php echo $records->username ;?>"><button type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit" aria-label="Left Align"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></a>
                        <a href="<?php echo base_url();?>Menus/view_user_by_username/<?php echo $records->username ;?>/view"><button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="View" aria-label="Left Align"><span class="fa fa-eye" aria-hidden="true"></span></button></a>
						<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="deleteUser('<?php echo $records->username ;?>');" aria-label="right Align">
 						<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
						</button>						
						</td>
                    </tr>
                  <?php }?>  
                    </tbody>
                    <tfoot>                   
                    </tfoot>
                    </table>
                    </div>
                </div>
            </div>
            </div>
            <div class="row"></div>
       
        </div>
        </div>
   <?php $this->load->view('common/footerscript');?>
<script type="text/javascript">
    function deleteUser(name)
    {       
        var result=confirm("Do you really want to delete the user ?");
        if(result==true)
            {
            location.href="<?php echo base_url();?>Menus/delete_user/"+name;
            }
        else
            {
                return false;
            }
        return ;
    }
    </script>
</body>
</html>