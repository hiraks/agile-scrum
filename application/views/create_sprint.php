<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $this->session->userdata('languageArray')['Create Sprint']?> </title>
<?php $this->load->view('common/headerscript');?>
<body class="no-skin-config">
<!--<div class="loader"></div>-->
    <div id="wrapper">
    <div id="page-wrapper" class="gray-bg"> 
    <div class="row border-bottom"></div>
    <?php include 'projectmenu.php';?> 
        <div class="row wrapper line_buttom white-bg">
            <div class="col-lg-12">
                <h2><?php echo $this->session->userdata('languageArray')['Create Sprint']?></h2>
            </div>                
        </div>  
    <div class="col-lg-12 animated fadeInRight">
        <div class="ibox"> 
            <div class="ibox-content">
            <?php 
            if ($error_count==TRUE)
            {
            ?>
            <div class="alert alert-danger">
               <?php echo $this->session->userdata('languageArray')['Sprint already exists for the time frame']?> 
            </div>
            <?php }?>
            <form class="form-horizontal" id="form-horizontal" role="form" method="POST" action="<?php echo base_url();?>Scrum/create_sprints">
                <?php 
                    $csrf = array(
            			'name' => $this->security->get_csrf_token_name(),
            			'hash' => $this->security->get_csrf_hash()
    				);
                ?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>"/>
                <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Sprint Id']?> *</label>
                	<div class="col-lg-6">
                	<?php 
	                    foreach($sprintId as $sprint){?>
                    <input type="sprint_id" name="sprint_id" class="form-control" value="<?php echo ($sprint->sprint_id+1) ;?>" required="required"/>
                    <?php }?>
                    </div>
                </div>           
                <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Sprint Title']?> *</label>
                	<div class="col-lg-6">
                    <input type="title" name="title" class="form-control" required="required"/>
                    </div>
                </div>               
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Drag Issues/ Stories from Right to Left']?></label>
                    <div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 blue-bg" style="overflow: scroll; width:200px; height:300px;">         
                        <select id="sd" multiple="multiple" class="sortable-list form-control" name="issues[]">
                            <option value="">--Drag Issues/Stories Here--</option>
                        </select> 
                        <ul class="sortable-list agile-list gray-bg" style="height:100%;" id="listissue">
                        </ul>                   
                    </div> 
                    <div class="col-xs-4 col-md-4 col-sm-4 col-lg-4" style="overflow:scroll; width:200px; height:300px;">         
                        <ul class="sortable-list agile-list grey-bg" style="height:100%;">
                        <?php
                        // print_r($results);                                           
                        foreach($issueList as $records){
                        ?>
                            <li id="<?php echo $records->issue_id;?>">
                            <a href="#"><?php echo $records->summary; ?></a>                
                            </li>                                       
                        <?php
                           }               
                        ?>                                               
                        </ul>                    
                    </div> 
                </div>                           
                <div class="form-group" id="data_1">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Start Date']?> *</label>
                	<div class="input-group date col-lg-6">
                	 	<input id="stdate" type="stdate" name="stdate" class="form-control"  required="required"/>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>                   
                </div>
                <div class="form-group" id="data_1">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['End Date']?> *</label>
                	<div class="input-group date col-lg-6">
                	  <input id="enddate" name="enddate" class="form-control"  required="required">
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>                                       
                </div> 
                 
                
                
                
                <div class="form-group" >
                 	 <label class="col-lg-4 control-label"></label>
                	   <div class="col-lg-6 ">
                	   
                    	   	<input type="radio" value="option1" name="a" ><?php echo $this->session->userdata('languageArray')['All tasks apart from completed']?> </input>
                    	  
                    	   
                	   	 </div>
                </div>
                 <div class="form-group" >
                 	 <label class="col-lg-4 control-label"></label>
                	   <div class="col-lg-6 ">
                	   
                    	   	<input type="radio" value="option1" name="a"><?php echo $this->session->userdata('languageArray')['Will do it manually']?> </input>    
                    	  
                    	   
                	   	 </div>
                </div>
                                                          
                           	               
                <div class="form-group form-controlcenter">                
                    <button type="submit" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Submit']?></button>
                    <button type="reset" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Cancel']?></button>                 
                </div>       
            </form>
        </div>                
        </div>
    </div>       
    </div>
</div>   
<?php $this->load->view('common/footerscript');?> 
<!--Drag drop-->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui-1.8.custom.min.js"></script>

<script src="<?php echo base_url();?>jassets/Angular/js/plugins/ui-sortable/sortable.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $("#sd").hide();
    $('#page-wrapper .sortable-list').sortable({
        connectWith: '#page-wrapper .sortable-list',
        placeholder: 'placeholder',
    });
});
var $sortable = $(".sortable-list");  
    $sortable.sortable({    
        stop: function (event, ui){   
        status = ui.item.closest('ul').attr('id');      
        issue_id = ui.item.closest('li').attr('id');                    
        issue_name = ui.item.closest('li').text();                 
        //alert(issue_id);     
       // alert(issue_name); 
        if(status == 'listissue') 
        {
            $("#sd").append('<option value="'+issue_id+'" selected="selected">'+issue_name+'</option>');
            $("select#sd li:contains('"+issue_name+"')").remove();
        }
        else
        {
            $("#sd option[value='"+issue_id+"']").remove(); 
            $("select#sd li:contains('"+issue_name+"')").remove();
        }
    }
}); 
</script> 
 
</body>
</html>