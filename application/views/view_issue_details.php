<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['View Issue Details']?></title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">
<script type="text/javascript">
    function test(title,username,issue_id,logged_estimate,sprint_id)
    {      
        $(".modal-body #userid").val( username );
        $(".modal-body #title").val( title );
        $(".modal-body #issue_id").val( issue_id );
        $(".modal-body #logged_estimate").val( logged_estimate );
        $(".modal-body #sprint_id").val( sprint_id );     
    }
</script> 
</head>
<body class="no-skin-config">
<div class="loader"></div>
    <div id="wrapper">
    <div id="page-wrapper" class="gray-bg"> 
    <div class="row border-bottom"></div>
        <div class="row wrapper line_buttom white-bg page-heading">
            <div class="col-lg-12">
                <h2><?php echo $this->session->userdata('languageArray')['View Issue Details']?></h2>
            </div>                
        </div>  
        <?php include 'projectmenu.php';?>           
    <div class="col-lg-12 white-bg animated fadeInRight">
        <div class="ibox">                  
            <div class="ibox-content">
            <?php             
                $issue_title=""; 
                foreach($results as $records){ 
            ?>
            <div>
            <a class="btn  btn-warning" href="<?php echo base_url();?>Scrum/set_status/<?php echo $records->issue_id?>/Progress/" ><?php echo $this->session->userdata('languageArray')['In Progress']?></a>
            <a class="btn btn-primary" href="<?php echo base_url();?>Scrum/set_status/<?php echo $records->issue_id?>/Resolved/" ><?php echo $this->session->userdata('languageArray')['Resolved']?></a>
            <a class="btn  btn-info" href="<?php echo base_url();?>Scrum/set_status/<?php echo $records->issue_id?>/Reopened/" ><?php echo $this->session->userdata('languageArray')['Reopened']?></a>
            <a class="btn  btn-success" href="<?php echo base_url();?>Scrum/set_status/<?php echo $records->issue_id?>/Closed/" ><?php echo $this->session->userdata('languageArray')['Closed']?></a>
            <!-- <a class="btn  btn-warning" href="<?php //echo base_url();?>Menus/SubMenu/<?php //echo $this->session->userdata('project_id')?>/timesheet" >Log Work</a>-->
            <a class="btn  btn-warning" onclick="test('<?php echo $records->summary; ?>','<?php echo $this->session->userdata('user_name');?>','<?php echo $records->issue_id ;?>','<?php echo $records->logged_estimate ;?>','<?php echo $records->sprint_id ;?>');" href="#modal-form" data-toggle="modal" ><?php echo $this->session->userdata('languageArray')['Log Work']?></a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="btn btn-primary" href="<?php echo base_url();?>Scrum/edit_issues/<?php echo $records->issue_id?>/" ><?php echo $this->session->userdata('languageArray')['Edit']?></a>
            <a class="btn btn-danger" href="<?php echo base_url();?>Scrum/delete_issues/<?php echo $records->issue_id?>/" ><?php echo $this->session->userdata('languageArray')['Delete']?></a>
            </div>            
            <div class="col-md-7">
                <h4><?php echo $this->session->userdata('languageArray')['Details']?></h4>
                <dl class="dl-horizontal m-t-md"> 
                    <dt><?php echo $this->session->userdata('languageArray')['Type']?>:</dt>
                    <dd><?php if($records->issue_type_id=='1') {echo  $this->session->userdata('languageArray')['Bug'];} elseif($records->issue_type_id=='2') {echo $this->session->userdata('languageArray')['Story'];} elseif($records->issue_type_id=='3') {echo $this->session->userdata('languageArray')['Epic'];} else {echo $this->session->userdata('languageArray')['Change Request'];}?></dd>
                    <dt><?php echo $this->session->userdata('languageArray')['Priority']?>:</dt>
                    <dd><?php echo($records->priority);?></dd>            
                    <dt><?php echo $this->session->userdata('languageArray')['Attachments']?>:</dt>
                    <dd><a href="<?php echo base_url();?>Scrum/download_file/<?php echo($records->filename);?>"><?php echo($records->filename);?></a></dd>
                    <dt><?php echo $this->session->userdata('languageArray')['Status']?>:</dt>
                    <dd> 
                    <?php 
                        if( $records->status=='Progress') {?>
                            <span class="badge badge-warning pull-lef"><?php echo $this->session->userdata('languageArray')['In Progress']?></span>
                        <?php }?>                    
                        <?php if( $records->status=='Resolved') {?>
                            <span class="badge badge-primary pull-lef"><?php echo($records->status);?></span>
                        <?php }?>            
                        <?php if( $records->status=='Reopened') {?>
                            <span class="badge badge-info pull-lef"><?php echo($records->status);?></span>
                        <?php }?>            
                        <?php if( $records->status=='Closed') {?>
                            <span class="badge badge-success pull-lef"><?php echo($records->status);?></span>
                        <?php }?>            
                        <?php if( $records->status=='Not Started') {?>
                            <span class="badge badge-danger pull-left"><?php echo($records->status);?></span> 
                        <?php }?>
                    </dd>
                </dl>
                <h4><?php echo $this->session->userdata('languageArray')['Description']?></h4>
                <dd><?php echo($records->description);?></dd>
            </div>           
            <div class="col-md-5">
                <h4>People</h4>
                <dl class="dl-horizontal m-t-md"> 
                    <dt><?php echo $this->session->userdata('languageArray')['Reported By']?>:</dt>
                    <dd><?php echo($records->reported_by);?></dd>
                    <dt><?php echo $this->session->userdata('languageArray')['Assigned to']?>:</dt>
                    <dd><?php echo($records->assigned_to);?></dd>
                    <dt><?php echo $this->session->userdata('languageArray')['Estimate']?>:</dt>
                    <dd><?php echo($records->original_estimate);?></dd>
                    <dt><?php echo $this->session->userdata('languageArray')['Remaining Estimate']?>:</dt>
                    <dd><?php echo(($records->original_estimate)-($records->logged_estimate));?></dd>
                    <dt><?php echo $this->session->userdata('languageArray')['Sprint Name']?>:</dt>
                    <dd><?php echo($records->sprint_id);?></dd>
                </dl>
            </div>
            <?php } ?> 
            </div> 
           </div>
          </div>
          
          <div class="col-lg-12 white-bg animated fadeInRight">
        <div class="ibox">                  
            <div class="ibox-content">     
            
                    
            <div class="col-md-12">
                <h3><?php echo $this->session->userdata('languageArray')['Logged Hours']?></h3>
                <table class="table">   
                    <thead>
                        <tr>
                            <th><?php echo $this->session->userdata('languageArray')['Date']?></th>
                            <th><?php echo $this->session->userdata('languageArray')['Hours']?></th>
                            <th><?php echo $this->session->userdata('languageArray')['Description']?></th>
                        </tr>
                    </thead>
                    <tbody> 
                    <?php foreach($resultsTimesheet as $records){?>
                    <tr>
                        <td><?php echo $records->logged_date ;?></td>
                        <td><?php echo $records->logged_hours ;?></td>
                        <td><?php if($records->description !='') { echo $records->description ;} else {echo $issue_title;}?></td>        
                        <!-- <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Details" aria-label="Left Align">
                                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                            </button>
                        <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Delete" aria-label="right Align">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        </button>
                         -->    
                    </tr>
                    <?php }?>    
                    </tbody>        
                </table>
            </div>
            </div>
        </div>
    </div>      
<?php $this->load->view('common/footerscript');?>
</body>
</html>