<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>SCRUM | Events</title>
<?php $this->load->view('common/headerscript');?>
<link href='<?php echo base_url();?>assets/fullcalendar/fullcalendar.min.css' rel='stylesheet'/>
<link href='<?php echo base_url();?>assets/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print'/>
<script src='<?php echo base_url();?>assets/fullcalendar/lib/moment.min.js'></script>
<script src='<?php echo base_url();?>assets/fullcalendar/lib/jquery.min.js'></script>
<script src='<?php echo base_url();?>assets/fullcalendar/fullcalendar.min.js'></script>
<style>
    #calendarbs {
        max-width: 900px;
        margin: 0 auto;
    }
</style>
</head>
<body class="no-skin-config">
<!--<div class="loader"></div>-->
    <div id="wrapper">   
        <div id="page-wrapper" class="gray-bg">  
        <div class="row border-bottom"></div>      
            <div class="row wrapper line_buttom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Events</h2> 
                    <input type="hidden" name="user_id" id="user_id" value="<?php $user_id = $this->session->userdata('user_id'); echo $user_id; ?>">      
                </div>
                <div class="col-lg-2">                
                    <h2><a class="btn btn-primary" href="<?php echo base_url();?>Menus/add_events">Add a new Events</a></h2>
                </div>
            </div>
            <!--<div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">   
                    <div id="modal-form" aria-hidden="true"></div>                            
                        <div class="ibox-content">
                          <div id='calendarbs'> </div> 
                        </div>
                    </div>
                </div>                
            </div>-->
            <div id="editevent"></div>
        </div>        
    </div>    
</body>
<?php //$this->load->view('common/footerscript');?>
<script>
    $(document).ready(function(){

      var format = function (time, format) {
            var t = new Date(time);
            var tf = function (i) { return (i < 10 ? '0' : '') + i };
            return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
                switch (a) {
                    case 'yyyy':
                        return tf(t.getFullYear());
                        break;
                    case 'MM':
                        return tf(t.getMonth() + 1);
                        break;
                    case 'mm':
                        return tf(t.getMinutes());
                        break;
                    case 'dd':
                        return tf(t.getDate());
                        break;
                    case 'HH':
                        return tf(t.getHours());
                        break;
                    case 'ss':
                        return tf(t.getSeconds());
                        break;
                }
            })
        }
       //alert(format(new Date(1513681074000).getTime(), 'MM/dd/yyyy HH:mm:ss'));

        var user_id = $("#user_id").val();          
        var url = "<?php echo site_url('Menus/events_details');?>"; 
               $('#calendarbs').fullCalendar({
                    //defaultDate: '2017-11-12',
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    events: function(start, end, timezone, callback) {
                      $.ajax({
                        type: "post",
                        url: url,
                        datatype:'json',
                        data: {user_id:user_id},
                        success: function(json) {
                          //alert(json);                             
                          var obj = jQuery.parseJSON(json);                             
                          var events = [];
                          //print_r(obj);
                          $.each(obj, function(index, value) {
                            //var kk = value['event_title'];
                            //alert(kk);
                            events.push({
                              id: value['event_id'],                              
                              title: value['event_title'],
                              start: value['event_startdate'],                                  
                              end: value['event_enddate'],
                              description: value['event_description'],
                              backgroundColor: value['event_color']                             
                            });                                                 
                          });
                          callback(events);
                        }
                });                 
            },                                  
            eventClick: function(calEvent) {                        
               /* var output = '';
                for (var entry in calEvent) {
                  output += 'key: ' + entry + ' | value: ' + calEvent[entry] + '\n';
                }
                alert(output);
                */            
                //alert(calEvent.start);
                kk = calEvent.title;
                estartdate = calEvent.start;
                eenddate = calEvent.end;
                edescription = calEvent.description;
                color = calEvent.backgroundColor; 

              $("#editevent").html('<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url();?>Projects/events_update" id="form-horizontal"><input type="hidden" name="event_id" value="'+calEvent.id+'"><div class="form-group"><label class="col-lg-4 control-label">Event Title *</label><div class="col-lg-6"><input type="text" name="event_title" class="form-control" required="required" value="'+kk+'"></div></div><div class="form-group"><label class="col-lg-4 control-label">Event Start Date *</label><div class="input-group col-lg-6"><input type="text" id="event_startdate" name="event_startdate" class="form-control" required="required" value="'+format(new Date(calEvent.start).getTime(), 'yyyy/MM/dd HH:mm:ss')+'"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div></div><div class="form-group" id="data_1"><label class="col-lg-4 control-label">Event End Date *</label><div class="input-group date col-lg-6"><input type="text" id="event_enddate" name="event_enddate" class="form-control" required="required" value="'+format(new Date(calEvent.end).getTime(), 'yyyy/MM/dd HH:mm:ss')+'"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div></div><div class="form-group"><label class="col-lg-4 control-label">Description *</label><div class="input-group col-lg-6"><textarea id="event_description" name="event_description" class="form-control" required="required">'+edescription+'</textarea></div></div><div class="form-group"><label class="col-lg-4 control-label">Event Color</label><div class="input-group col-lg-6"><select id="event_color" name="event_color" class="form-control"><option value="">--Select Color--</option><option value="Green">Green</option><option value="Blue">Blue</option><option value="Red">Red</option><option value="Yellow">Yellow</option><option value="Black">Black</option><option value="Pink">Pink</option><option value="Orange">Orange</option></select></div></div><div class="form-group form-controlcenter"><button type="submit" class="btn btn-primary">Submit</button>&nbsp;<button type="reset" class="btn btn-primary">Cancel</button></div></form>'); 
              $('#event_color').val(color).find("option[value=" + color +"]").attr('selected', true);          
            },
            error: function(){                      
                alert('Error while request..');
            }
        })      
    });

function getFormattedDate(date) {
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear().toString().slice(2);
    var kkl = day + '-' + month + '-' + year;  
    //return kkl;    
    return day + '-' + month + '-' + year;             
}
$('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

</script>
</html>