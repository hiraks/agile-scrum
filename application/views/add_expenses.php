<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Add Expenses']?> </title>
  <?php 
    $this->load->view('common/headerscript');
       
        //dumpEx($ticket_details);
    if(!empty($expenses_details))
    {
       foreach($expenses_details as $records){
       $exp_id = $records->exp_id;  
       $exp_amount = $records->exp_amount;     
       $exp_purpose = $records->exp_purpose;
       $exp_description = $records->exp_description;
       $exp_date = $records->exp_date;            
       }
    }
  ?>
</head>
<body>
<div class="loader"></div>
    <div id="wrapper">   
        <div id="page-wrapper" class="gray-bg">  
        <div class="row border-bottom"></div>               
            <div class="row wrapper line_buttom white-bg page-heading">
                <div class="col-lg-12">
                    <h2><?php echo $this->session->userdata('languageArray')['Add Expenses']?></h2>                                       
                </div>                
            </div>  
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12 animated fadeInRight">
                <div class="ibox float-e-margins">                  
                <div class="ibox-content">
                <form class="form-horizontal" id="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="<?php echo base_url();?>Menus/expenses_add">
                <input type="hidden" name="exp_id" value="<?php echo (!empty($exp_id))?$exp_id:''; ?>"/>    
                <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Expense Purpose']?> *</label>
                    <div class="col-lg-6">
                    <input type="text" name="exp_purpose" class="form-control" value="<?php echo (!empty($exp_purpose))?$exp_purpose:''; ?>" required="required">
                    </div>
                </div>
                <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Expense Description']?> *</label>
                    <div class="col-lg-6">
                    <textarea name="exp_description" class="form-control" required="required"><?php echo (!empty($exp_description))?$exp_description:''; ?></textarea>
                    </div>
                </div>                                 
                <div class="form-group" id="data_1">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Expense Date']?> *</label>
                    <div class="input-group col-lg-6 date">
                        <input type="text" id="exp_date" name="exp_date" class="form-control" required="required" value="<?php echo (!empty($exp_date))?$exp_date:''; ?>">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Expense Amount']?> *</label>
                    <div class="col-lg-6">
                    <input type="text" name="exp_amount" class="form-control" value="<?php echo (!empty($exp_amount))?$exp_amount:''; ?>" required="required">
                    </div>
                </div>
                <div class="form-group form-controlcenter">                
                    <button type="submit" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Submit']?></button>
                    <button type="reset" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Cancel']?></button>                 
                </div>       
            </form>
        </div>                
        </div>
        </div>        
    </div>
</div>   
<?php $this->load->view('common/footerscript');?>   
</body>
</html>