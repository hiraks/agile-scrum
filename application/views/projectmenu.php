<div class="content">                   
    <section class="header">        
        <!-- <nav class="navbar navbar-default-bs"> -->
            <div class="container">       
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav " id="pmenu"> 
                      
                      <?php if ($_GET["active"] =='dashboard'){?>  
                        <li><a class="active" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/dashboard?active=dashboard"> <i class="fa fa-inbox "></i><?php echo  $this->session->userdata('languageArray')['Project Overview'] ?> </a></li>
                      <?php } else {?>
                      <li><a class="kk" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/dashboard?active=dashboard"> <i class="fa fa-inbox "></i><?php echo $this->session->userdata('languageArray')['Project Overview']?></a></li>
                      <?php }?> 
                      <?php if ($_GET["active"] =='backlog'){?>  
                         <li><a class="active" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/backlog?active=backlog"> <i class="fa fa-suitcase"></i><?php echo $this->session->userdata('languageArray')['Back Log']?> </a></li>
                       <?php }else {?>
                           <li><a class="kk" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/backlog?active=backlog"> <i class="fa fa-suitcase"></i> <?php echo $this->session->userdata('languageArray')['Back Log']?></a></li>
                       <?php }?> 
                       <?php if ($_GET["active"] =='issues'){?>     
                        <li><a class="active" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/issues?active=issues"> <i class="fa fa-envelope-o"></i> <?php echo $this->session->userdata('languageArray')['Issues / Story']?></a></li>
                       <?php } else {?>
                       <li><a class="kk" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/issues?active=issues"> <i class="fa fa-envelope-o"></i> <?php echo $this->session->userdata('languageArray')['Issues / Story']?></a></li>  
                       <?php }?>          
                      <?php if ($_GET["active"] =='sprint'){?>      
                        <li><a class="active" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/sprint?active=sprint"> <i class="fa fa-list-alt"></i> <?php echo $this->session->userdata('languageArray')['Sprint']?></a></li> 
                      <?php } else {?>
                      <li><a class="kk" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/sprint?active=sprint"> <i class="fa fa-list-alt"></i> <?php echo $this->session->userdata('languageArray')['Sprint']?></a></li> 
                      <?php }?>  
                      <?php if ($_GET["active"] =='retro'){?>
                        <li><a class="active" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/retro?active=retro"> <i class="fa fa-list-alt"></i> <?php echo $this->session->userdata('languageArray')['Retrospective']?></a></li>
                      <?php } else {?>  
                        <li><a class="kk" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/retro?active=retro"> <i class="fa fa-list-alt"></i><?php echo $this->session->userdata('languageArray')['Retrospective']?></a></li>
                      <?php }?>  
                      <?php if ($_GET["active"] =='timesheet'){?>
                        <li><a class="active" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/timesheet?active=timesheet"> <i class="fa fa-clock-o"></i> <?php echo $this->session->userdata('languageArray')['Timesheets']?> </a></li>
                      <?php } else {?>  
                        <li><a class="kk" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/timesheet?active=timesheet"> <i class="fa fa-clock-o"></i> <?php echo $this->session->userdata('languageArray')['Timesheets']?> </a></li>
                        <?php }?>
                        <?php if ($_GET["active"] =='reports'){?>
                        <li><a class="active" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/reports?active=reports"> <i class="fa fa-line-chart"></i> <?php echo $this->session->userdata('languageArray')['Reports']?></a></li> 
                       <?php } else {?>
                        <li><a class="kk" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/reports?active=reports"> <i class="fa fa-line-chart"></i> <?php echo $this->session->userdata('languageArray')['Reports']?></a></li>
                        <?php }?>
                        <?php if ($_GET["active"] =='files'){?>
                        <li><a class="active" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/files?active=files"> <i class="fa fa-line-chart"></i> <?php echo $this->session->userdata('languageArray')['Files']?></a></li>
                       <?php } else {?>
                       <li><a class="kk" href="<?php echo base_url();?>Menus/SubMenu/<?php echo $this->session->userdata('project_id')?>/files?active=files"> <i class="fa fa-line-chart"></i> <?php echo $this->session->userdata('languageArray')['Files']?></a></li>  
                       <?php }?>
                    </ul> 
                </div>
            </div>
        <!-- </nav> -->
    </section>
</div>