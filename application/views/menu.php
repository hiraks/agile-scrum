<?php  
$data=$this->session->userdata('first_level_menu');
$results1=companyDetails(1);
$resultsk=$results1[0];
//dumpEx($results);
$logo_pic=$resultsk['logo'];
$company_name=$resultsk['company_name'];
?>
<nav class="navbar-default navbar-static-side" role="navigation">
  <div class="sidebar-collapse">
    <ul class="nav" id="side-menu">
      <li class="nav-header">
        <div class="dropdown profile-element"> 
          <img alt="image" class="img-circle-bs" src="<?php echo(base_url()."./uploads/".$logo_pic);?>"/>
          <a style="text-align: center;" data-toggle="dropdown" class="dropdown-toggle" href="#"  title="Delete">
          <span class="block m-t-xs"><strong><?php echo $company_name; ?></strong>
          </span></a>
        </div>
        <div class="logo-element">Scrum</div>
      </li>          
      <?php 
       foreach ($data['menu_name'] as $item) {
      ?>
      <li data-toggle="dropdown" class="dropdown-toggle" href="#" title='<?php echo $item;?>'>
      <?php
         echo $item;
      ?>           
      </li>
      <?php
       }            
      ?>
    </ul>      
  </div>
</nav>