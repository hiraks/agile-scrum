<script>
function loadLanguage()
{

	location.replace('dashboard?lang='+document.getElementById('language').value);
	
}

</script>

<?php
    $id = $this->session->userdata('user_id');           
    $msg_count = message_count($id);                                    
    $msgc = $msg_count[0];
?>
<nav class="navbar navbar-static-top gray-bg" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header" style="margin-left: 16%">        
        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="#" ng-click="minimalize()"><i class="fa fa-bars"></i></a>
    </div>
   
    <ul class="nav navbar-top-links navbar-right">         
       <li>
       		 <select class="select2_demo_1 form-control" name="language" id="language" onchange="loadLanguage()">
              <option value="">Select</option>
              <?php if($this->session->userdata('selectedLanguage')=='en'){ ?>
              <option value="en" selected="selected">English</option>
              <option value="es" >Spanish</option>
              <?php } else if($this->session->userdata('selectedLanguage')=='es'){ ?>
              <option value="en" >English</option>
              <option value="es" selected="selected">Spanish</option>
              <?php }?>
        	</select>
       
       </li>
       <li>&nbsp;</li>
        <li>
            <div class="team-members">
                <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$this->session->userdata('pic_name'));?>"/>                               
                <i class="fa fa-avatar"></i><?php echo $this->session->userdata('languageArray')['Welcome']?>  <?php echo $this->session->userdata('user_name'); ?>    
            </div>                    
        </li> 
        <li uib-dropdown>
            <a class="count-info" href="<?php echo base_url();?>Menus/Messages" >
                <i class="fa fa-envelope"></i><span class="label label-warning"><?php echo $msgc['mc'];?></span>
            </a> 
        </li>        
        <li>
            <a  href="<?php echo base_url();?>Login/logout">
                <i class="fa fa-sign-out"></i><?php echo $this->session->userdata('languageArray')['Log out']?>
            </a>
        </li>                
    </ul>
</nav>
<!--
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <span minimaliza-sidebar></span>
        </div>
        <ul class="nav navbar-top-links navbar-right">    
            <li>
                <a ui-sref="login">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
            <li>
                <a ng-click="$root.rightSidebar = !$root.rightSidebar">
                    <i class="fa fa-tasks"></i>
                </a>
            </li>
        </ul>
    </nav>
</div>
-->