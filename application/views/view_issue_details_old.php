<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Actions']?>View Issue Details</title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">  
     <link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">
<script type="text/javascript">
   // $(document).on("click", ".btn", function () {
    function test(title,username,issue_id,logged_estimate,sprint_id)
    {      
        $(".modal-body #userid").val( username );
        $(".modal-body #title").val( title );
        $(".modal-body #issue_id").val( issue_id );
        $(".modal-body #logged_estimate").val( logged_estimate );
        $(".modal-body #sprint_id").val( sprint_id );     
    }
</script> 
</head>
<body class="no-skin-config">
    <div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom ">
            <div class="col-lg-12">
				<h2><?php echo $this->session->userdata('languageArray')['Actions']?>View Details</h2>
		    </div>            
        </div>        
<?php 
include 'projectmenu.php';
  $issue_title=""; 
  foreach($results as $records){ 
?>
<div class="col-lg-12 animated fadeInRight">
    <div class="ibox">                  
        <div class="ibox-content">
<h2 class="font-bold m-b-xs">
    <?php $issue_title=$records->summary; echo($records->summary);?>
</h2>     
<div>
<a class="btn  btn-warning" href="<?php echo base_url();?>Scrum/set_status/<?php echo $records->issue_id?>/Progress/" >In Progress</a>
<a class="btn btn-primary" href="<?php echo base_url();?>Scrum/set_status/<?php echo $records->issue_id?>/Resolved/" >Resolved</a>
<a class="btn  btn-info" href="<?php echo base_url();?>Scrum/set_status/<?php echo $records->issue_id?>/Reopened/" >Reopened</a>
<a class="btn  btn-success" href="<?php echo base_url();?>Scrum/set_status/<?php echo $records->issue_id?>/Closed/" >Closed</a>
<!-- <a class="btn  btn-warning" href="<?php //echo base_url();?>Menus/SubMenu/<?php //echo $this->session->userdata('project_id')?>/timesheet" >Log Work</a>-->
<a class="btn  btn-warning" onclick="test('<?php echo $records->summary; ?>','<?php echo $this->session->userdata('user_name');?>','<?php echo $records->issue_id ;?>','<?php echo $records->logged_estimate ;?>','<?php echo $records->sprint_id ;?>');" href="#modal-form" data-toggle="modal" >Log Work</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a class="btn btn-primary" href="<?php echo base_url();?>Scrum/edit_issues/<?php echo $records->issue_id?>/" >Edit</a>
<a class="btn btn-danger" href="<?php echo base_url();?>Scrum/delete_issues/<?php echo $records->issue_id?>/" >Delete</a>
</div>
<div class="col-md-7">
<h4>Details</h4>
    <dl class="dl-horizontal m-t-md small"> 
        <dt>Type:</dt>
        <dd><?php if($records->issue_type_id=='1') {echo 'Bug';} elseif($records->issue_type_id=='2') {echo 'Story';} elseif($records->issue_type_id=='3') {echo 'Epic';} else {echo 'Change Request';}?></dd>
        <dt>Priority:</dt>
        <dd><?php echo($records->priority);?></dd>            
        <dt>Attachment:</dt>
        <dd><a href="<?php echo base_url();?>Scrum/download_file/<?php echo($records->filename);?>"><?php echo($records->filename);?></a></span> </a></dd>
        <dt>Status:</dt>
        <dd> 
        <?php 
            if( $records->status=='Progress') {?>
                <span class="badge badge-warning pull-lef">In Progress</span></li>
            <?php }?>                    
            <?php if( $records->status=='Resolved') {?>
                <span class="badge badge-primary pull-lef"><?php echo($records->status);?></span>
            <?php }?>            
            <?php if( $records->status=='Reopened') {?>
                <span class="badge badge-info pull-lef"><?php echo($records->status);?></span>
            <?php }?>            
            <?php if( $records->status=='Closed') {?>
                <span class="badge badge-success pull-lef"><?php echo($records->status);?></span>
            <?php }?>            
            <?php if( $records->status=='Not Started') {?>
                <span class="badge badge-danger pull-left"><?php echo($records->status);?></span> 
            <?php }?>
        </dd>
    </dl>
    <h4>Description</h4>
    <dd><?php echo($records->description);?></dd>
</div>
<div class="col-md-5">
<h4>People</h4>
    <dl class="dl-horizontal m-t-md small"> 
        <dt>Reported By:</dt>
        <dd><?php echo($records->reported_by);?></dd>
        <dt>Assigned to:</dt>
        <dd><?php echo($records->assigned_to);?></dd>
        <dt>Estimate:</dt>
        <dd><?php echo($records->original_estimate);?></dd>
        <dt>Remaining Estimate:</dt>
        <dd><?php echo(($records->original_estimate)-($records->logged_estimate));?></dd>
        <dt>Sprint Name:</dt>
        <dd><?php echo($records->sprint_id);?></dd>
    </dl>
</div>
<?php } ?>         
<div class="col-md-12">
<h4>Logged Hours</h4>
    <table class="table">   
        <thead>
            <tr>
                <th>Date</th>
                <th>Hours</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody> 
   <?php foreach($resultsTimesheet as $records){?>
        <tr>
            <td><?php echo $records->logged_date ;?></td>
            <td><?php echo $records->logged_hours ;?></td>
            <td><?php if($records->description !='') { echo $records->description ;} else {echo $issue_title;}?></td>        
            <!-- <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Details" aria-label="Left Align">
    				<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    			</button>
    		<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Delete" aria-label="right Align">
    			<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
    		</button>
    		 -->	
        </tr>
  <?php }?>    
        </tbody>        
    </table>
    </div>
    </div>
</div>      
    <div id="modal-form" class="modal fade" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row ">
                        <div class="col"><h3 class="m-t-none m-b border-bottom">Log Time</h3>
                           <br/>
                            <form role="form" method="POST" class="form-horizontal" action="<?php echo base_url();?>Scrum/save_timesheet">
                                <div class="form-group">
								<label class="col-lg-3 control-label">User</label>
								<div class="col-lg-9">
								<input type="email" id="userid" name="userid" disabled="disabled" class="form-control">
								</div>
								</div>								
								<input type="hidden" name="issue_id" id="issue_id"/>
								<input type="hidden" name="logged_estimate" id="logged_estimate"/>
                                <div class="form-group">
									<label class="col-lg-3 control-label">Issue</label>
									<div class="col-lg-9">
									<input type="email" id="title" disabled="disabled" class="form-control">
									</div>
								</div>								
								<div class="form-group" id="data_1">
									<label class="col-lg-3 control-label">Date</label>
									<div class="col-lg-9">
									<div class="input-group date">
           								<input type="text" id="logged_date" name="logged_date" class="form-control" value="">
           								 <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        							</div>
									</div>
								</div>                                
                                <div class="form-group">
									<label class="col-lg-3 control-label">Worked</label>
									<div class="col-lg-9">
									<input type="text"  class="form-control" id="logged_hours" name="logged_hours">
									</div>
								</div>								
								<div class="form-group">
									<label class="col-lg-3 control-label">Description</label>
									<div class="col-lg-9">
									<textarea id="description" name="description" rows="6" cols="25"></textarea> 
									</div>
								</div>
                                <input type="hidden"  id="sprint_id" name="sprint_id">
                                <div>
                                    <button class="btn btn-sm btn-primary pull-center m-t-n-xs" type="submit"><strong>Submit</strong></button>         
                                </div>
                            </form>
                        </div>                           
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script> 
    <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script> 
    <script src="<?php echo base_url();?>assets/js/SCRUM.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>    
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.responsive.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.tableTools.min.js"></script>   
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>
</body>
</html>