<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $this->session->userdata('languageArray')['Message Dashboard']?> </title>
<?php $this->load->view('common/headerscript');?>
</head>
<body class="no-skin-config">
<div class="loader"></div>
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">  
    <div class="row border-bottom"></div>       
        <div class="row wrapper line_buttom white-bg page-heading">
            <div class="col-lg-12">
                <h2><?php echo $this->session->userdata('languageArray')['Message Dashboard']?></h2>
            </div>
        </div>
        <?php include('common/message_menu.php');?>              
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-6 col-sm-6">                              
                <div class="ibox-content">      
            <table class="table table-bordered table-hover dataTables-example" id="s">
                <thead>
                    <tr>
                        <th></th>                
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($results as $records){?>
                    <tr id="<?php echo $records->message_id;?>">                      
                        <td>
                        <?php 
                        $id = $records->message_send_by;
                        $user_dtl = userDetails($id);
                        //dump($user_dtl);                        
                        $user_name=$user_dtl[0];
                        ?>
                       <div id="solTitle"> <a href="#<?php echo $records->message_id;?>" class="message-font"><div class="team-members"> <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$user_name['filename']);?>"/>&nbsp;<?php echo $user_name['fullname'] ;?><span>&nbsp;<?php echo $records->message_created_on;?></span><br>                       
                        <span class="alignbs"><?php echo $records->message_title;?><br>
                        <span class="alignbs"><?php echo $records->message_descrription;?></span></div></a>
                        </div>
                        </td>                                                 
                        <!--<td>                       
                        <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="deletemessageinbox('<?php //echo $records->message_id;?>');" aria-label="right Align">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        </button>                       
                        </td>-->
                    </tr>
                  <?php }?> 
                 </tbody>
                    <tfoot>                   
                    </tfoot>
                    </table>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">                              
                    <div class="ibox-content" id="msb">             
                        <i class="fa fa-comment" aria-hidden="true" style="font-size: 72px;"></i>   
                    </div>
                </div>
            </div>       
        </div>
    </div>
</body>
<?php $this->load->view('common/footerscript');?>
</html>