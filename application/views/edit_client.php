<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Edit Client']?></title>
    <?php $this->load->view('common/headerscript');?>
<script type="text/javascript">
function CancelFunction()
{
	window.location="<?php echo base_url();?>Menus/Clients";
}
</script>
</head>
<body class="no-skin-config">
<div class="loader"></div>
    <div id="wrapper">    
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom"></div>
            <div class="row wrapper line_buttom white-bg">
                <div class="col-lg-12">
                    <h2><?php echo $this->session->userdata('languageArray')['Edit Client']?></h2>                    
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">           
            <div class="row">         
            <div class="col-lg-12 animated fadeInRight">
            <div class="ibox-content">
              <?php foreach($results as $client){?>      
                <form class="form-horizontal"  role="form" method="POST"  action="<?php echo base_url();?>Clientdetailscontroller/edit_client/<?php echo $client->client_id; ?>" enctype="multipart/form-data">                
                <?php 
               $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>"/>      
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Contact Person']?> *</label>
                	<div class="col-lg-6">
                   	<input type="contact_person" name="contact_person" class="form-control" value="<?php echo $client->contact_person ; ?>"  required="required">
                 	</div>
                </div>                
                 <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Organization Name']?> *</label>
                	<div class="col-lg-6">
                   	<input type="organization_name" name="organization_name" class="form-control" value="<?php echo $client->organization_name ; ?>"  required="required">
                 	</div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Phone No']?>. *</label>
                	<div class="col-lg-6">
                   	<input type="text" name="phone_no" class="form-control" value="<?php echo $client->phone_no ; ?>"  required="required">
                 	</div>
                </div>              
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Email']?> *</label>
                	<div class="col-lg-6">
                   	<input type="email" name="email" class="form-control" value="<?php echo $client->email ; ?>"  required="required">
                 	</div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Skype Id']?> *</label>
                  <div class="col-lg-6">
                    <input type="skype" name="skype" class="form-control" value="<?php echo $client->skype ; ?>">
                  </div>
                </div> 
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Address']?></label>
                	<div class="col-lg-6">                   	
                     <textarea id="address" name="address" type="text" class="form-control"><?php echo $client->address ; ?></textarea>
                 	</div>
                </div>       
               <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Company Logo']?> *</label>
                    <div class="col-lg-6">
                    <input type="file" id="logoimg" name="userfile" class="form-control" >
                    <img class="img-circle-bs" src="<?php echo(base_url().UPLOAD_PATH_URL."client/".$client->c_logo);?>"/>
                    </div>
                </div>
                <?php
                if($mode!="view") 
                {
                ?>
                <div class="form-group form-controlcenter">                
                    <button type="submit" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Submit']?>Submit</button>
                    <button type="reset" class="btn btn-success" onclick="CancelFunction()"><?php echo $this->session->userdata('languageArray')['Cancel']?></button>        
                </div>
                <?php
                }
                ?>                               
            </form>
<?php }?>
                </div>                
             </div>
        </div>        
    </div>
</div>
    <!-- Mainly scripts -->
<?php $this->load->view('common/footerscript');?>
    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {                   
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }                   
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }
                    var form = $(this);                
                    if (currentIndex < newIndex)
                    {                   
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }                    
                    form.validate().settings.ignore = ":disabled,:hidden";                 
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {                    
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }                  
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);                   
                    form.validate().settings.ignore = ":disabled";
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);                
                    form.submit();
                }
            }).validate({
                errorPlacement: function (error, element)
                {
                    element.before(error);
                },
                rules: {
                    confirm: {
                        equalTo: "#password"
                    }
                }
            });
       });
    </script>
</body>
</html>