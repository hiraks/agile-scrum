<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Edit Issues']?> </title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
</head>
<body class="no-skin-config">
<div class="loader"></div>
    <div id="wrapper">
    <div id="page-wrapper" class="gray-bg"> 
    <div class="row border-bottom"></div>
        <?php include 'projectmenu.php';?> 
        <div class="row wrapper line_buttom white-bg">
            <div class="col-lg-12">
                <h2><?php echo $this->session->userdata('languageArray')['Edit Issues']?></h2>                
            </div>                
        </div> 
        <div class="col-lg-12 animated fadeInRight">
            <div class="ibox-content">
        <?php foreach($issues as $results){?>
        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="<?php echo base_url();?>Scrum/set_edit_issues/<?php echo($results->issue_id)?>/">                
        <?php 
           $csrf = array(
    				'name' => $this->security->get_csrf_token_name(),
    				'hash' => $this->security->get_csrf_hash()
		);?>
		<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />                
            <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Issue Type']?> *</label>
                <div class="col-lg-6">
                    <select name="issue_type_id" class="form-control" required="required">
                    <?php if($results->issue_type_id == '1'){?>
	                    <option value="1" selected="selected" ><?php echo $this->session->userdata('languageArray')['Bug']?></option>
	                    <option value="2"><?php echo $this->session->userdata('languageArray')['Story']?></option>
	                    <option value="3"><?php echo $this->session->userdata('languageArray')['Epic']?></option>
	                    <option value="4"><?php echo $this->session->userdata('languageArray')['Change Request']?></option>
	                    <?php }	                    
	                    if($results->issue_type_id == '2') {?>
	                    <option value="1" ><?php echo $this->session->userdata('languageArray')['Bug']?></option>
	                    <option value="2" selected="selected"><?php echo $this->session->userdata('languageArray')['Story']?></option>
	                    <option value="3"><?php echo $this->session->userdata('languageArray')['Epic']?></option>
	                    <option value="4"><?php echo $this->session->userdata('languageArray')['Change Request']?></option>
	                    <?php }	                    
	                    if($results->issue_type_id == '4') {?>
	                    <option value="1" ><?php echo $this->session->userdata('languageArray')['Bug']?></option>
	                    <option value="2" ><?php echo $this->session->userdata('languageArray')['Story']?></option>
	                    <option value="3"><?php echo $this->session->userdata('languageArray')['Epic']?></option>
	                    <option value="4" selected="selected"><?php echo $this->session->userdata('languageArray')['Change Request']?></option>
	                    <?php }?>
                    </select>
                </div>
            </div>                
            <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Title']?> *</label>
                	<div class="col-lg-6">
                    <input type="title" name="title" class="form-control" required="required" maxlength="27" value="<?php echo $results->summary ; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Description']?> *</label>
                	<div class="col-lg-6">
                    <textarea type="description" name="description" class="form-control" required="required" ><?php echo $results->description ; ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Priority']?> *</label>
                	<div class="col-lg-6">
                    <select name="priority" class="form-control" required="required">
                    <?php if($results->priority == 'Major'){?>
	                    <option value="Major" selected="selected"><?php echo $this->session->userdata('languageArray')['Major']?>  *</option>
	                    <option value="Minor"><?php echo $this->session->userdata('languageArray')['Minor']?>  </option>
	                    <option value="Trivial"><?php echo $this->session->userdata('languageArray')['Trivial']?>  </option>
	                    <option value="Critical"><?php echo $this->session->userdata('languageArray')['Critical']?>   </option>
	                  <?php }   
	                  if($results->priority == 'Minor'){?>
                    <option value="Major"> <?php echo $this->session->userdata('languageArray')['Major']?> </option>
                    <option value="Minor"  selected="selected"><?php echo $this->session->userdata('languageArray')['Minor']?>  </option>
                    <option value="Trivial"><?php echo $this->session->userdata('languageArray')['Trivial']?>  </option>
                    <option value="Critical"><?php echo $this->session->userdata('languageArray')['Critical']?>   </option>
                   <?php } 
                  	if($results->priority == 'Trivial'){?>
        	          <option value="Major"> <?php echo $this->session->userdata('languageArray')['Major']?> </option>
        	          <option value="Minor" ><?php echo $this->session->userdata('languageArray')['Minor']?>  </option>
        	          <option value="Trivial"  selected="selected"><?php echo $this->session->userdata('languageArray')['Trivial']?>  </option>
        	          <option value="Critical"><?php echo $this->session->userdata('languageArray')['Critical']?>   </option>
        	          <?php }                  	          
        	          if($results->priority == 'Critical'){?>
	                    <option value="Major"> <?php echo $this->session->userdata('languageArray')['Major']?> </option>
	                    <option value="Minor" > <?php echo $this->session->userdata('languageArray')['Minor']?> </option>
	                    <option value="Trivial"> <?php echo $this->session->userdata('languageArray')['Trivial']?> </option>
	                    <option value="Critical"  selected="selected"> <?php echo $this->session->userdata('languageArray')['Critical']?>  </option>
	                  <?php }?>     
                    </select>
                 	</div>
                </div>
                 <?php }?>            
                 <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Reported By']?> *</label>
                	<div class="col-lg-6">
                    <select name="reported_by" class="form-control" required="required">
	                     <?php foreach($users as $records){?>                  
	                    <option value="<?php echo $records->username ;?>"> <?php echo $records->username ;?> </option>
	                   <?php }?>
                    </select>
                   </div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Assigned To']?></label>
                	<div class="col-lg-6">
                    <select name="assigned_to" class="form-control" required="required">
	                     <?php foreach($users as $records){?>                  
	                    <option value="<?php echo $records->username ;?>"> <?php echo $records->username ;?> </option>
	                   <?php }?>
                    </select>
                   </div>
                </div>
                 <?php foreach($issues as $results){?>
                 <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Time Estimate']?></label>
                	<div class="col-lg-6">
                    <input type="text" name="time" class="form-control" required="required" value="<?php echo $results->original_estimate ;?>">
                    </div>
                </div>
                <input type="hidden" name="project_id" id="project_id" value="<?php echo $results->project_id ;?>"/>
               <!--  <div class="form-group">
                    <label class="col-lg-3 control-label">Remaining Estimate</label>
                	<div class="col-lg-5">
                    <input type="rtime" name="rtime" class="form-control"  required="required">
                    </div>
                </div> -->
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Attachments']?></label>
                	<div class="col-lg-6">
                     <input type="file" name="userfile" class="form-control" >
                    <span class="label pull-right"><a href="<?php echo base_url();?>Scrum/download_file/<?php echo($results->filename);?>"><?php echo($results->filename);?></a></span>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" type="submit" ><?php echo $this->session->userdata('languageArray')['Submit']?></button>
             <?php }?>  
            </form>
        </div>                
        </div>
        </div>  
    </div>
</div>   
<?php $this->load->view('common/footerscript');?>   
</body>
</html>