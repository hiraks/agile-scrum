<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Client Details']?></title>
    <?php $this->load->view('common/headerscript');?>
</head>
<body>
<div id="wrapper">  
<div class="loader"></div>  
    <div id="page-wrapper" class="gray-bg">
    <div class="row border-bottom"></div>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><?php echo $this->session->userdata('languageArray')['Add Client']?></h2>                    
        </div>
        <div class="col-lg-2"></div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">           
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">                  
                    <div class="ibox-content">
                        <form id="form-horizontal" method="post" action="<?php echo base_url();?>Clientdetailscontroller" class="form-horizontal" enctype="multipart/form-data">                   
                        <div class="row">
                            <div class="col-lg-12">
                            <?php 
                               $csrf = array(
                    				'name' => $this->security->get_csrf_token_name(),
                    				'hash' => $this->security->get_csrf_hash()
                    			);
                            ?>
			                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Organization Name']?> *</label>
                                <div class="col-lg-6">
                                <input id="orgname" name="orgname" type="text" class="form-control required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Contact Person']?> *</label>
                                <div class="col-lg-6">
                                <input id="contactperson" name="contactperson" type="text" class="form-control required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Phone No']?>. *</label>
                                <div class="col-lg-6">
                                <input id="phone" name="phone" type="text" class="form-control required phone">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Email']?> *</label>
                                <div class="col-lg-6">
                                <input id="email" name="email" type="text" class="form-control required email">
                                </div>
                            </div>                            
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Skype']?> *</label>
                                <div class="col-lg-6">
                                <input id="skype" name="skype" type="text" class="form-control required ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Address']?></label>
                                <div class="col-lg-6">                                
                                <textarea id="address" name="address" type="text" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Company Logo']?> *</label>
                                <div class="col-lg-6">
                                <input type="file" id="logoimg" name="userfile" class="form-control" required="required">
                                <img class="img-circle-bs" src="<?php echo(base_url()."./uploads/".$records->c_logo);?>"/>
                                </div>
                            </div>
                            <div class="form-group form-controlcenter">                
                                <button type="submit" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Submit']?></button>
                                <button type="reset" class="btn btn-info" onClick="CancelFunction()"><?php echo $this->session->userdata('languageArray')['Cancel']?></button>        
                            </div> 
                            </div>                            
                        </div>                                     
                        </form>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
   <?php $this->load->view('common/footerscript');?>
</body>
<script>
function CancelFunction()
{
	window.location="<?php echo base_url();?>Menus/Clients";
}

</script>
</html>