<!-- Mainly scripts -->
<script src="<?php echo base_url();?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>
<!-- Data Tables -->
<script src="<?php echo base_url();?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.tableTools.min.js"></script>
<!-- Custom and plugin javascript -->
 <script src="<?php echo base_url();?>assets/js/SCRUM.js"></script> 
<script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
<!-- Jquery Validate -->
<script src="<?php echo base_url();?>assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/datepickr.min.js"></script>
<!-- Data picker -->
<script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<!-- <script src="<?php //echo base_url();?>assets/Angular/js/directives.js"></script> -->
<!--for loader
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>-->


<script>
    $(document).ready(function(){
        $(window).load(function() {
            $(".loader").fadeOut("slow");
        });

        /*$('#pmenu li a').click(function(){
            alert('123');
            $('li').removeClass("active");
            $(this).addClass("active");
        });

        var selector = '.nav li';        
        $(selector).on('click', function(){
            alert('123');
            $(selector).removeClass('active');
            $(this).addClass('active');
        });*/

        $('.dataTables-example').dataTable({
            responsive: true,
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "<?php echo base_url();?>assets/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
            }
        });
        var oTable = $('#editable').dataTable();
        /* Apply the jEditable handlers to the table */
        oTable.$('td').editable( 'http://agilescrumsolutions.com/example_ajax.php', {
            "callback": function( sValue, y ) {
                var aPos = oTable.fnGetPosition( this );
                oTable.fnUpdate( sValue, aPos[0], aPos[1] );
            },
            "submitdata": function ( value, settings ){
                return {
                    "row_id": this.parentNode.getAttribute('id'),
                    "column": oTable.fnGetPosition( this )[2]
                };
            },
            "width": "90%",
            "height": "100%"
        } );

            // process the form
    $("#solTitle a").click(function() {
        var addressValue = $(this).attr("href");
        var message_id = addressValue.substring(1, addressValue.length);
        //alert(message_id); 
            var url = "<?php echo site_url('Menus/messages_details');?>";         
            $.ajax({
                type: "post",
                url: url,
                cache: false,               
                datatype: 'json',               
                data:{message_id:message_id},
                success: function(json){                    
                    var obj = jQuery.parseJSON(json);              
                   // alert(obj.message_title);
                    //$("#message_title").html(obj.message_title);
                   // $("#message_descrription").html(obj.message_descrription);         
                    var urldoc = '<a target="_blank" href="'+'<?php echo base_url();?>'+'uploads/'+obj.message_attachment+'">'+obj.message_attachment+'</a>';          
                    //$("#message_attachment").html(urldoc);      
                    $("#msb").html('<ul style="padding: 0"><li class="list-group-item"><strong>Message Title</strong><div id="message_title">'+obj.message_title+'</div><strong>Message Description</strong><div id="message_descrription">'+obj.message_descrription+'</div><strong>Attachment</strong><div id="message_attachment">'+urldoc+'</div></li></ul><form method="POST" action="<?php echo base_url();?>Scrum/message_send" enctype="multipart/form-data"><div class="form-group"><input type="hidden" name="message_title" value="Reply of '+obj.message_title+'"><textarea class="form-control" name="message_descrription" placeholder="Write a reply..."></textarea></div><div class="form-group"><input type="file" name="userfile" class="form-control"></div><button type="submit" class="btn btn-primary">Reply</button></form>');
                    },
                error: function(){                      
                    alert('Error while request..');
                }
            }) 
        });   

     });
    $('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
    function fnClickAddRow(){
        $('#editable').dataTable().fnAddData( [
            "Custom row",
            "New row",
            "New row",
            "New row",
            "New row" ] );
    }

    $("#form-horizontal").validate();
</script>  
<script>            
	datepickr('.from-calendar-icon',{ altInput: document.getElementById('enddate') }); 
    datepickr('[title="parseMe"]');          
</script>  
<script>            
   datepickr('.from-calendar-icon',{altInput: document.getElementById('startdate')});
   datepickr('.to-calendar-icon',{altInput: document.getElementById('enddate') });      
   datepickr('[title="parseMe"]');           
</script> 

<script type="text/javascript">
function deletemessage(message_id)
{       
    var result=confirm("Do you really want to delete the user ?");
    if(result==true)
        {
            location.href="<?php echo base_url();?>Menus/delete_message/"+message_id;
        }
    else
        {
            return false;
        }
    return;
}
</script>
<script type="text/javascript">
function deletemessageinbox(message_id)
{       
    var result=confirm("Do you really want to delete the user ?");
    if(result==true)
        {
            location.href="<?php echo base_url();?>Menus/delete_message_inbox/"+message_id;
        }
    else
        {
            return false;
        }
    return;
}
</script>