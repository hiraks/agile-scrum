<div>                           
    <div class="form-group">                                        
        <a class="btn btn-primary" href="<?php echo base_url();?>Menus/add_messages"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Compose</a>
        <a class="btn btn-primary" href="<?php echo base_url();?>Menus/messages"><i class="fa fa-envelope-o"></i> Inbox</a>
        <a class="btn btn-primary" href="<?php echo base_url();?>Menus/sent_messages"><i class="fa fa-share-square-o" aria-hidden="true"></i>Sent mail</a> 
        <a class="btn btn-primary" href="<?php echo base_url();?>Menus/trash_messages"><i class="fa fa-trash" aria-hidden="true"></i>Trash</a>
    </div>                
</div>