<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<!-- Data Tables -->
<link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">


<link href="<?php echo base_url();?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/steps/jquery.steps.css" rel="stylesheet">
<!-- Morris -->
<link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
<!-- Gritter -->
<link href="<?php echo base_url();?>assets/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">


<link href="<?php echo base_url();?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/datepickr.min.css">-->

<style type="text/css">
.from-calendar-icon{
    display: inline-block;
    vertical-align: middle;
    width: 32px;
    height: 32px;
    background: url(<?php echo base_url();?>assets/img/from_calendar.png);
}
.to-calendar-icon{
    display: inline-block;
    vertical-align: middle;
    width: 32px;
    height: 32px;
    background: url(<?php echo base_url();?>assets/img/to_calendar.png);
}    
body.DTTT_Print{
    background: #fff;
}
.DTTT_Print #page-wrapper{
    margin: 0;
    background:#fff;
}
button.DTTT_button, div.DTTT_button, a.DTTT_button{
    border: 1px solid #e7eaec;
    background: #fff;
    color: #676a6c;
    box-shadow: none;
    padding: 6px 8px;
}
button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover{
    border: 1px solid #d2d2d2;
    background: #fff;
    color: #676a6c;
    box-shadow: none;
    padding: 6px 8px;
}
.dataTables_filter label{
    margin-right: 5px;
}     
</style>
<script type="text/javascript">
function ViewClient()
{
    if(document.getElementById("role").value =='3')
    {
        document.getElementById("clientiddiv").style.display = "block";
    }
    else
    {
        document.getElementById("clientiddiv").style.display = "none";
    }
}
</script>
<script type="text/javascript">   
    function test(title,username,issue_id,logged_estimate)
    {         
        $(".modal-body #userid").val(username);
        $(".modal-body #title").val(title);
        $(".modal-body #issue_id").val(issue_id);
        $(".modal-body #logged_estimate").val(logged_estimate);
    }
</script>