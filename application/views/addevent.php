<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SCRUM | Add Project</title>
   <?php 
   $this->load->view('common/headerscript');
   $reported_by = 0;
        $assigned_to = 0;
        $s_id = 0;
        //dumpEx($ticket_details);
    if(!empty($event_details))
    {
       foreach($event_details as $records){
       $ticket_id = $records->ticket_id;  
       $ticket_type_id = $records->ticket_type_id;     
       $ticket_title = $records->ticket_title;
       $priority = $records->priority;
       $description = $records->description;       
       $estimated_hours = $records->estimated_hours;       
       $reported_by = $records->reported_by;
       $assigned_to = $records->assigned_to;
       $s_id = $records->status_id;           
       }
    }

   ?>
<body>
    <div id="wrapper">   
        <div id="page-wrapper" class="gray-bg">  
        <div class="row border-bottom"></div>                
            <div class="row wrapper line_buttom white-bg page-heading">
                <div class="col-lg-12">
                    <h2>Add Event</h2>                    
                </div>                
            </div>        
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12 animated fadeInRight">
                <div class="ibox float-e-margins">                  
                <div class="ibox-content">
            <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url();?>Projects/events_add" id="form-horizontal">
                <div class="form-group">
                <label class="col-lg-4 control-label">Event Title *</label>
                	<div class="col-lg-6">
                    <input type="text" name="event_title" class="form-control" required="required">
                    </div>
                </div>                           
                <div class="form-group" id="data_1">
                    <label class="col-lg-4 control-label">Event Start Date *</label>                	
                    <div class="input-group date col-lg-6">
                        <input type="text" id="event_startdate" name="event_startdate" class="form-control" required="required">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>                 
                </div>             
                <div class="form-group" id="data_1">
                    <label class="col-lg-4 control-label">Event End Date *</label>                  
                    <div class="input-group date col-lg-6">
                        <input type="text" id="event_enddate" name="event_enddate" class="form-control" required="required">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>                 
                </div>  
                <div class="form-group">
                    <label class="col-lg-4 control-label">Description *</label>                  
                    <div class="input-group col-lg-6">
                        <textarea id="event_description" name="event_description" class="form-control" required="required"></textarea>                        
                    </div>                 
                </div> 
                 <div class="form-group">
                    <label class="col-lg-4 control-label">Event Color</label>                  
                    <div class="input-group col-lg-6">
                        <select id="event_color" name="event_color" class="form-control">
                            <option value="">--Select Color--</option>
                            <?php 
                            $gg="Green";
                            //foreach($results as $records){
                            ?>
                                <!--<option value="<?php //echo $records->color_id; ?>"><?php //echo $records->color_name; ?></option>-->
                            <?php
                            //} 
                            ?>
                            <option value="Green">Green</option>
                            <option value="Blue">Blue</option>
                            <option value="Red">Red</option>
                            <option value="Yellow">Yellow</option>
                            <option value="Black">Black</option>
                            <option value="Pink">Pink</option>
                            <option value="Orange">Orange</option>
                        </select>                                             
                    </div>                 
                </div>                        
                <?php 
                    $csrf = array(
            			'name' => $this->security->get_csrf_token_name(),
            			'hash' => $this->security->get_csrf_hash()
    				);
                ?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <div class="form-group form-controlcenter">                
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-primary">Cancel</button>                 
                </div>      
            </form>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div> 
</body>
<?php $this->load->view('common/footerscript');?>
</html>