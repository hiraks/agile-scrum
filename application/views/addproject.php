<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Add Project']?></title>
<?php 
$this->load->view('common/headerscript');
//dumpEx($project_details);
$project_id = 0;
$client_id = 0;
$strar = [];
if(!empty($project_details))
{
    foreach($project_details as $pd){
       $project_id = $pd->project_id;
       $pname = $pd->Name;
       $client_id = $pd->client_id;
       $start_date = $pd->start_date;
       $end_date = $pd->end_date;
       $resources_allocatted = $pd->resources_allocatted;
       $fixed_rate = $pd->fixed_rate;
       $hourly_rate = $pd->hourly_rate;
       $estimated_hours = $pd->estimated_hours;
       $notes = $pd->Notes;
    }
$strar = explode(',',$resources_allocatted);
}
//echo $mode;exit;
//dumpEx($strar);
?>
<body>
<div class="loader"></div>
    <div id="wrapper">   
        <div id="page-wrapper" class="gray-bg">   
        <div class="row border-bottom"></div>              
            <div class="row wrapper line_buttom white-bg page-heading">
                <div class="col-lg-12">
                    <h2><?php echo $this->session->userdata('languageArray')['Add Project']?></h2>                    
                </div>
            </div>        
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12 animated fadeInRight">
                <div class="ibox float-e-margins">                  
                <div class="ibox-content">
            <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url();?>Projects" id="form-horizontal">
            <?php
            if(is_numeric($project_id))
            {
            ?>
               <input type="hidden" name="project_id" value="<?php echo (!empty($project_id))?$project_id:''; ?>">
            <?php
            }
            ?>            
                <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Project Name']?> *</label>
                	<div class="col-lg-6">
                    <input type="pname" name="pname" class="form-control" required="required" value="<?php echo (!empty($pname))?$pname:''; ?>">
                    </div>
                </div>
                <!--<div class="form-group">
                    <label class="col-lg-2 control-label">Project Id</label>
                	<div class="col-lg-10">
                    <input type="pid" name="pid" class="form-control" required="required">
                    </div>
                </div>-->                
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Client Name']?> *</label>
                	<div class="col-lg-6">
                    <select name="clientid" class="form-control" required="required">
                    <option value="">--Select--</option>
                    <?php foreach($client_list as $client_records){?>                    
                    <option value="<?php echo $client_records->client_id ;?>" <?php echo ($client_records->client_id==  $client_id)? 'selected="selected"':''; ?>><?php echo $client_records->organization_name ;?></option>>
                    <?php }?>                    
                    </select>                    
                    </div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Allocated Resources']?> *</label>
                	<div class="col-lg-6">                    
                    <select name="allocated_res[]" class="form-control" required="required" multiple="multiple">
                    <option value="">--Select--</option>
                    <?php foreach($user_list as $user_records){?>                    
                    <option value="<?php echo $user_records->user_id ;?>" <?php echo (in_array($user_records->user_id, $strar))?'selected="selected"':''; ?>><?php echo $user_records->fullname ;?></option>>
                    <?php }?>                    
                    </select>  
                    </div>
                </div>
                <div class="form-group"  id="data_1">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Start Date']?> *</label>               	
                    <div class="input-group col-lg-6 date">
                        <input type="text" id="startdate" name="startdate" class="form-control" required="required" value="<?php echo (!empty($start_date))?$start_date:''; ?>">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>                  
                </div>               
                <div class="form-group" id="data_1">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['End Date']?> *</label>                	
                    <div class="input-group col-lg-6 date">
                        <input type="text" id="enddate" name="enddate" class="form-control" required="required" value="<?php echo (!empty($end_date))?$end_date:''; ?>">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Fixed Rate']?> *</label>
                	<div class="col-lg-6">
                    <input type="frate" name="frate" class="form-control" required="required" value="<?php echo (!empty($fixed_rate))?$fixed_rate:''; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Hourly Rate']?> *</label>
                	<div class="col-lg-6">
                    <input type="hrate" name="hrate" class="form-control" required="required" value="<?php echo (!empty($hourly_rate))?$hourly_rate:''; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Estimated Hours']?> *</label>
                	<div class="col-lg-6">
                    <input type="esthrs" name="esthrs" class="form-control" required="required" value="<?php echo (!empty($estimated_hours))?$estimated_hours:''; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Notes']?> *</label>
                	<div class="col-lg-6">
                    <textarea type="notes" name="notes" class="form-control" required="required"><?php echo (!empty($notes))?$notes:''; ?></textarea>
                    </div>
                </div>
                <?php 
                    $csrf = array(
            			'name' => $this->security->get_csrf_token_name(),
            			'hash' => $this->security->get_csrf_hash()
    				);
                ?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <?php
                //if($mode!="view") 
                //{
                ?>
                <div class="form-group form-controlcenter">                
                    <button type="submit" class="btn btn-primary"><?php echo $this->session->userdata('languageArray')['Submit']?></button>
                    <button type="reset" class="btn btn-success" onclick="CancelFunction()"><?php echo $this->session->userdata('languageArray')['Cancel']?></button>                 
                </div>
                <?php
                //}
                ?>       
            </form>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>    
<?php $this->load->view('common/footerscript');?>
</body>
<script>
function CancelFunction()
{
	window.location="<?php echo base_url();?>Menus/Projects";
}

</script>
</html>