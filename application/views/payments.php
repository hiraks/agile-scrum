<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['Payments Received']?> </title>
   <?php $this->load->view('common/headerscript');?>   
</head>
<body class="no-skin-config">
<div class="loader"></div>
    <div id="wrapper">   
        <div id="page-wrapper" class="gray-bg">  
        <div class="row border-bottom"></div>      
            <div class="row wrapper line_buttom white-bg">
                <div class="col-lg-12">
                    <h2><?php echo $this->session->userdata('languageArray')['Payments']?></h2>
                </div>               
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">                  
                    <div class="ibox-content">
                    <table class="table table-bordered table-hover dataTables-example">                  
                    <thead>
                        <tr>
                            <th><?php echo $this->session->userdata('languageArray')['Invoice Id']?></th>
                            <th><?php echo $this->session->userdata('languageArray')['Client']?></th>
                            <th><?php echo $this->session->userdata('languageArray')['Status']?></th>
                            <th><?php echo $this->session->userdata('languageArray')['Total Amt']?></th>
                            <th><?php echo $this->session->userdata('languageArray')['Due Amount']?></th>
                            <th><?php echo $this->session->userdata('languageArray')['Actions']?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($results as $records){?>
                    <tr>
                        <td><?php echo $records->invoice_id ;?></td>
                        <td><?php echo $records->organization_name ;?></td>
                        <td><?php echo $records->pay_status ;?></td>
                        <td class="center"><?php echo '$'. $records->total_amt;?></td>
                        <td class="center"><?php echo '$'. (intval($records->total_amt)-intval($records->paid_amt));?></td>
                        <td>                        
                        <!--<a href="<?php //echo base_url();?>Projects/view_invoice/<?php //echo $records->invoice_id ;?>">-->
                        <a href="<?php echo base_url();?>Projects/view_invoice_details/<?php echo $records->invoice_id ;?>">
                        <button type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit"aria-label="Left Align"><span class="fa fa-eye" aria-hidden="true"></span></button></a>		
						</td>
                    </tr>
                  <?php }?>  
                    </tbody>
                    <tfoot>                   
                    </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php $this->load->view('common/footerscript');?>
    <script type="text/javascript">
    function deleteUser(name)
    {        
        var result=confirm("Do you really want to delete the user ?");
        if(result==true)
        {
            location.href="<?php echo base_url();?>Menus/delete_user/"+name;
        }
        else
        {
            return false;
        }
        return ;
    }
    </script>
</body>
</html>