<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['View Backlog']?> </title>
    <?php $this->load->view('common/headerscript');?>
</head>
<body class="no-skin-config">
<div class="loader"></div>
    <div id="wrapper">
    <div id="page-wrapper" class="gray-bg"> 
    <div class="row border-bottom"></div>
        <?php include 'projectmenu.php';?> 
        <div class="row wrapper line_buttom white-bg page-heading">
            <div class="col-lg-12">
                <h2><?php echo $this->session->userdata('languageArray')['Backlog']?></h2>                
            </div>                
        </div>                   
    <div class="col-lg-12 animated fadeInRight">
        <div class="ibox">                  
            <div class="ibox-content">
            <table class="table table-bordered table-hover dataTables-example" >                   
                <thead>
                    <tr>
                        <th><?php echo $this->session->userdata('languageArray')['Title']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Reported By']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Assigned To']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Priority']?></th>
                        <th><?php echo $this->session->userdata('languageArray')['Logged Date']?></th> 
                        <th><?php echo $this->session->userdata('languageArray')['Status']?></th>                                   
                    </tr>
                </thead>
                <tbody>               
                   <?php foreach($results as $records){?>
                    <tr>
                        <td><a href="<?php echo base_url();?>Scrum/issue_details/<?php echo $records->issue_id ;?>" ><?php echo $records->summary ;?></a></td>
                        <td><?php echo $records->reported_by ;?>
                        </td>
                        <td><?php echo $records->assigned_to ;?></td>
                        <td class="center"><?php echo $records->priority ;?></td>
                        <td class="center"><?php echo $records->created_date ;?></td>                        
                        <?php if( $records->status=='Progress') {?>
                        <td class="center"><span class="badge badge-warning"><?php echo $this->session->userdata('languageArray')['In Progress']?></span></td>                
                        <?php }                        
                        else if( $records->status=='Resolved') {?>
                        <td class="center"><span class="badge badge-primary"><?php echo $this->session->userdata('languageArray')['Resolved']?></span></td>                   
                        <?php }                        
                        else if( $records->status=='Reopened') {?>
                        <td class="center"><span class="badge badge-info"><?php echo $this->session->userdata('languageArray')['Reopened']?></span></td>                      
                        <?php }                        
                        else if( $records->status=='Closed') {?>
                        <td class="center"><span class="badge badge-success"><?php echo $this->session->userdata('languageArray')['Closed']?></span></td>                     
                        <?php }                         
                        else {                        	
                        ?>
                        <td class="center"><span class="badge badge-danger"><?php echo $this->session->userdata('languageArray')['Not Started']?></span></td>
                        <?php }?>                        						
                    </tr>
                  <?php }?>                   
                </tbody>               
            </table>
            </div>
        </div>
    </div>      
<?php $this->load->view('common/footerscript');?>
</body>
</html>