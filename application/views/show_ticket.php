<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->session->userdata('languageArray')['View Ticket']?> </title>
  <?php 
    $this->load->view('common/headerscript');
        $reported_by = 0;
        $assigned_to = 0;
        $s_id = 0;
    if(!empty($ticket_details))
    {
       foreach($ticket_details as $records){
       $ticket_id = $records->ticket_id;  
       $ticket_type_id = $records->ticket_type_id;     
       $ticket_title = $records->ticket_title;
       $priority = $records->priority;
       $description = $records->description;       
       $estimated_hours = $records->estimated_hours;       
       $reported_by = $records->reported_by;
       $assigned_to = $records->assigned_to;
       $s_id = $records->status_id; 
       }
    }
  ?>
</head>
<body>
    <div id="wrapper">
    <div class="loader"></div>   
        <div id="page-wrapper" class="gray-bg"> 
        <div class="row border-bottom"></div>                
            <div class="row wrapper line_buttom white-bg page-heading">
                <div class="col-lg-12">
                    <h2><?php echo $this->session->userdata('languageArray')['View Ticket']?></h2>                                       
                </div>                
            </div>  
                <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12 animated fadeInRight">
                <div class="ibox float-e-margins">                  
                <div class="ibox-content">
                <form class="form-horizontal" id="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="<?php echo base_url();?>Menus/tickets_add">               
                <input type="hidden" name="ticket_id" value="<?php echo (!empty($ticket_id))?$ticket_id:''; ?>" />    
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Ticket Type']?> *</label>
                    <div class="col-lg-6">
                    <select name="ticket_type_id" class="form-control" disabled="disabled">
                    <option value="">--Select--</option>                    
                        <option value="1" <?php echo (!empty($ticket_type_id)=='1')?'selected="selected"':''; ?>><?php echo $this->session->userdata('languageArray')['Bug']?></option>
                        <option value="2" <?php echo (!empty($ticket_type_id)=='2')?'selected="selected"':''; ?>><?php echo $this->session->userdata('languageArray')['Story']?></option>                        
                        <option value="4" <?php echo (!empty($ticket_type_id)=='4')?'selected="selected"':''; ?>><?php echo $this->session->userdata('languageArray')['Change Request']?></option>         
                    </select>
                    </div>
                </div>                
                <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Ticket Title']?> *</label>
                    <div class="col-lg-6">
                    <input type="text" name="ticket_title" class="form-control" value="<?php echo (!empty($ticket_title))?$ticket_title:''; ?>" required="required">
                    </div>
                </div>
                <div class="form-group">
                <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Ticket Description']?> *</label>
                    <div class="col-lg-6">
                    <textarea type="description" name="description" class="form-control" required="required"><?php echo (!empty($description))?$description:''; ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Priority']?> *</label>
                    <div class="col-lg-6">
                    <select name="priority" class="form-control" disabled="disabled">
                        <option value="">--Select--</option>
                        <option value="Major" <?php echo (!empty($priority)=='Major')?'selected="selected"':''; ?>><?php echo $this->session->userdata('languageArray')['Major']?></option>
                        <option value="Minor" <?php echo (!empty($priority)=='Minor')?'selected="selected"':''; ?>><?php echo $this->session->userdata('languageArray')['Minor']?></option>
                        <option value="Critical" <?php echo (!empty($priority)=='Critical')?'selected="selected"':''; ?>><?php echo $this->session->userdata('languageArray')['Critical']?></option>
                    </select>
                    </div>
                </div>                           
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Reported By']?> *</label>
                    <div class="col-lg-6">
                    <select name="reported_by" class="form-control" disabled="disabled">
                        <option value="">--Select--</option>
                        <?php foreach($results as $rb){
                        $kkr = $rb->user_id;
                        ?>                    
                        <option value="<?php echo $rb->user_id;?>" <?php echo ($rb->user_id==$reported_by)?'selected="selected"':''; ?>><?php echo $rb->username;?></option>
                       <?php }?>
                    </select>
                   </div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Assigned To']?> *</label>
                    <div class="col-lg-6">
                    <select name="assigned_to" class="form-control" disabled="disabled">
                        <option value="">--Select--</option>
                        <?php foreach($results as $at){
                            $kk = $at->user_id;
                        ?>         
                        <option value="<?php echo $at->user_id ;?>" <?php  echo ( $at->user_id == $assigned_to)?'selected="selected"':''; ?>><?php echo $at->username;?> </option>
                       <?php }?>
                    </select>
                   </div>
                </div>  
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Status']?> *</label>
                    <div class="col-lg-6">
                    <select name="status_id" class="form-control" disabled="disabled">
                        <option value="">--Select--</option>
                        <?php 
                        foreach($status_list as $st){
                           $status_id = $st->status_id;                          
                        ?>         
                        <option value="<?php echo $status_id ;?>"<?php echo ($status_id == $s_id)?'selected="selected"':''; ?>><?php echo $st->status_name;?></option>
                       <?php }?>                      
                    </select>
                    </div>
                </div>                
                <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $this->session->userdata('languageArray')['Time Estimate']?> *</label>
                    <div class="col-lg-6">
                    <input type="text" name="estimated_hours" class="form-control" value="<?php echo (!empty($estimated_hours))?$estimated_hours:''; ?>" required="required">
                    </div>
                </div>     
            </form>
        </div>                
        </div>
        </div>        
    </div>
</div>   
<?php $this->load->view('common/footerscript');?>   
</body>
</html>