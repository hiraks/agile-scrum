<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $this->session->userdata('languageArray')['Project Overview']?> </title>
<?php $this->load->view('common/headerscript');?>
</head>
<body class="no-skin-config">
<div class="loader"></div>
    <div id="wrapper">
    <div id="page-wrapper" class="gray-bg"> 
    <div class="row border-bottom"></div>
		<?php include 'projectmenu.php';?> 	
			<div class="row wrapper line_buttom  page-heading">
				<div class="col-lg-10">
				 <?php foreach($results as $records){?>
					<h2><?php echo $records->Name ;?></h2>
					
				</div>
				<div class="col-lg-2">
				 	 <a class="btn btn-primary " href="<?php echo base_url();?>Menus/edit_project/<?php echo $this->session->userdata('project_id')?>" ><?php echo $this->session->userdata('languageArray')['Edit Project']?></a>
				</div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-12 animated fadeInRight">
						<div class="col-lg-7">

						<div class="row">
							<div class="col-lg-12">
								<dl class="dl-horizontal">
								

									<dt><?php echo $this->session->userdata('languageArray')['Status']?>:</dt>
									<dd>
										<span class="label label-primary"><?php echo $this->session->userdata('languageArray')['Active']?></span>
									</dd>
									<dt><?php echo $this->session->userdata('languageArray')['Created by']?>:</dt>
									<dd><?php echo $this->session->userdata('languageArray')['Admin']?></dd>
									<dt><?php echo $this->session->userdata('languageArray')['Client']?>:</dt>
									<dd>
										<a href="#" class="text-navy"> <?php echo $records->client_name ;?></a>
									</dd>
									<dt><?php echo $this->session->userdata('languageArray')['Fixed / Hourly Rate']?>:</dt>
									<dd> $<?php echo $records->fixed_rate ;?> /  $<?php echo $records->hourly_rate ;?></dd>
								</dl>
							</div>


							<div class="col-lg-12">
								<dl class="dl-horizontal">

									<dt><?php echo $this->session->userdata('languageArray')['Estimated End Date']?>:</dt>
									<dd> <?php echo $records->end_date ;?></dd>
									<dt><?php echo $this->session->userdata('languageArray')['Created On']?>:</dt>
									<dd> <?php echo $records->start_date ;?></dd>
									<dt><?php echo $this->session->userdata('languageArray')['Estimated Hours']?>:</dt>
									<dd><?php echo $records->estimated_hours ;?> hrs</dd>
									<dt><?php echo $this->session->userdata('languageArray')['Participants']?>:</dt>
									
									 <?php 
                        $userids = explode(',', $records->resources_allocatted);                 
                        $ss = count($userids);                        
                        $i=0;
                    ?>
                    <div class="team-members">
                    <?php
                        for($i=0;$i<$ss;$i++){                            
                            $rr = userDetails($userids[$i]);                
                            foreach ($rr as $userpic) 
                            {                                                              
                    ?>
                            <img class="img-circle" data-placement="top" src="<?php echo(base_url()."./uploads/".$userpic['filename']);?>"/>&nbsp;
                    <?php
                            }                                 
                        }                    
                    ?>  
									<!-- <dd class="project-people">
										<a href=""><img alt="image" class="img-circle"
											src="img/a3.jpg"></a> <a href=""><img alt="image"
											class="img-circle" src="img/a1.jpg"></a> <a href=""><img
											alt="image" class="img-circle" src="img/a2.jpg"></a> <a
											href=""><img alt="image" class="img-circle" src="img/a4.jpg"></a>
										<a href=""><img alt="image" class="img-circle"
											src="img/a5.jpg"></a>
									</dd> -->
								</dl>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12">
								<dl class="dl-horizontal">
									<dt><?php echo $this->session->userdata('languageArray')['Completed']?>:</dt>
									<dd>
										<div class="progress progress-striped active m-b-sm">
											<div style="width: 60%;" class="progress-bar"></div>
										</div>
										<small>Project completed in <strong>60%</strong>. Remaining
											close the project, sign a contract and invoice.
										</small>
									</dd>
								</dl>
							</div>
						</div>



						<div class="col-lg-12">
							<div class="row">
								<h4><?php echo $this->session->userdata('languageArray')['Project description']?></h4>
								<p><?php echo $records->Notes ;?></p>

							</div>
						</div>
						</div>
						<?php } ?>
						<div class="col-lg-5">
						<div class="col-lg-12">
								<div class="feed-activity-list">
								<?php //print_r($timeline_results);?>
								<?php foreach($timeline_results as $records){?>
									<div class="feed-element">
										<!-- <a href="#" class="pull-left"> <img alt="image"
											class="img-circle" src="img/a2.jpg">
										 -->	
									<a href="#" class="pull-left"> 	<img class="img-circle"  src="<?php echo(base_url()."./uploads/".$records->filename);?>"/>&nbsp;
											
										</a>	
										
										<div class="media-body ">
											<!-- <small class="pull-right"> --><strong><?php echo $records->fullname ;?></strong> &nbsp;<?php echo $records->created_date ;?><br><br><!-- </small> --> 
											<?php if($records->status == 'Not Started'){ echo('<span class="label badge-primary">Created </span>');}else{ echo('<span class="label badge-warning">Updated</span>');}  ;?>  &nbsp; <strong>Task: #<?php echo $records->issue_id ;?> -  </strong> <?php echo $records->summary ;?>. <br><br> <!-- <small
												class="text-muted">Today 2:10 pm - 12.06.2014</small> -->
												<strong>Status:</strong> <?php echo $records->status ;?>
											<!-- <span class="label badge-warning">Major</span>
											<span class="label badge-primary">10h </span> -->
									</div>
									<div>&nbsp;</div>
									<?php }?>
									<!-- <div class="feed-element">
										<a href="#" class="pull-left"> <img alt="image"
											class="img-circle" src="img/a3.jpg">
										</a>
										<div class="media-body ">
											<small class="pull-right">2h ago</small> <strong>Janet
												Rosowski</strong> add 1 photo on <strong>Monica Smith</strong>.
											<br> <small class="text-muted">2 days ago at 8:30am</small>
										</div>
									</div>
									<div class="feed-element">
										<a href="#" class="pull-left"> <img alt="image"
											class="img-circle" src="img/a4.jpg">
										</a>
										<div class="media-body ">
											<small class="pull-right text-navy">5h ago</small> <strong>Chris
												Johnatan Overtunk</strong> started following <strong>Monica
												Smith</strong>. <br> <small class="text-muted">Yesterday
												1:21 pm - 11.06.2014</small>
											<div class="actions">
												<a class="btn btn-xs btn-white"><i class="fa fa-thumbs-up"></i>
													Like </a> <a class="btn btn-xs btn-white"><i
													class="fa fa-heart"></i> Love</a>
											</div>
										</div>
									</div>
									<div class="feed-element">
										<a href="#" class="pull-left"> <img alt="image"
											class="img-circle" src="img/a5.jpg">
										</a>
										<div class="media-body ">
											<small class="pull-right">2h ago</small> <strong>Kim Smith</strong>
											posted message on <strong>Monica Smith</strong> site. <br> <small
												class="text-muted">Yesterday 5:20 pm - 12.06.2014</small>
											<div class="well">Lorem Ipsum is simply dummy text of the
												printing and typesetting industry. Lorem Ipsum has been the
												industry's standard dummy text ever since the 1500s. Over
												the years, sometimes by accident, sometimes on purpose
												(injected humour and the like).</div>
										</div>
									</div>
									<div class="feed-element">
										<a href="#" class="pull-left"> <img alt="image"
											class="img-circle" src="img/profile.jpg">
										</a>
										<div class="media-body ">
											<small class="pull-right">23h ago</small> <strong>Monica
												Smith</strong> love <strong>Kim Smith</strong>. <br> <small
												class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
										</div>
									</div>
									<div class="feed-element">
										<a href="#" class="pull-left"> <img alt="image"
											class="img-circle" src="img/a7.jpg">
										</a>
										<div class="media-body ">
											<small class="pull-right">46h ago</small> <strong>Mike
												Loreipsum</strong> started following <strong>Monica Smith</strong>.
											<br> <small class="text-muted">3 days ago at 7:58 pm -
												10.06.2014</small>
										</div>
									</div> -->
								</div>
							</div>
						</div>
						
						
						
					</div>
				</div>
			</div>



		</div>







	</div>

	</div>				
					
					
				
<?php $this->load->view('common/footerscript');?>
</body>
</html>