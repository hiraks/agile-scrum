<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        // Load url helper
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Loginmodel');
        //$this->load->library("security");
    }
    /**
     * Method to display login page
     */
    public function index()
    {
        $flag=$this->security->xss_clean($this->input->post('flag'));
        if($flag=='forgot')
        {
            $data['flag']='forgot';
        }
        elseif($flag=='create')
        {
            $data['flag']='create';
        }
        else {
            $data['flag']=null;
        }
        $data['error_flag']=true;
        $this->load->view('agile_login',$data);
    }
    
    /**
     * Function for installing the software
     */
    public function install()
    {
        $data['error_flag']=true;
        $this->load->view('install/install',$data);
    }
    public function installprj()
    {
        $db['production'] = array(
            'dsn'	=> '',
            'hostname' => 'localhost',
            'username' => 'root',
            'password' => '',
            'database' => 'agile',
            'dbdriver' => 'mysqli',
            'dbprefix' => '',
            'pconnect' => FALSE,
            'db_debug' => (ENVIRONMENT !== 'production'),
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'encrypt' => FALSE,
            'compress' => FALSE,
            'stricton' => FALSE,
            'failover' => array(),
            'save_queries' => TRUE
        );
        echo ("============="+base_url());
        
        $existingData=file_get_contents(base_url()."config/production/database.php");
        
        file_put_contents(base_url()."config/production/database.php",$db['production']);
        
        /*$this->load->model('Loginmodel');
         $queries=file_get_contents(base_url()."./db/"."db.sql");
         
         $this->Loginmodel->install_db_scripts($queries);
         */
        $data['error_flag']=true;
        //$this->load->view('agile_login',$data);
    }
    /**
     * Forgot password resolution.
     */
    public function forgot_password()
    {
        $this->load->view('forgotpassword');
    }
    
    /**
     *
     */
    public function reset_password()
    {
        $username=$this->security->xss_clean($this->input->post('username'));
        $email=$this->security->xss_clean($this->input->post('email'));
        $this->load->model('Loginmodel');
        $this->load->library('agileemail');
        $query=$this->Loginmodel->verify_and_reset_pwd($username,$email);
        if($query=='1')
        {
            $emailArray['to']=$email;
            $emailArray['from']="a@b.com";
            $emailArray['subject']="test";
            $emailArray['message']="test";
            //$this->load->library('Agileemail');
            $this->agileemail->sendemail($emailArray);
            $data['error_flag']=true;
            $data['flag']="forgot";
            $this->load->view('agile_login',$data);
        }
        
        else {
            $data['error_flag']=false;
            $this->load->view('forgotpassword',$data);
        }
    }
    /**
     * Method to create an account
     */
    public function create_account()
    {
        $this->load->view('createaccount');
    }
    /**
     * Method to validate login
     */
    public function validate_login()
    {
        $userName=$this->security->xss_clean($this->input->post('username'));
        $password=$this->security->xss_clean($this->input->post('password'));
        $this->load->model('Loginmodel');
        $this->load->model('Projectsmodel');
        $query=$this->Loginmodel->verify_user_login($userName,$password);
        //print_r($query);
        if($query->num_rows()== 1)
        {
            $row=$query->row_array();
            $this->session->set_userdata('user_name',$userName);
            $this->session->set_userdata('user_type_id', $row['user_type_id']);
            $this->session->set_userdata('user_id', $row['user_id']);
            $this->session->set_userdata('client_id', $row['client_id']);
            $this->session->set_userdata('pic_name', $row['filename']);
            $user_id = $this->session->userdata('user_id');
            if($row['client_id']== null or $row['client_id']=='0')
            {
                $client_id=null;
            }
            else {
                $client_id=$row['client_id'];
            }
            
            
            
            /**     **/
            $sel_lang = $this->input->get('lang');
            
            if($sel_lang == null || $sel_lang=="")
            {
                $sel_lang='en';
            }
            $resultLang=$this->Projectsmodel->get_lang($sel_lang);
            $langArray = array();
            $prop=$sel_lang.'_lang';
            for($i=0;$i<count($resultLang);$i++)
            {
                //echo "Test===".$resultLang[$i]->label_prop;
                //echo "Test===".$resultLang[$i]->es_lang;
                
                $langArray[$resultLang[$i]->label_prop]=$resultLang[$i]->$prop;
            }
            $this->session->set_userdata('languageArray', $langArray);
           // echo ($this->session->userdata('languageArray')[dashboard]);
            
            /**     **/
            
            
            
            
            
            
            
            
            
            
            $user_type_id=$this->session->userdata('user_type_id');
            $data['menu_name']=$this->Loginmodel->get_main_menu_list($user_type_id,'Dashboard');
            $this->session->set_userdata('first_level_menu',$data);
            //print_r($data);
            //array('data'=>$data);
            $this->load->model('Projectsmodel');
            //$results=$this->Projectsmodel->ViewProjectsOverview();
            //$data=array('results'=>$results);
            $data['results']=$this->Projectsmodel->view_projects_overview($client_id);
            $data['todolist']=$this->Projectsmodel->get_all_todo_list($client_id);
            $data['projectcount']=$this->Projectsmodel->get_project_count($client_id);
            $data['openIssueCount']=$this->Projectsmodel->get_open_issue_count($client_id);
            $data['closedIssueCount']=$this->Projectsmodel->get_closed_issue_count($client_id);
            $data['loggedhours']=$this->Projectsmodel->get_total_logged_hours($client_id);
            $data['lm_earnings']=$this->Projectsmodel->get_total_earnings_last_month($client_id);
            $data['lm_bill']=$this->Projectsmodel->get_total_bill_last_month($client_id);
            $data['lm_pending']=$this->Projectsmodel->get_total_pending_bill_last_month($client_id);
            $data['flag']=null;
            
            
            /*for ticket status graph*/
            $status_list = $this->Projectsmodel->display_status();
            $data['tickets'] = $this->Projectsmodel->all_ticket_list();
            $data['issues'] = $this->Projectsmodel->all_issue_list();
            $data['projects'] = $this->Projectsmodel->all_project_list();
            $data['sprints'] = $this->Projectsmodel->all_sprint_list();
            $data['payments'] = $this->Projectsmodel->get_payments_list($user_type_id,$client_id);
            $data['timeline_results'] =  $this->Projectsmodel->get_todo_list(null);
            $data['expense_data'] =  $this->Projectsmodel->get_expense_graph();
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            /*for ticket status graph
             $status_list = $this->Projectsmodel->display_status();
             
             
             $status_name = array();
             $data['graph_count'] = array();
             $i = 0;
             foreach ($status_list as $sl) {
             $status_id = $sl->status_id;
             $status_name = $sl->status_name;
             $count_g = $this->Projectsmodel->count_ticket_status($status_id,$user_id);
             foreach ($count_g as $cg) {
             $g_count = $cg->status_count;
             }
             array_push($data['graph_count'],(object) array('status_name' => $status_name,'status_count' =>$g_count ));
             }
             for ticket status graph*/
            
            /*For Income Expense graph*/
            $expense_list = $this->Projectsmodel->sum_expense($user_id);
            //dumpEx($expense_list);
            $tot_expense = $expense_list[0]->tot_exp;
            $income_list = $this->Projectsmodel->sum_income($user_id);
            $tot_income = $income_list[0]->tot_income;
            $graph_index = array('income','expense');
            $value_index = array($tot_income,$tot_expense);
            $i = 0;
            $data['income_expense'] = array();
            foreach ($graph_index as $gi) {
                $status_name = $graph_index[$i];
                $status_val = $value_index[$i];
                
                array_push($data['income_expense'],(object)array('label_name' => $status_name,'label_val' => $status_val));
                $i=$i+1;
            }
            /*For Income Expense graph*/
            
            /*for ticket status graph*/
            $status_list = $this->Projectsmodel->display_status();
            $status_name = array();
            $data['graph_count'] = array();
            $i = 0;
            foreach ($status_list as $sl) {
                $status_id = $sl->status_id;
                $status_name = $sl->status_name;
                $count_g = $this->Projectsmodel->count_ticket_status($status_id,$user_id);
                foreach ($count_g as $cg) {
                    $g_count = $cg->status_count;
                }
                array_push($data['graph_count'],(object) array('status_name' => $status_name,'status_count' =>$g_count ));
            }
            /*for ticket status graph*/
            
            if($user_type_id=='1')
            {
                $this->load->view('menu',$data);
                $this->load->view('border');
                $this->load->view('dashboard_2',$data);
            }
            elseif($user_type_id=='2')
            {
                $this->load->view('menu');
                $this->load->view('border');
                $this->load->view('projects',$data);
            }
            elseif($user_type_id=='3')
            {
                $this->load->view('menu');
                $this->load->view('border');
                $this->load->view('dashboard_2',$data);
            }
        }
        else {
            $data['flag']=null;
            $data['error_flag']=false;
            $this->load->view('agile_login',$data);
        }
    }
    /**
     * Method to populate main menu
     */
    public function get_main_menu()
    {
        $user_type_id=$this->session->userdata('user_type_id');
        $query=$this->Loginmodel->get_main_menu_list($user_type_id);
        return $query;
        //print_r($query);
    }
    /**
     * Method to logout user
     */
    public function logout()
    {
        $this->session->unset_userdata('user_type_id');
        $this->session->unset_userdata('user_name');
        $this->session->sess_destroy();
        $data['flag']=null;
        $data['error_flag']=true;
        $this->load->view('agile_login',$data);
    }
}