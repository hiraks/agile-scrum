<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Menus extends CI_Controller {	
	function __construct() {
		parent::__construct ();	
		
		/*if($this->session->user_id == null)
		{
		    redirect(site_url(),'refresh');
		}*/
		// Load url helper
		$this->load->helper ('url');
		$this->load->library ('session');
		$this->load->helper('form');
	}
	
	public function downloadFile($fileName) {
	    echo("download controller");
	    if ($fileName) {
	        $this->load->helper('download');
	        $data = file_get_contents('./uploads/'.$fileName);
	        force_download($fileName, $data);
	    }
	}
	/* Method to load dashboard	 */
	public function dashboard() {
	    
		if ($this->session->user_name != null) {			
			$this->load->model('Loginmodel');		
			$this->load->model ( 'Projectsmodel' );
			$user_type_id = $this->session->userdata('user_type_id');
			$client_id_val = $this->session->userdata('client_id');
			$user_id = $this->session->userdata('user_id');//exit;
			//echo $username = $this->session->userdata('user_name');
			//exit;
			$sel_lang = $this->input->get('lang');
			
			if($sel_lang == null || $sel_lang=="")
			{
			    $sel_lang=$this->Projectsmodel->get_default_language();
			}
			$this->session->set_userdata('selectedLanguage', $sel_lang);
			$resultLang=$this->Projectsmodel->get_lang($sel_lang);
			$langArray = array();
			$prop=$sel_lang.'_lang';
			for($i=0;$i<count($resultLang);$i++)
			{
			    //echo "Test===".$resultLang[$i]->label_prop;
			    //echo "Test===".$resultLang[$i]->es_lang;
			    
			    $langArray[$resultLang[$i]->label_prop]=$resultLang[$i]->$prop;
			}
			$this->session->set_userdata('languageArray', $langArray);
			$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'DASHBOARD');
			$this->session->set_userdata('first_level_menu', $data);
			if ($client_id_val == null or $client_id_val == '0') {
				$client_id = null;
			} else {
				$client_id = $client_id_val;
			}			
		
		
			
			// $results=$this->Projectsmodel->view_projects_overview();
			// $data=array('results'=>$results);
			$data['results'] = $this->Projectsmodel->view_projects_overview($client_id);
			$data['todolist'] = $this->Projectsmodel->get_all_todo_list($client_id);
			$data['projectcount'] = $this->Projectsmodel->get_project_count($client_id);
			$data['openIssueCount'] = $this->Projectsmodel->get_open_issue_count($client_id );
			$data['closedIssueCount'] = $this->Projectsmodel->get_closed_issue_count($client_id);
			$data['loggedhours'] = $this->Projectsmodel->get_total_logged_hours($client_id);
			$data['lm_earnings'] = $this->Projectsmodel->get_total_earnings_last_month($client_id);
			$data['lm_bill'] = $this->Projectsmodel->get_total_bill_last_month($client_id);
			$data['lm_pending'] = $this->Projectsmodel->get_total_pending_bill_last_month($client_id);
			$data['tickets'] = $this->Projectsmodel->all_ticket_list();
			$data['issues'] = $this->Projectsmodel->all_issue_list();
			$data['projects'] = $this->Projectsmodel->all_project_list();
			$data['sprints'] = $this->Projectsmodel->all_sprint_list();
			$data['payments'] = $this->Projectsmodel->get_payments_list($user_type_id,$client_id_val);
			$data['timeline_results'] =  $this->Projectsmodel->get_todo_list(null);
			$data['expense_data'] =  $this->Projectsmodel->get_expense_graph();

			/*For Income Expense graph*/
			$expense_list = $this->Projectsmodel->sum_expense($user_id);
			//dumpEx($expense_list);
			$tot_expense = $expense_list[0]->tot_exp;	
			$income_list = $this->Projectsmodel->sum_income($user_id);	
			$tot_income = $income_list[0]->tot_income;
			$graph_index = array('income','expense');
			$value_index = array($tot_income,$tot_expense);
			$i = 0;
			$data['income_expense'] = array();
			foreach ($graph_index as $gi) {
				$status_name = $graph_index[$i];
				$status_val = $value_index[$i];		
			
			array_push($data['income_expense'],(object)array('label_name' => $status_name,'label_val' => $status_val));
			$i=$i+1;
			}
			/*For Income Expense graph*/

			/*for ticket status graph*/
			$status_list = $this->Projectsmodel->display_status();			
			$status_name = array();
			$data['graph_count'] = array();			
			$i = 0;
			foreach ($status_list as $sl) {
					$status_id = $sl->status_id;					
					$status_name = $sl->status_name;	
					$count_g = $this->Projectsmodel->count_ticket_status($status_id,$user_id);
					foreach ($count_g as $cg) {
						$g_count = $cg->status_count;
					}					
					array_push($data['graph_count'],(object) array('status_name' => $status_name,'status_count' =>$g_count ));
				}	
				
				
				//foreach ($resultLang as $key => $value) {
				    // $arr[3] will be updated with each value from $arr...
				  //  echo "{$key} => {$value} ";
				//print_r($langArray);
				//echo ($langArray[dashboard]);
				//}
				
				/*while($row = mysql_fetch_array($resultLang)){
				    // Put the values into the array, no other variables needed
				    $langArray[$row['label_prop']] = $row['es_lang'];*/
				//}
				
				
			/*for ticket status graph*/
			//dump($data['graph_count']);				
			$this->load->view ('menu');
			$this->load->view ('border');
			$this->load->view ('dashboard_2', $data);
		}
	}
	
	/**/
	public function ProductBacklog() {
	    if ($this->session->user_name != null) {
	        $this->load->model ( 'Loginmodel' );
	        $user_type_id = $this->session->userdata ( 'user_type_id' );
	        $data ['menu_name'] = $this->Loginmodel->get_main_menu_list ( $user_type_id, 'PRODUCT BACKLOG' );
	        $this->session->set_userdata ( 'first_level_menu', $data );	
	        
	        $this->load->model('Projectsmodel');
	        $data['projects'] = $this->Projectsmodel->all_project_list();
	        
	        
	        $this->load->model('Scrummodel');
	        $records = $this->Scrummodel->list_backlog('');
	        $data ['results'] = $records;
	        //$data['heading'] = 'Backlog';
	        // print_r($data['results'][0]);
	        // print_r($data);
	        // $project_id = $this->uri->segment($id);
	        $this->load->view('menu');
	        $this->load->view('border');
	        $this->load->view('view_backlog_all',$data);
	    }
	}
	/*
	 * 
	 */
	public function ViewIssues() {
	    if ($this->session->user_name != null) {
	        $this->load->model ( 'Loginmodel' );
	        $user_type_id = $this->session->userdata ( 'user_type_id' );
	        $data ['menu_name'] = $this->Loginmodel->get_main_menu_list ( $user_type_id, 'ISSUES / STORY' );
	        $this->session->set_userdata ( 'first_level_menu', $data );
	        
	        $this->load->model('Projectsmodel');
	        $data['projects'] = $this->Projectsmodel->all_project_list();
	        
	        
	        $this->load->model('Scrummodel');
	        $records = $this->Scrummodel->view_all_project_issues();
	        $data ['results'] = $records;
	        
	        $this->load->view('menu');
	        $this->load->view('border');
	        $this->load->view('view_issues_all',$data);
	    }
	}
	
	/*
	 * 
	 */
	
	public function ViewSprints() {
	    if ($this->session->user_name != null) {
	        $this->load->model ( 'Loginmodel' );
	        $user_type_id = $this->session->userdata ( 'user_type_id' );
	        $data ['menu_name'] = $this->Loginmodel->get_main_menu_list ( $user_type_id, 'ISSUES / STORY' );
	        $this->session->set_userdata ( 'first_level_menu', $data );
	        
	        $this->load->model('Projectsmodel');
	        $data['projects'] = $this->Projectsmodel->all_project_list();
	        
	        
	        $this->load->model('Scrummodel');
	        $records = $this->Scrummodel->view_all_project_issues();
	        $data ['results'] = $records;
	        
	        $this->load->view('menu');
	        $this->load->view('border');
	        $this->load->view('view_issues_all',$data);
	    }
	}
	
	/* Method to load client list */
	public function clients() {
		if ($this->session->user_name != null) {			
			$this->load->model ( 'Loginmodel' );
			$user_type_id = $this->session->userdata ( 'user_type_id' );
			$data ['menu_name'] = $this->Loginmodel->get_main_menu_list ( $user_type_id, 'CLIENTS' );
			$this->session->set_userdata ( 'first_level_menu', $data );			
			$this->load->model ( 'Clientmodel' );
			$results = $this->Clientmodel->view_clients_overview ();
			$data = array (
					'results' => $results 
			);			
			$this->load->view ( 'menu' );
			$this->load->view ( 'border' );
			$this->load->view ( 'clients', $data );
		}
	}	
	/* Method to get settings details */
	public function settings() {
		if ($this->session->user_name != null) {			
			$this->load->model ( 'Loginmodel' );
			$user_type_id = $this->session->userdata ( 'user_type_id' );
			$data ['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'SETTINGS');
			$this->session->set_userdata ( 'first_level_menu', $data );			
			$this->load->model ('Projectsmodel');
			$results = $this->Projectsmodel->get_company_details();
			$data = array (
				'results' => $results 
			);	

			$data['tax'] = $this->Projectsmodel->get_tax_details();	
			//dumpEx($data['tax']);		
			$this->load->view ('menu');
			$this->load->view ('border');
			$this->load->view ('settings',$data);
		}
	}	
	/* Get client details based on client id. * @param $client_id */
	public function view_client_by_id($client_id){
		$data['mode'] = $this->uri->segment(4);
		if ($this->session->user_name != null) {			
			$this->load->model ( 'Clientmodel' );
			$data['results'] = $this->Clientmodel->get_client_by_client_id ($client_id);		
			$this->load->view ('menu');
			$this->load->view ('border');
			$this->load->view ('edit_client',$data);
		}
	}	
	/* Method to load users list */
	public function users() {
		if ($this->session->user_name != null) {			
			if($this->session->userdata('user_type_id')!= '3' && $this->session->userdata('user_type_id')!='2')
			{			
			$this->load->model ('Loginmodel');
			$user_type_id = $this->session->userdata('user_type_id');
			$data ['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id, 'USERS');
			$this->session->set_userdata('first_level_menu', $data);			
			$this->load->model('Usersmodel');
			$results = $this->Usersmodel->get_users();
			$data = array (
					'results' => $results 
			);			
			$this->load->view ('menu');
			$this->load->view ('border');
			$this->load->view ('users',$data);
			}
		}
	}	
	/* Method to get list of Invoices */
	public function invoices() {
		if ($this->session->user_name != null) {			
			$this->load->model('Loginmodel');
			$user_type_id = $this->session->userdata('user_type_id');
			$data ['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'INVOICES');
			$this->session->set_userdata('first_level_menu',$data);			
			$this->load->model('Projectsmodel');
			$results = $this->Projectsmodel->get_invoice_list($this->session->userdata('user_type_id'),$this->session->userdata('client_id'));
			$data ['results'] = $results;
			$data ['user_type'] = $user_type_id;			
			$this->load->view('menu');
			$this->load->view('border');
			$this->load->view('invoices', $data);
		}
	}	
	/* Method to get list of payments received. */
	public function payments(){
		if ($this->session->user_name != null){			
			$this->load->model('Loginmodel');
			$user_type_id = $this->session->userdata('user_type_id');
			$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'PAYMENTS');
			$this->session->set_userdata('first_level_menu',$data);			
			$this->load->model ('Projectsmodel');
			$results = $this->Projectsmodel->get_payments_list($this->session->userdata('user_type_id'),$this->session->userdata('client_id'));
			$data = array (
				'results' => $results 
			);			
			$this->load->view('menu');
			$this->load->view('border');
			$this->load->view('payments',$data);
		}
	}	
	/**
	 * Method to delete user based on username.
	 * 
	 * @param
	 *        	$username
	 */
	public function delete_user($username) {
		$this->load->model('Usersmodel');
		$this->Usersmodel->delete_user($username);
		
		$results = $this->Usersmodel->get_users();
		$data = array (
				'results' => $results 
		);
		
		$this->load->view ('menu');
		$this->load->view ('border');
		$this->load->view ('users',$data);
	}	
	/**
	 * Method to delete client based on userid  $userid */
	public function delete_client($userid) {
		$this->load->model ( 'Clientmodel' );
		$this->Clientmodel->delete_client ( $userid );		
		$results = $this->Clientmodel->view_clients_overview ();
		$data = array (
				'results' => $results 
		);		
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('clients',$data);
	}	
	/* Method to delete invoice.$invoiceid	 */
	public function delete_invoice($invoiceid) {
		$this->load->model('Projectsmodel');
		$this->Projectsmodel->delete_invoice($invoiceid);		
		$this->load->model('Projectsmodel');
		$results = $this->Projectsmodel->get_invoice_list($this->session->userdata('user_type_id'),$this->session->userdata('client_id'));
		$data = array (
				'results' => $results 
		);		
		$this->load->view ('menu');
		$this->load->view ('border');
		$this->load->view ('invoices', $data);
	}	
	/* Method to view user by username.@param  $username */
	public function view_user_by_username($username){
		$data['mode'] = $this->uri->segment(4);
		if ($this->session->user_name != null){			
			$this->load->model('Usersmodel');
			$data['results'] = $this->Usersmodel->get_user_by_username($username);
			$this->load->model('Clientmodel');
			$data['clients'] = $this->Clientmodel->view_clients_overview();			
			$this->load->view('menu');
			$this->load->view('border');
			$this->load->view('edit_user',$data);
		}
	}	
	/** Method to add user.	 */
	public function add_user(){
		if ($this->session->user_name != null) {			
			$this->load->model('Usersmodel');
			$data ['results'] = $this->Usersmodel->get_users();
			$this->load->model ('Clientmodel');
			$data ['clients'] = $this->Clientmodel->view_clients_overview();			
			// $data=array('results'=>$results);			
			$this->load->view ('menu');
			$this->load->view ('border');
			$this->load->view ('adduser',$data);
		}
	}	
	/** Method to add invoice	 */
	public function add_invoice(){
		if ($this->session->user_name != null){			
			$this->load->model ('Usersmodel');
			$results = $this->Usersmodel->get_users();
			$data = array(
					'results' => $results 
			);			
			$this->load->model('Projectsmodel');
			$invoiceId = $this->Projectsmodel->get_current_invoiceId();
			$data ['invoiceId'] = $invoiceId;			
			$this->load->model('Clientmodel');
			$clientDetails = $this->Clientmodel->get_client_name_and_id();
			$data ['clientdetails'] = $clientDetails;			
			$this->load->view('menu');
			$this->load->view('border');
			$this->load->view('createinvoice',$data);
		}
	}	
	/* Method to get client details	 */
	public function client_details() {
		if ($this->session->user_name != null) {
			$this->load->model('Loginmodel');				
			$user_type_id = $this->session->userdata('user_type_id');			
			$this->load->view ('menu');
			$this->load->view ('border');			
			$this->load->view('clientdetails');
		}
	}	
	/** Method to get projects list.*/
	public function projects() {		
		if ($this->session->user_name != null) {			
			$this->load->model('Loginmodel');				
			$user_type_id = $this->session->userdata('user_type_id');
			//echo $user_type_id;
			$loginname = $this->session->userdata('user_name');
			$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'AGILE PROJECTS');
			$this->session->set_userdata ('first_level_menu',$data);			
			$this->load->model('Projectsmodel');
			//dumpEx($this->session->userdata('client_id'));
			$data['results'] = $this->Projectsmodel->view_projects_overview($this->session->userdata('client_id'));
			//dumpEx($data);		
			$this->load->view('menu');
			$this->load->view('border');
			$this->load->view('projects',$data);
		}
	}	
	/**
	 * Method to add project
	 */
	public function add_project() {
		if ($this->session->user_name != null) {			
			$this->load->model('Clientmodel');
			$this->load->model('Usersmodel');
			$results['client_list'] = $this->Clientmodel->get_client_name_and_id();            
			$results['user_list'] = $this->Usersmodel->get_users();
			//dumpEx($results['user_list']);
			/*$data = array (
					'results' => $results 
			);
			*/	
			//dumpEx($results);		
			$this->load->view ('menu');
			$this->load->view ('border');
			$this->load->view ('addproject',$results);
		}
	}
	/*public function edit_project()
    {        
        $project_id = $this->uri->segment(3);
        $results['mode'] = $this->uri->segment(4);
        if (is_numeric($project_id)){        
            $this->load->model('Clientmodel');
            $this->load->model('Usersmodel');
            $results['client_list'] = $this->Clientmodel->get_client_name_and_id();            
            $results['user_list'] = $this->Usersmodel->get_users();
            $this->load->model('Projectsmodel');
            $results['project_details'] = $this->Projectsmodel->get_project($project_id);     
        }
        //dumpEx($results);
            $this->load->view ('menu');
            $this->load->view ('border');
            $this->load->view ('addproject',$results);
    } */
    public function edit_project()
    {        
        $project_id = $this->uri->segment(3);
        //$data['mode'] = $this->uri->segment(4);
        if ($this->session->user_name != null){	
        if (is_numeric($project_id)){       
            $this->load->model('Clientmodel');
            $this->load->model('Usersmodel');
            $data['client_list'] = $this->Clientmodel->get_client_name_and_id();            
            $data['user_list'] = $this->Usersmodel->get_users();
            $this->load->model('Projectsmodel');
            $data['project_details'] = $this->Projectsmodel->get_project($project_id);     
        }
        //dumpEx($results);
            $this->load->view ('menu');
            $this->load->view ('border');
            $this->load->view ('addproject',$data);
        }
    }  
   /* public function view_user_by_username($username){
		$data['mode'] = $this->uri->segment(4);
		if ($this->session->user_name != null){			
			$this->load->model('Usersmodel');
			$data['results'] = $this->Usersmodel->get_user_by_username($username);
			$this->load->model('Clientmodel');
			$data['clients'] = $this->Clientmodel->view_clients_overview();			
			$this->load->view('menu');
			$this->load->view('border');
			$this->load->view('edit_user',$data);
		}
	}*/   
	/* Method to get project details based on project id.  @param $id */
	public function project_details($id){
		if ($this->session->user_name != null){			
			/*
			 * $this->load->model('Clientmodel');
			 * $results=$this->Clientmodel->get_client_name_and_id();
			 * $data=array('results'=>$results);
			 */			
			if (is_numeric($id)){
				$this->session->set_userdata("project_id", $id);
			}			
			// echo($this->session->userdata('project_id'));
			$this->load->model('Projectsmodel');
			//echo("Menus Controller==================================");
			$results = $this->Projectsmodel->view_project_details($this->session->userdata('project_id'));
			$data = array (
			    'results' => $results
			);
			//dumpEx($data);
			//$data['heading'] = 'Scrum Dashboard';
			$this->load->view ('menu');
			$this->load->view ('border');
			$this->load->view ('agile_dashboard',$data);
		}
	}	
	/*
	 * Function to add issues.	 * 
	 * @param $navs- navigational
	 * parameter
	 * @param $project_id -project
	 * id.
	 */
	public function add_issues($navs, $project_id){
		//echo ('==========================================' . $navs);
		$this->load->model('Usersmodel');
		$results = $this->Usersmodel->get_users();
		// $data=array('results'=>$results);
		$data ['results'] = $results;
		$data ['navs'] = $navs;		
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('create_issues',$data);
	}	
	/**
	 * Method to create sprint.
	 */
	public function create_sprint(){
		$this->load->model('Scrummodel');
		// $results=$this->Scrummodel->view_issues($this->session->userdata('project_id'));
		$data ['issueList'] = $this->Scrummodel->view_all_issues($this->session->userdata('project_id'));		
		$sprintId = $this->Scrummodel->get_current_sprintid();
		$data ['sprintId'] = $sprintId;		
		$data ['error_count'] = FALSE;
		$this->load->library ('calendar');
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('create_sprint',$data);
	}	
	/*
	 * Method to edit sprint based on sprint id.
	 * @param  $sprint_id
	*/
	public function edit_sprint($sprint_id) {
		$this->load->model ('Scrummodel');
		// $results=$this->Scrummodel->view_issues($this->session->userdata('project_id'));
		$data['issueList'] = $this->Scrummodel->view_issues($this->session->userdata('project_id'));		
		$data['sprintList'] = $this->Scrummodel->view_sprints_byid($sprint_id);		
		$data['sprintId'] = array (
				$sprint_id 
		);		
		$data ['error_count'] = FALSE;
		$data ['issueList'] = $this->Scrummodel->view_all_issues($this->session->userdata('project_id'));		
		$this->load->library('calendar');
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('edit_sprint',$data);
	}	
		
	/*
	 * To manage the submenus	 
	 * @param $Project id        	
	*/
	public function submenu($id, $menuType) {
		if ($this->session->user_name != null) {			
			if ($menuType == 'issues') {
				$this->load->model('Scrummodel');
				$records = $this->Scrummodel->view_issues($id);
				$data ['results'] = $records;
				//$data['heading'] = 'Issues';				
				// print_r($data['results'][0]);
				// print_r($data);				
				// $project_id = $this->uri->segment($id);
				$this->load->view('menu');
				$this->load->view('border');
				$this->load->view('view_issues',$data);
			}	
			if ($menuType == 'backlog') {
			    print_r("Project Id=="+$id);
				$this->load->model('Scrummodel');
				$records = $this->Scrummodel->list_backlog($id);
				$data ['results'] = $records;
				//$data['heading'] = 'Backlog';				
				// print_r($data['results'][0]);
				// print_r($data);				
				// $project_id = $this->uri->segment($id);
				$this->load->view('menu');
				$this->load->view('border');
				$this->load->view('view_backlog',$data);
			}		
			if ($menuType == 'stories') {
				$this->load->model('Scrummodel');
				$records = $this->Scrummodel->view_stories($id);
				$data ['results'] = $records;	
				//$data['heading'] = 'Stories';			
				// print_r($data['results'][0]);
				// print_r($data);				
				// $project_id = $this->uri->segment($id);
				$this->load->view('menu');
				$this->load->view('border');
				$this->load->view('view_stories',$data);
			}			
			if ($menuType == 'sprint') {				
				$sprint_id = '';
				$this->load->model('Scrummodel');
				$resultsSprint = $this->Scrummodel->view_sprints($id);
				$data ['sprintList'] = $resultsSprint;				
				$sprintIdData = $resultsSprint;
				//$data['heading'] = 'Sprint';
				//print_r ($sprintIdData);				
				foreach ($sprintIdData as $value) {
					$sprint_id = $value->sprint_id;
				}				
				$results = $this->Scrummodel->view_sprintwise_issues($sprint_id);
				$data ['issueList'] = $results;				
				$data ['error_count'] = FALSE;
				
				$maxSprintId=$this->Scrummodel->get_max_sprint_id($this->session->userdata('project_id'));
				// $this->session->set_userdata("project_id",$id);				
				// $project_id = $this->uri->segment($id);

				$this->load->model('Projectsmodel');
				$data['worklist'] = $this->Projectsmodel->get_sprint_list($this->session->userdata('project_id'),$maxSprintId[0]->sprint_id);	

				$this->load->view('menu');
				$this->load->view('border');
				$this->load->view('view_sprint',$data);
			}			
			if ($menuType == 'timesheet') {
				$this->load->model('Scrummodel');
				$records = $this->Scrummodel->view_all_issues($id);
				$data ['results'] = $records;
				//$data['heading'] = 'Timesheet';				
				// print_r($data['results'][0]);
				// print_r($data);				
				// $project_id = $this->uri->segment($id);
				$this->load->view ( 'menu' );
				$this->load->view ( 'border');
				$this->load->view ( 'timesheet', $data );
			}			
			if ($menuType == 'reports') {
				$this->load->model ('Scrummodel');
				$finaldata = $this->Scrummodel->get_map_coordinates($this->session->userdata('project_id'));		
				// $this->load->model('Scrummodel');
				// $records=$this->Scrummodel->view_all_issues($id);
				// $data['results']=$records;				
				// print_r($data['results'][0]);
				// print_r($data);				
				// $project_id = $this->uri->segment($id);				
				$data ['chart_data'] = $finaldata; 
				//$data['heading'] = 'Sprint BurnDown Chart';
				/*
                  * array(
                  *
                  *
                  * array( 'name' => '5',
                  * 'link' => '15',
                  * 'con' => '8'
                  * ),
                  *
                  * array( 'name' => '7',
                  * 'link' => '20',
                  * 'con' => '10'
                  * )
                  * );				                                 
				 * 'data1'=>array(5,10,15),
				 * 'data2'=>array(6,12,17),
				 * 'data3'=>array(7,14,19));
				 */
				// $data['min_year'] = array(1,2,3);
				// $data['max_year'] = array(10,20,30);				
				// $this->load->view('chart', $data);
				$this->load->view ('menu');
				$this->load->view ('border');
				$this->load->view ('reports',$data);
			}			
			if ($menuType == 'dashboard') {
			    $this->session->set_userdata("project_id", $id);
				$this->load->model('Projectsmodel');
				$data['results'] = $this->Projectsmodel->view_project_details($this->session->userdata('project_id'));				
				//$data = array (
				//		'results' => $results 
			//	);	
				
				$data['timeline_results'] =  $this->Projectsmodel->get_todo_list($id);
				//print_r($timeline_results);
				//$data['timeline_results'] =  $this->Projectsmodel->get_todo_list($this->session->userdata('project_id'));	
				
				//dumpEx($data);
				//$data['heading'] = 'Scrum Dashboard';			
				$this->load->view ('menu');
				$this->load->view ('border');
				$this->load->view ('agile_dashboard',$data);
			}
			
			if ($menuType == 'retro') {
			    $this->session->set_userdata("retro_project_type", "project_type");
			    $this->session->set_userdata("project_id", $id);
			    $this->load->model('Projectsmodel');
			    $data['results'] = $this->Projectsmodel->view_project_details($this->session->userdata('project_id'));
			   
			       /* $this->load->model('Loginmodel');
			        $user_type_id = $this->session->userdata('user_type_id');
			        $data ['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'RETROSPECTIVE');
			        $this->session->set_userdata('first_level_menu',$data);*/
			        
			        $this->load->model('Projectsmodel');
			        $results = $this->Projectsmodel->get_retro($id);
			        $data = array(
			            'results' => $results
			        );
			        
			        $this->load->view('menu');
			        $this->load->view('border');
			        $this->load->view('project_retro',$data);
			    
			}
			if ($menuType == 'files') {
			    $this->session->set_userdata("project_id", $id);
			    $this->load->model('Projectsmodel');
			    $data['results'] = $this->Projectsmodel->view_project_details($this->session->userdata('project_id'));
			    
			    /* $this->load->model('Loginmodel');
			     $user_type_id = $this->session->userdata('user_type_id');
			     $data ['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'RETROSPECTIVE');
			     $this->session->set_userdata('first_level_menu',$data);*/
			    
			    $results=$this->Projectsmodel->get_project_files($this->session->userdata('project_id'));
			    $data=array('results'=>$results);
			    $this->load->view('menu');
			    $this->load->view('border');
			    $this->load->view('add_project_files',$data);
			    
			}
		}
	}

	public function events() {
		if ($this->session->user_name != null) {			
			$this->load->model('Loginmodel');				
			$user_type_id = $this->session->userdata('user_type_id');
			//echo $user_type_id;
			$loginname = $this->session->userdata('user_name');
			$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'EVENTS');
			$this->session->set_userdata ('first_level_menu',$data);			
			$this->load->model('Projectsmodel');
			//dumpEx($this->session->userdata('client_id'));
			$results = $this->Projectsmodel->view_projects_overview($this->session->userdata('client_id'));	
			//dumpEx($results);		
			$data = array(
				'results' => $results 
			);				
			//dumpEx($data);		
			$this->load->view('menu');
			$this->load->view('border');
			$this->load->view('event',$data);
			//$this->load->view('event','',TRUE);
		}
	}	
	/* Method to add events */
	public function add_events() {
		if ($this->session->user_name != null) {			
			$this->load->model('Clientmodel');
			$results = $this->Clientmodel->get_client_name_and_id();
			$this->load->model('Scrummodel');
			$colors = $this->Scrummodel->view_all_color();
			$results = $colors;
			//dumpEx($colors);
			$data = array (
					'results' => $results 
			);
			//dumpEx($data);
			$this->load->view('menu');
			$this->load->view('border');
			$this->load->view('addevent',$data);
		}
	}
	function events_details(){
		if($this->input->post()){
			//$this->load->model('Loginmodel');				
			//$user_id = $this->session->userdata('user_id');
			$user_id = $this->input->post('user_id',TRUE);	
			$this->load->model('Projectsmodel');		
			$reslut = $this->Projectsmodel->view_events_details($user_id);	
			//dumpEx($reslut);
			echo json_encode($reslut);
			//$data['event_details'] = $this->Projectsmodel->view_events_details($user_id);	
			//$this->load->view('addevent',$data);
		}
	}	
	public function messages(){
		if ($this->session->user_name!= null){			
			$this->load->model('Loginmodel');				
			$user_type_id = $this->session->userdata('user_type_id');	
			$loginname = $this->session->userdata('user_name');
			$user_id = $this->session->userdata('user_id');
			$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'MESSAGES');
			$this->session->set_userdata('first_level_menu',$data);			
			$this->load->model('Projectsmodel');			
			$results = $this->Projectsmodel->view_messages_overview($user_id);	
			$this->load->model('Usersmodel');
			$data = array(
				'results' => $results 
			);				
			//dumpEx($data);		
			$this->load->view('menu');
			$this->load->view('border');
			$this->load->view('message_dashboard',$data);
		}
	}	
	/* Method to add messages */
	public function add_messages(){
		if ($this->session->user_name != null){	
			$this->load->model('Loginmodel');				
			$user_type_id = $this->session->userdata('user_type_id');	
			$loginname = $this->session->userdata('user_name');
			$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'MESSAGES');
			$this->session->set_userdata('first_level_menu',$data);
			$this->load->model('Usersmodel');
			$data['username']=$this->Usersmodel->get_nousers($loginname);
			//dumpEx($data);				
			$this->load->view('menu');
			$this->load->view('border');
			$this->load->view('message_compose',$data);
		}
	}	
	/* Method to sent messages */
	public function sent_messages(){
		if ($this->session->user_name!= null){			
			$this->load->model('Loginmodel');				
			$user_type_id = $this->session->userdata('user_type_id');	
			$loginname = $this->session->userdata('user_name');
			$user_id = $this->session->userdata('user_id');
			$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'MESSAGES');
			$this->session->set_userdata('first_level_menu',$data);			
			$this->load->model('Projectsmodel');			
			$results = $this->Projectsmodel->view_messages_sent($user_id);						
			$data = array(
				'results' => $results 
			);				
			//dumpEx($data);		
			$this->load->view('menu');
			$this->load->view('border');
			$this->load->view('message_sent_dashboard',$data);
		}
	}
	/* Method to trash messages */
	public function trash_messages(){
		if ($this->session->user_name!= null){			
			$this->load->model('Loginmodel');				
			$user_type_id = $this->session->userdata('user_type_id');	
			$loginname = $this->session->userdata('user_name');
			$user_id = $this->session->userdata('user_id');
			$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'MESSAGES');
			$this->session->set_userdata('first_level_menu',$data);			
			$this->load->model('Projectsmodel');			
			$results = $this->Projectsmodel->view_messages_trash($user_id);						
			$data = array(
				'results' => $results 
			);				
			//dumpEx($data);		
			$this->load->view('menu');
			$this->load->view('border');
			$this->load->view('message_trash_dashboard',$data);
		}
	}
	/* Method to delete message based on username.	 */
	public function delete_message($message_id) {
		$this->load->model('Scrummodel');
		$this->Scrummodel->delete_message($message_id);			
		redirect('Menus/sent_messages');
	}
	public function delete_message_inbox($message_id) {
		$this->load->model('Scrummodel');
		$this->Scrummodel->delete_message($message_id);			
		redirect('Menus/messages');
	}		
	public function messages_details(){
		if($this->input->post()){
			$message_id = $this->input->post('message_id');	
			$this->load->model('Projectsmodel');	
			//echo $message_id;exit;
			$this->Projectsmodel->update_messages_count($message_id);	
			$reslut = $this->Projectsmodel->view_messages_details($message_id);	
			//var_dump($reslut);
			$id = $reslut['message_send_by'];			
            $user_dtl = userDetails($id);                                    
            $user_name = $user_dtl[0];
            $rply_id = $this->session->userdata('user_id');
            $user_rply = userDetails($rply_id);
            //dump($user_dtl);                        
            $rply_name = $user_rply[0];
            //$sendername = $user_name['fullname'];
            $reslut['sender_name'] = $user_name['fullname'];
            $reslut['filename'] = $user_name['filename'];    
            $reslut['rplypic'] = $rply_name['filename'];    
            //dumpEx($reslut);
			echo json_encode($reslut);
	}
	}	
	public function notes()
	{
		$this->load->model('Loginmodel');				
		$user_type_id = $this->session->userdata('user_type_id');	
		$loginname = $this->session->userdata('user_name');
		$user_id = $this->session->userdata('user_id');
		$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'NOTES');
		$this->session->set_userdata('first_level_menu',$data);			
		$this->load->model('Projectsmodel');			
		$results = $this->Projectsmodel->get_note_list($user_id);						
		$data = array(
			'results' => $results 
		);				
		//dumpEx($data);		
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('view_notes',$data);		
	}
	public function view_note()
	{
		$this->load->model('Loginmodel');
		$user_type_id = $this->session->userdata('user_type_id');
		$loginname = $this->session->userdata('user_name');
		$user_id = $this->session->userdata('user_id');
		$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'NOTES');
		$this->session->set_userdata('first_level_menu',$data);	
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('add_notes',$data);	
	}
	public function edit_note()
	{
		$note_id = $this->uri->segment(3);
		if(!empty($note_id) && (is_numeric($note_id)))
		{
			$this->load->model('Projectsmodel');
			$data['notes_details'] = $this->Projectsmodel->display_notes($note_id);
		}	
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('add_notes',$data);	
	}
	public function show_note()
	{
		$note_id = $this->uri->segment(3);
		if(!empty($note_id) && (is_numeric($note_id)))
		{
			$this->load->model('Projectsmodel');
			$data['notes_details'] = $this->Projectsmodel->display_notes($note_id);
		}	
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('show_notes',$data);	
	}
	public function notes_add()
	{
		$this->load->model('Loginmodel');
		$user_type_id = $this->session->userdata('user_type_id');
		$loginname = $this->session->userdata('user_name');
		$user_id = $this->session->userdata('user_id');
		$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'NOTES');
		$this->session->set_userdata('first_level_menu',$data);	
		$datam = array(
		'notes_title'=>$this->input->post('notes_title',TRUE),
		'note_description'=>$this->input->post('note_description',TRUE),
		'priority'=>$this->input->post('priority',TRUE),
		'created_by'=>$this->session->userdata('user_id',TRUE)		 		
		);	
		//$notes_id = 0;
		$notes_id = $this->input->post('notes_id',TRUE);
		$this->load->model('Projectsmodel');
		if(!empty($notes_id))
		{			
			$this->Projectsmodel->update_notes($datam,$notes_id);
		}
		else
		{
			$this->Projectsmodel->add_notes($datam);
		}				
		redirect(base_url('Menus/Notes'));
	}
	public function delete_notes($notes_id) {
		$this->load->model('Projectsmodel');
		$this->Projectsmodel->delete_notes($notes_id);			
		redirect(base_url('Menus/Notes'));
	}	
	public function tickets()
	{		
		$this->load->model('Loginmodel');				
		$user_type_id = $this->session->userdata('user_type_id');	
		$loginname = $this->session->userdata('user_name');
		$user_id = $this->session->userdata('user_id');
		$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'TICKETS');
		$this->session->set_userdata('first_level_menu',$data);			
		$this->load->model('Projectsmodel');			
		$results = $this->Projectsmodel->get_ticket_list($user_id);						
		$data = array(
			'results' => $results 
		);				
		//dumpEx($data);		
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('view_tickets',$data);		
	}
	public function view_tickets()
	{
		$this->load->model('Loginmodel');
		$user_type_id = $this->session->userdata('user_type_id');
		$loginname = $this->session->userdata('user_name');
		$user_id = $this->session->userdata('user_id');
		$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'NOTES');
		$this->session->set_userdata('first_level_menu',$data);	
		$this->load->model('Usersmodel');		
		$data ['results'] = $this->Usersmodel->get_users();
		$this->load->model('Projectsmodel');		
		$data ['status_list'] = $this->Projectsmodel->display_status();
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('add_ticket',$data);	
	}
	public function edit_tickets()
	{
		$ticket_id = $this->uri->segment(3);
		if(!empty($ticket_id) && (is_numeric($ticket_id)))
		{
			$this->load->model('Projectsmodel');
			$data['ticket_details'] = $this->Projectsmodel->display_ticket($ticket_id);
			$this->load->model('Usersmodel');
			$results = $this->Usersmodel->get_users();
			$data ['results'] = $results;
			$this->load->model('Projectsmodel');		
			$data ['status_list'] = $this->Projectsmodel->display_status();
		}		
		//dumpEx($data);
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('add_ticket',$data);	
	}
	public function show_tickets()
	{
		$ticket_id = $this->uri->segment(3);
		if(!empty($ticket_id) && (is_numeric($ticket_id)))
		{
			$this->load->model('Projectsmodel');
			$data['ticket_details'] = $this->Projectsmodel->display_ticket($ticket_id);
			$data ['status_list'] = $this->Projectsmodel->display_status();			
			$this->load->model('Usersmodel');
			$results = $this->Usersmodel->get_users();
			$data ['results'] = $results;
		}		
		//dumpEx($data);
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('show_ticket',$data);	
	}
	public function tickets_add()
	{
		$this->load->model('Loginmodel');
		$user_type_id = $this->session->userdata('user_type_id');
		$loginname = $this->session->userdata('user_name');
		$user_id = $this->session->userdata('user_id');
		$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'NOTES');
		$this->session->set_userdata('first_level_menu',$data);	
		$datam = array(
		'ticket_type_id'=>$this->input->post('ticket_type_id',TRUE),
		'ticket_title'=>$this->input->post('ticket_title',TRUE),
		'priority'=>$this->input->post('priority',TRUE),
		'description'=>$this->input->post('description',TRUE),
		'estimated_hours'=>$this->input->post('estimated_hours',TRUE),
		'status_id'=>$this->input->post('status_id',TRUE),
		'reported_by'=>$this->input->post('reported_by',TRUE),
		'assigned_to'=>$this->input->post('assigned_to',TRUE),
		'created_by'=>$this->session->userdata('user_id',TRUE)		 		
		);	
		//dumpEx($datam);
		//$notes_id = 0;
		$ticket_id = $this->input->post('ticket_id',TRUE);
		$this->load->model('Projectsmodel');
		if(!empty($ticket_id))
		{			
			$this->Projectsmodel->update_ticket($datam,$ticket_id);
		}
		else
		{
			$this->Projectsmodel->add_ticket($datam);
		}				
		redirect(base_url('Menus/Tickets'));
	}
	public function delete_ticket($ticket_id) 
	{
		$this->load->model('Projectsmodel');
		$this->Projectsmodel->delete_ticket($ticket_id);			
		redirect(base_url('Menus/Tickets'));
	}	
	public function expenses()
	{		
		$this->load->model('Loginmodel');				
		$user_type_id = $this->session->userdata('user_type_id');	
		$loginname = $this->session->userdata('user_name');
		$user_id = $this->session->userdata('user_id');
		$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'EXPENSES');
		$this->session->set_userdata('first_level_menu',$data);			
		$this->load->model('Projectsmodel');			
		$results = $this->Projectsmodel->get_expenses_list($user_id);						
		$data = array(
			'results' => $results 
		);				
		//dumpEx($data);		
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('view_expense',$data);		
	}
	public function view_expenses()
	{
		$this->load->model('Loginmodel');
		$user_type_id = $this->session->userdata('user_type_id');
		$loginname = $this->session->userdata('user_name');
		$user_id = $this->session->userdata('user_id');
		$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'EXPENSES');
		$this->session->set_userdata('first_level_menu',$data);			
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('add_expenses',$data);	
	}
	public function edit_expenses()
	{
		$exp_id = $this->uri->segment(3);
		if(!empty($exp_id) && (is_numeric($exp_id)))
		{
			$this->load->model('Projectsmodel');
			$data['expenses_details'] = $this->Projectsmodel->display_expenses($exp_id);
			$this->load->model('Usersmodel');
			$results = $this->Usersmodel->get_users();
			$data ['results'] = $results;			
		}		
		//dumpEx($data);
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('add_expenses',$data);	
	}
	public function show_expenses()
	{
		$exp_id = $this->uri->segment(3);
		if(!empty($exp_id) && (is_numeric($exp_id)))
		{
			$this->load->model('Projectsmodel');
			$data['expenses_details'] = $this->Projectsmodel->display_expenses($exp_id);		
			$this->load->model('Usersmodel');
			$results = $this->Usersmodel->get_users();
			$data ['results'] = $results;
		}		
		//dumpEx($data);
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('show_expenses',$data);	
	}
	public function expenses_add()
	{
		$this->load->model('Loginmodel');
		$user_type_id = $this->session->userdata('user_type_id');
		$loginname = $this->session->userdata('user_name');
		$user_id = $this->session->userdata('user_id');
		$data['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'EXPENSES');
		$this->session->set_userdata('first_level_menu',$data);	
		$datam = array(		
		'exp_purpose'=>$this->input->post('exp_purpose',TRUE),
		'exp_amount'=>$this->input->post('exp_amount',TRUE),
		'exp_description'=>$this->input->post('exp_description',TRUE),
		'exp_date'=>$this->input->post('exp_date',TRUE),
		'created_by'=>$user_id
		);	
		$exp_id = $this->input->post('exp_id',TRUE);
		$this->load->model('Projectsmodel');
		if(!empty($exp_id))
		{			
			$this->Projectsmodel->update_expenses($datam,$exp_id);
		}
		else
		{
			$this->Projectsmodel->add_expenses($datam);
		}				
		redirect(base_url('Menus/Expenses'));
	}
	public function delete_expenses($exp_id) 
	{
		$this->load->model('Projectsmodel');
		$this->Projectsmodel->delete_expenses($exp_id);			
		redirect(base_url('Menus/Expenses'));
	}
	/*
	 * Definition of Ready - DOR
	 */
	public function Dor()
	{
	   
	    if ($this->session->user_name != null) {
	        $this->load->model('Loginmodel');
	        $user_type_id = $this->session->userdata('user_type_id');
	        $data ['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'DEFINITION OF READY');
	        $this->session->set_userdata('first_level_menu',$data);
	    
	    $this->load->model('Projectsmodel');
	    $results = $this->Projectsmodel->get_dor_details();
	    $data = array(
	        'results' => $results
	    );
	    
	    $this->load->view('menu');
	    $this->load->view('border');
	    $this->load->view('view_dor',$data);
	}
	}
	
	public function add_dor()
	{
	    $this->load->model('Projectsmodel');
	    $this->load->view('menu');
	    $this->load->view('border');
	    $this->load->view('add_dor');
	}
	public function save_dor()
	{
	    
	    $datam = array(
	        'description'=>$this->input->post('description',TRUE),
	        'created_by'=>$this->session->userdata('user_id',TRUE)
	    );	
	  
	    $this->load->model('Projectsmodel');
	    $this->Projectsmodel->save_dor_details($datam);
	    
	    $this->load->model('Projectsmodel');
	    $results = $this->Projectsmodel->get_dor_details();
	    $data = array(
	        'results' => $results
	    );		
	      
	    $this->load->view('add_dor',$data);
	}
	
	public function save_dor_position()
	{
	    $divid=$this->security->xss_clean($this->input->post('divids'));
	    $position=$this->security->xss_clean($this->input->post('divposition'));
	   
	    $this->load->model('Projectsmodel');
	    $this->Projectsmodel->set_dor_position( $divid,$position);
	    
	    $this->load->model('Projectsmodel');
	    $results = $this->Projectsmodel->get_dor_details();
	    $data = array(
	        'results' => $results
	    );
	    
	    // $this->load->view('menu');
	    // $this->load->view('border');
	    $this->load->view('add_dor',$data);
	}
	
	public function update_dor()
	{
	    $dorid=$this->security->xss_clean($this->input->post('dor_id'));
	    $description=$this->security->xss_clean($this->input->post('description'));
	    
	    //echo($this->input->post('definition',TRUE));
	    
	    $this->load->model('Projectsmodel');
	    $this->Projectsmodel->update_dor( $dorid,$description);
	    
	    $this->load->model('Projectsmodel');
	    $results = $this->Projectsmodel->get_dor_details();
	    $data = array(
	        'results' => $results
	    );
	    
	    $this->load->view('add_dor',$data);
	}
	
	public function delete_dor()
	{
	    $dorid=$this->security->xss_clean($this->input->post('dor_id'));
	   	    
	    //echo($this->input->post('definition',TRUE));
	    
	    $this->load->model('Projectsmodel');
	    $this->Projectsmodel->delete_dor( $dorid);
	    
	    $this->load->model('Projectsmodel');
	    $results = $this->Projectsmodel->get_dor_details();
	    $data = array(
	        'results' => $results
	    );
	    
	    $this->load->view('add_dor',$data);
	}
	
	
	
	public function Dod()
	{
	    
	    if ($this->session->user_name != null) {
	        $this->load->model('Loginmodel');
	        $user_type_id = $this->session->userdata('user_type_id');
	        $data ['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'DEFINITION OF DONE');
	        $this->session->set_userdata('first_level_menu',$data);
	        
	        $this->load->model('Projectsmodel');
	        $results = $this->Projectsmodel->get_dod_details();
	        $data = array(
	            'results' => $results
	        );
	        
	        $this->load->view('menu');
	        $this->load->view('border');
	        $this->load->view('view_dod',$data);
	    }
	}
	
	
	
	
	
	public function save_dod()
	{
	    
	    $datam = array(
	        'description'=>$this->input->post('description',TRUE),
	        'created_by'=>$this->session->userdata('user_id',TRUE)
	    );
	    
	    $this->load->model('Projectsmodel');
	    $this->Projectsmodel->save_dod_details($datam);
	    
	    $this->load->model('Projectsmodel');
	    $results = $this->Projectsmodel->get_dod_details();
	    $data = array(
	        'results' => $results
	    );
	    
	    $this->load->view('add_dod',$data);
	}
	
	public function save_dod_position()
	{
	    $divid=$this->security->xss_clean($this->input->post('divids'));
	    $position=$this->security->xss_clean($this->input->post('divposition'));
	    
	    $this->load->model('Projectsmodel');
	    $this->Projectsmodel->set_dod_position( $divid,$position);
	    
	    $this->load->model('Projectsmodel');
	    $results = $this->Projectsmodel->get_dod_details();
	    $data = array(
	        'results' => $results
	    );
	    
	    // $this->load->view('menu');
	    // $this->load->view('border');
	    $this->load->view('add_dod',$data);
	}
	
	public function update_dod()
	{
	    echo ($this->security->xss_clean($this->input->post('dod_id')));
	    $dodid=$this->security->xss_clean($this->input->post('dod_id'));
	    $description=$this->security->xss_clean($this->input->post('description'));
	    
	    //echo($this->input->post('definition',TRUE));
	    
	    $this->load->model('Projectsmodel');
	    $this->Projectsmodel->update_dod( $dodid,$description);
	    
	    $this->load->model('Projectsmodel');
	    $results = $this->Projectsmodel->get_dod_details();
	    $data = array(
	        'results' => $results
	    );
	    
	    $this->load->view('add_dod',$data);
	}
	
	public function delete_dod()
	{
	    $dodid=$this->security->xss_clean($this->input->post('dod_id'));
	    
	    //echo($this->input->post('definition',TRUE));
	    
	    $this->load->model('Projectsmodel');
	    $this->Projectsmodel->delete_dod( $dodid);
	    
	    $this->load->model('Projectsmodel');
	    $results = $this->Projectsmodel->get_dod_details();
	    $data = array(
	        'results' => $results
	    );
	    
	    $this->load->view('add_dod',$data);
	}
	
	
	
	
	
	
	
	public function onChangeViewBacklog()
	{
	    
	        $projectId=$this->input->post('pid',TRUE);
	        $this->session->set_userdata("project_id", $projectId);
	      //  print_r("Project Id="+$projectId);
	       // $menuType=$this->input->post('menuType',TRUE);
	        $this->submenu($projectId,"backlog");
	  
	}
	
	public function ViewRetrospective()
	{
	    $this->session->set_userdata("retro_project_type", "retro_type");
	    if ($this->session->user_name != null) {
	        $this->load->model('Loginmodel');
	        $user_type_id = $this->session->userdata('user_type_id');
	        $data ['menu_name'] = $this->Loginmodel->get_main_menu_list($user_type_id,'RETROSPECTIVE');
	        $this->session->set_userdata('first_level_menu',$data);
	        
	        $this->load->model('Projectsmodel');
	        $results = $this->Projectsmodel->get_retro(null);
	        $data = array(
	            'results' => $results
	        );
	        
	        $this->load->view('menu');
	        $this->load->view('border');
	        $this->load->view('view_retrospective',$data);
	    }
	}
	
	public function delete_files($fileName) {
	    $this->load->model ( 'Projectsmodel' );
	    $this->Projectsmodel->delete_files ( $fileName );
	    $results=$this->Projectsmodel->get_project_files($this->session->userdata('project_id'));
	    $data=array('results'=>$results);
	    $this->load->view('menu');
	    $this->load->view('border');
	    $this->load->view('add_project_files',$data);
	}	
}