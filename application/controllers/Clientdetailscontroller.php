<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientdetailscontroller extends CI_Controller {

	function __construct() {
		parent::__construct();
		// Load url helper
		$this->load->helper('url');
		$this->load->library('session');
	}
	/* Method to insert Client Details */
	public function index()
	{		
		$datam= array(
		'organization_name'=> $this->input->post('orgname',TRUE),
		'contact_person'=>$this->input->post('contactperson',TRUE),
		'phone_no'=>$this->input->post('phone',TRUE),
		'email'=>$this->input->post('email',TRUE),
		'address'=>$this->input->post('address',TRUE),		
		'skype'=>$this->input->post('skype',TRUE));

		$input_data = $this->input->post();
		//dump($input_data);
		$input_data['c_logo'] = "";
			if (!empty($_FILES['userfile']['name']))
			{				
				$config['upload_path'] = UPLOAD_PATH_URL.'client/';
				$config['allowed_types'] = 'gif|jpg|png|bmp';
				$config['file_name'] = time().strtolower(str_replace(' ','-',$_FILES['userfile']['name']));	
				$this->load->library('upload');
				$this->upload->initialize($config);
				
				if ($this->upload->do_upload('userfile'))
				{
					$report_image_arr = $this->upload->data();
					$datam['c_logo'] = $report_image_arr['file_name'];					
				}
			}

		$this->load->model('Clientmodel');
		//dumpEx($datam);
		$this->Clientmodel->insert_client_details($this->security->xss_clean($datam));		
		$results=$this->Clientmodel->view_clients_overview();
		$data=array('results'=>$results);
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('clients',$data);		
	}	
	/* Method to edit client details @param  $client_id */
	public function edit_client($client_id_params)
	{	
		$client_id=$this->security->xss_clean($client_id_params);
	//echo("PHONE==".$this->input->post('phone_no'));
		$datam= array(
			'organization_name'=> $this->input->post('organization_name',TRUE),
			'contact_person'=>$this->input->post('contact_person',TRUE),
			'phone_no'=>$this->input->post('phone_no',TRUE),
			'email'=>$this->input->post('email',TRUE),
			'address'=>$this->input->post('address',TRUE),				
			'skype'=>$this->input->post('skype',TRUE),
		);

		$input_data['c_logo'] = "";
		if (!empty($_FILES['userfile']['name']))
		{				
			$config['upload_path'] = UPLOAD_PATH_URL.'client/';
			$config['allowed_types'] = 'gif|jpg|png|bmp';
			$config['file_name'] = time().strtolower(str_replace(' ','-',$_FILES['userfile']['name']));	
			$this->load->library('upload');
			$this->upload->initialize($config);
			
			if ($this->upload->do_upload('userfile'))
			{
				$report_image_arr = $this->upload->data();
				$datam['c_logo'] = $report_image_arr['file_name'];					
			}
		}
		$this->load->model('Clientmodel');
		$this->Clientmodel->update_client($client_id,$this->security->xss_clean($datam));	
		$results=$this->Clientmodel->view_clients_overview();
		$data=array('results'=>$results);
		$this->load->view('menu');
		$this->load->view('border');
		$this->load->view('clients',$data);	
	}
}