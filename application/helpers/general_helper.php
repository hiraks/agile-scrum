<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
	function companyDetails($id){
		$CI =& get_instance();		
		$sql = "select * from ag_company_mst where company_id = {$id}";	
		//echo "[".$sql."]";	
		$datawq = $CI->db->query($sql)->result_array();	
		return $datawq;
	}	
	function userDetails($id){
		$CI =& get_instance();		
		$sql = "select * from ag_users where user_id = {$id}";		
		$datauser = $CI->db->query($sql)->result_array();	
		return $datauser;
	}
	function message_count($id){
		$CI =& get_instance();		
		$sql = "select count(*) mc from ag_message where message_send_to = 1 and message_view='N' and message_status='S'";	
		$datauser = $CI->db->query($sql)->result_array();	
		return $datauser;
	}	
	function dump($data){
		echo '<pre>';
		print_r($data);
		echo '</pre>';	
	}	
	function dumpEx($data){
		echo '<pre>';
		print_r($data);
		echo '</pre>';
		exit;
	}	
	function sys_date($date){
		if(($date!='0000-00-00')&&($date!='')){
			$date = str_replace('/', '-', $date);			
			$d = date('Y-m-d', strtotime($date));
			$dn = date_create($d);
			$dn = date_format($dn,"Y-m-d");
			return $dn;
		}else{
			return $date;
		}			
	}	
	function form_date($date){
		//echo $date.'<br/>';
		if($date!='0000-00-00'){
			$date = str_replace('-', '/', $date);			
			$d = date('d/m/Y', strtotime($date));
			return $d;
		}else{
			return '00/00/00';
		}			
	}	
	function auth(){
		$CI =& get_instance();
		if($CI->session->userdata('user_id')==''){
			redirect('home/login');
		}
	}	
	function logged_in_user(){
		$CI =& get_instance();
		return $CI->session->userdata('user_id');
	}
	function logged_in_user_group(){
		$CI =& get_instance();
		return $CI->session->userdata('user_group');
	}
	function getGeneralName($name){
		$name =  str_replace('_', ' ', $name);
		return ucwords($name);
	}		
	function dbQuery($sql){
		if (strpos($sql,'delete') !== false){ 
			echo 'Delete query is restricated ';
			exit;
		}
		if (strpos($sql,'update') !== false){
			echo 'Update query is restricated '; 
			exit;
		}
		if($sql=='' || $sql=='0'){
			echo 'query not defined';exit;
		}		
		$CI =& get_instance(); 
		$result = 0;
		$result = $CI->db->query($sql)->row_array();
		if(isset($result) && isset($result['result'])){			
			return $result['result'];
		}else{
			$result = '';			
			return $result;
		}
	}	
	function dbQuery1($sql){
		$resultsds = '';
		if (strpos($sql,'delete') !== false){ 
			echo 'Delete query is restricated ';
			exit;
		}
		if (strpos($sql,'update') !== false){
			echo 'Update query is restricated '; 
			exit;
		}
		if($sql=='' || $sql=='0'){
			echo 'query not defined';exit;
		}
		$CI =& get_instance(); 
		
		$resultsds = $CI->db->query($sql)->row_array();
		//echo $sql.'<br/>';dump($resultsds).'<br/>';
		$sql='';
		if(isset($resultsds) && isset($resultsds['current'])){
			return $resultsds['current'];
		}else if(isset($resultsds) && isset($resultsds['cummulative'])){
			return $resultsds['cummulative'];
		}else{			
			$resultsds = '0';
			return $resultsds;
		}
	}	