<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Projectsmodel extends CI_Model {
	
	/**
	 * Default constructor.
	 */
	public function __construct() {
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}	
	/**
	 * Method to insert Project Details
	 *
	 * @param Array $data
	 *        	in Name Value pair
	 */
	public function insert_project_details($data) {
		$this->db->insert('ag_project_mst',$data);
	}	
	public function update_project_details($data,$project_id) {
		$this->db->where('project_id',$project_id);
		$this->db->update('ag_project_mst',$data);
	}
	/**
	 * Method to insert Invoice information
	 *
	 * @param Array $data
	 *        	in Name Value pair
	 */
	public function insert_invoice($data) {
		$this->db->insert('ag_invoice_txn',$data);
	}	
	/**
	 * Method to insert Invoice Details of a particular invoice
	 *
	 * @param Array $data
	 *        	in Name Value pair
	 */
	public function insert_invoice_details($data){
		$this->db->insert('ag_invoice_list_details',$data);
	}	
	/**
	 * Method to delete Invoice Details of a particular invoice
	 *
	 * @param
	 *        	$invoice_list_id
	 */
	public function delete_invoice_details($invoice_list_id) {
		$this->db->where('list_id',$invoice_list_id);
		$this->db->delete('ag_invoice_list_details');
	}	
	/**
	 * Method to delete Invoice information
	 *
	 * @param
	 *        	$invoice_id
	 */
	public function delete_invoice($invoice_id) {
		$this->db->where('invoice_id',$invoice_id);
		$this->db->delete('ag_invoice_txn');
	}	
	/**
	 * Methods to get Projects overview
	 * 
	 * @param
	 *        	$client_id
	 */
	public function view_projects_overview($client_id){
		if ($client_id == null or $client_id == '0'){
			$queryParam = 'ag_project_mst.client_id = ag_client_details.client_id';
		} else {
			$queryParam = 'ag_project_mst.client_id = ag_client_details.client_id and ag_client_details.client_id=' . $client_id;
		}		
		$this->db->select ( 'project_id,Name,ag_client_details.organization_name client_name,resources_allocatted,start_date,end_date,estimated_hours');
		$this->db->from('ag_project_mst');
		$this->db->join('ag_client_details',$queryParam);
		$query = $this->db->get();
		return $query->result();
	}	
	
	
	public function view_project_details($project_id){
	    $queryParam = 'ag_project_mst.client_id = ag_client_details.client_id and ag_project_mst.project_id=' . $project_id;
	    $this->db->select ( 'project_id,Name,ag_client_details.organization_name client_name,resources_allocatted,start_date,end_date,estimated_hours,fixed_rate,hourly_rate,Notes');
	    $this->db->from('ag_project_mst');
	    $this->db->join('ag_client_details',$queryParam);
	    $query = $this->db->get();
	    return $query->result();
	}	
	
	/**
	 * Method to get Current Invoice Id.
	 */
	public function get_current_invoiceId(){
		$this->db->select_max('invoice_id');
		$query = $this->db->get('ag_invoice_txn');
		return $query->result();
	}	
	/**
	 * Method to get Invoice Information based on invoice id.
	 * 
	 * @param
	 *        	$invoice_id
	 */
	public function get_invoice_info($invoice_id) {
		$this->db->select( '*' );
		$this->db->from('ag_invoice_txn');
		$this->db->where ('invoice_id',$invoice_id);
		// $this->db->where('status','Not Started');
		$query = $this->db->get ();
		return $query->result ();
	}	
	/* Method to get To do List @param $project_id */
	public function get_todo_list($project_id){
	    if ($project_id != null){
	    $queryParam = 'ag_issues_txn.reported_by = ag_users.username and ag_issues_txn.project_id='."'". $project_id."'";
	    }
	    else {
	        $queryParam = 'ag_issues_txn.reported_by = ag_users.username ';
	    }
	    $this->db->select( '*' );
		$this->db->from('ag_issues_txn');
		$this->db->join('ag_users',$queryParam);
		$this->db->order_by("ag_issues_txn.created_date", "desc");
		$this->db->limit("15");
		
		//$this->db->where('project_id', $project_id);
		$query = $this->db->get();
		
		//echo("LAST query=".$this->db->last_query());
		
		return $query->result();
	}
	
	/**
	 * 
	 * @param  $project_id
	 * @return 
	 */
	public function get_expense_graph(){
	  //  $sql = "SELECT * FROM some_table WHERE id IN ? AND status = ? AND author = ?";
	    $sql= "SELECT  concat(DATE_FORMAT(created_on,'%b'),'-',DATE_FORMAT(created_on,'%y')) AS YEAR , total_amt , exp_amount FROM ag_invoice_txn a
JOIN ag_expenses b WHERE DATE_FORMAT(a.created_on,'%b')= DATE_FORMAT(b.created_date,'%b') and
 MONTH(created_on) BETWEEN MONTH(CURRENT_DATE - INTERVAL 5 MONTH) and MONTH(CURRENT_DATE - INTERVAL 0 MONTH)";
	  //  $this->db->query($sql, array(array(3, 6), 'live', 'Rick'));
	    $this->db->query($sql);
	    
	 
	    $query = $this->db->query($sql);
	    
	   // echo("LAST query=".$this->db->last_query());
	    
	    return $query->result();
	}
	
	
	
	/* Method to get Total hours logged. @param	$project_id	 */
	public function get_total($project_id) {
		$this->db->select('sum(logged_hours) as loggedhours');
		$this->db->from('ag_timesheet_details');
	}	
	/* Method to get Project count based on client id. @param  	$client_id	 */
	public function get_project_count($client_id){
		if ($client_id == null or $client_id == '0'){
			$this->db->select('count(*) as project_count');
			$this->db->from('ag_project_mst');
			// $this->db->where('project_id',$project_id);
			// $this->db->where('status','Not Started');
			$query = $this->db->get();
			$ret = $query->row();
			return $ret->project_count;
		} else {
			$this->db->select('count(*) as project_count');
			$this->db->from('ag_project_mst' );
			$this->db->where('client_id', $client_id);
			// $this->db->where('status','Not Started');
			$query = $this->db->get();
			$ret = $query->row();
			return $ret->project_count;
		}
	}	
	/* Method to get OpenIssue count based on client id.  @param$client_id	 */
	public function get_open_issue_count($client_id){
		if ($client_id == null or $client_id == '0'){
			$this->db->select('count(*) as open_issue_count');
			$this->db->from('ag_issues_txn');
			$this->db->where('status !=', "Closed");
			// $this->db->where('status','Not Started');
			$query = $this->db->get();
			$ret = $query->row();
			return $ret->open_issue_count;
		} else {
			$this->db->select('count(*) as open_issue_count');
			$this->db->from('ag_issues_txn');			
			$this->db->join ('ag_project_mst', 'ag_issues_txn.project_id = ag_project_mst.project_id and ag_project_mst.client_id=' . $client_id);
			$this->db->where ('status !=',"Closed");
			$query = $this->db->get();
			$ret = $query->row();
			return $ret->open_issue_count;
		}
	}	
	/* Method to get Closed Issue count based on client id.@param $client_id*/
	public function get_closed_issue_count($client_id){
		if ($client_id == null or $client_id == '0'){
			$this->db->select ( 'count(*) as closed_issue_count' );
			$this->db->from ( 'ag_issues_txn' );
			$this->db->where ( 'status =', "Closed" );
			// $this->db->where('status','Not Started');
			$query = $this->db->get ();
			$ret = $query->row ();
			return $ret->closed_issue_count;
		} else {
			$this->db->select('count(*) as closed_issue_count');
			$this->db->from('ag_issues_txn');			
			$this->db->join('ag_project_mst','ag_issues_txn.project_id = ag_project_mst.project_id and ag_project_mst.client_id=' . $client_id);
			$this->db->where ('status =',"Closed");
			$query = $this->db->get();
			$ret = $query->row();
			return $ret->closed_issue_count;
		}
	}	
	/** Method to get Total Logged hours based on client id.*  @param $client_id */
	public function get_total_logged_hours($client_id) {
		if ($client_id == null or $client_id == '0') {
			$this->db->select ('sum(logged_hours) as loggedhours');
			$this->db->from ('ag_timesheet_details');
			// $this->db->where('status =',"Closed");
			// $this->db->where('status','Not Started');
			$query = $this->db->get ();
			$ret = $query->row ();
			return $ret->loggedhours;
		} 
		else {
			$this->db->select('sum(logged_hours) as loggedhours');
			$this->db->from('ag_timesheet_details');			
			$this->db->join('ag_project_mst', 'ag_timesheet_details.project_id = ag_project_mst.project_id and ag_project_mst.client_id=' . $client_id);
			// $this->db->where('status =',"Closed");
			$query = $this->db->get();
			$ret = $query->row();
			return $ret->loggedhours;
		}
	}	
	/** Method to get Project count based on client id.* @param	$client_id */
	public function get_total_earnings_last_month($client_id) {
		if ($client_id == null or $client_id == '0') {
			$this->db->select ( 'sum(paid_amt) as lm_earnings' );
			$this->db->from ( 'ag_invoice_txn' );
			$this->db->where ( 'pay_status =', "Paid" );
			$this->db->where ( 'MONTH(created_on)=MONTH(CURRENT_DATE - INTERVAL 1 MONTH)' );
			$query = $this->db->get ();
			// echo($this->db->last_query());
			$ret = $query->row ();
			return $ret->lm_earnings;
		} 
		else {
			$this->db->select ( 'sum(paid_amt) as lm_earnings' );
			$this->db->from ( 'ag_invoice_txn' );			
			// $this->db->join('ag_project_mst', 'ag_invoice_txn.project_id = ag_project_mst.project_id and ag_project_mst.client_id='.$client_id);
			$this->db->where ( 'client_id=', $client_id );
			$this->db->where ( 'pay_status =', "Paid" );
			$this->db->where ( 'MONTH(created_on)=MONTH(CURRENT_DATE - INTERVAL 1 MONTH)' );
			$query = $this->db->get ();
			$ret = $query->row ();
			return $ret->lm_earnings;
		}
	}	
	/*Method to get total bills last month based on client id.@param $client_id	 */
	public function get_total_bill_last_month($client_id) {
		if ($client_id == null or $client_id == '0') {
			$this->db->select ( 'sum(total_amt) as lm_bill' );
			$this->db->from ( 'ag_invoice_txn' );
			$this->db->where ( 'pay_status =', "Paid" );
			$this->db->where ( 'MONTH(created_on)=MONTH(CURRENT_DATE - INTERVAL 1 MONTH)' );
			$query = $this->db->get ();
			// echo($this->db->last_query());
			$ret = $query->row ();
			return $ret->lm_bill;
		} else {
			$this->db->select ( 'sum(total_amt) as lm_bill' );
			$this->db->from ( 'ag_invoice_txn' );
			
			// $this->db->join('ag_project_mst', 'ag_invoice_txn.project_id = ag_project_mst.project_id and ag_project_mst.client_id='.$client_id);
			$this->db->where( 'client_id=', $client_id );
			$this->db->where( 'pay_status =', "Paid" );
			$this->db->where( 'MONTH(created_on)=MONTH(CURRENT_DATE - INTERVAL 1 MONTH)' );
			$query = $this->db->get();
			$ret = $query->row();
			return $ret->lm_bill;
		}
	}	
	/* Method to get total pending bills last month based on client id.@param $client_id */
	public function get_total_pending_bill_last_month($client_id) {
		if ($client_id == null or $client_id == '0') {
			$this->db->select ( 'sum(total_amt-paid_amt) as lm_pending' );
			$this->db->from ( 'ag_invoice_txn' );
			$this->db->where ( 'pay_status =', "Paid" );
			$this->db->where ( 'MONTH(created_on)=MONTH(CURRENT_DATE - INTERVAL 1 MONTH)' );
			$query = $this->db->get ();
			// echo($this->db->last_query());
			$ret = $query->row ();
			return $ret->lm_pending;
		} else {
			$this->db->select ( 'sum(total_amt-paid_amt) as lm_pending' );
			$this->db->from ( 'ag_invoice_txn' );
			
			// $this->db->join('ag_project_mst', 'ag_invoice_txn.project_id = ag_project_mst.project_id and ag_project_mst.client_id='.$client_id);
			$this->db->where ( 'client_id=', $client_id );
			$this->db->where ( 'pay_status =', "Paid" );
			$this->db->where ( 'MONTH(created_on)=MONTH(CURRENT_DATE - INTERVAL 1 MONTH)' );
			$query = $this->db->get ();
			$ret = $query->row ();
			return $ret->lm_pending;
		}
	}	
	/* Method to get all to do list based on client id. @param $client_id */
	public function get_all_todo_list($client_id) {
		if ($client_id == null or $client_id == '0') {
			$this->db->select ( '*' );
			$this->db->from ( 'ag_issues_txn' );
			$this->db->join ( 'ag_users', 'ag_issues_txn.assigned_to=ag_users.username' );
			// $this->db->where('status','Not Started');
			$query = $this->db->get ();
			//print_r($query->result ());
			return $query->result ();
		} else {
			$this->db->select ( '*' );
			$this->db->from ( 'ag_issues_txn' );
			
			$this->db->join ( 'ag_project_mst', 'ag_issues_txn.project_id = ag_project_mst.project_id and ag_project_mst.client_id=' . $client_id );
			// $this->db->where('status =',"Closed");
			$query = $this->db->get ();
			return $query->result ();
		}
	}	
	/*Method to get tasks in progress list based on project id.@param $project_id*/
	public function get_in_progress_list($project_id) {
		$this->db->select ( '*' );
		$this->db->from ( 'ag_issues_txn' );
		$this->db->where ( 'project_id', $project_id );
		$this->db->where ( 'status', 'In Progress' );
		$this->db->where ( 'status', 'Resoved' );
		$this->db->where ( 'status', 'Reopened' );
		$query = $this->db->get ();
		return $query->result ();
	}	
	/* Method to get invoice list. */
	public function get_invoice_list($user_type,$client_id) {
		if($user_type=='3')
		{
		$this->db->select ( '*' );
		$this->db->from ( 'ag_invoice_txn' );
		$this->db->join ( 'ag_client_details', 'ag_invoice_txn.client_id = ag_client_details.client_id and ag_invoice_txn.client_id='.$client_id );
		}
		else {
			$this->db->select ( '*' );
			$this->db->from ( 'ag_invoice_txn' );
			$this->db->join ( 'ag_client_details', 'ag_invoice_txn.client_id = ag_client_details.client_id' );		
		}		
		$query = $this->db->get ();
		return $query->result ();
	}	
	/* Method to get payments list.	 */
	public function get_payments_list($user_type,$client_id) {
		if($user_type=='3')
		{
		$this->db->select ( '*' );
		$this->db->from ( 'ag_invoice_txn' );
		$this->db->join ( 'ag_client_details', 'ag_invoice_txn.client_id = ag_client_details.client_id  and ag_invoice_txn.client_id='.$client_id.' and pay_status=' . "'Paid'" );
		}		
		else {
			$this->db->select ( '*' );
			$this->db->from ( 'ag_invoice_txn' );
			$this->db->join ( 'ag_client_details', 'ag_invoice_txn.client_id = ag_client_details.client_id and pay_status=' . "'Paid'");				
		}		
		$query = $this->db->get ();
		return $query->result ();
	}	
	/* Get invoice list details based on invoice id.@param $invoice_id	 */
	public function get_invoice_details($invoice_id) {
		$this->db->select ( '*' );
		$this->db->from ( 'ag_invoice_list_details' );
		$this->db->where ( 'invoice_id', $invoice_id );
		$query = $this->db->get ();
		return $query->result ();
	}	
	/* Method to get company details */
	public function get_company_details() {
		$this->db->select ( '*' );
		$this->db->from ( 'ag_company_mst' );
		$this->db->where ( 'company_id', 1 );
		$query = $this->db->get ();
		return $query->result ();
	}
	/* Method to get default language */
	public function get_default_language() {
	    $this->db->select ( 'lang' );
	    $this->db->from ( 'ag_company_mst' );
	    $this->db->where ( 'company_id', 1 );
	    $query = $this->db->get ();
	    $ret = $query->row ();
	    return $ret->lang;
	}
	
		
	/* Method to update company details @param Array $data  */
	public function update_company_details($data) {
		$this->db->where('company_id', 1 );
		$this->db->update('ag_company_mst', $data );
	}	
	/* Method to update invoice transaction details @param Array $data  param$invoice_id */
	public function update_invoice_txn($data, $invoice_id){
		$this->db->where('invoice_id',$invoice_id);
		$this->db->update('ag_invoice_txn',$data);
	}	
	/* Method to get total invoice amount of an individual invoice. @param $invoice_id */
	public function get_total_amount($invoice_id){
		$this->db->select('sum(row_total) as sum_total');
		$this->db->from('ag_invoice_list_details');
		$this->db->where('invoice_id =',$invoice_id);
		$query = $this->db->get();
		$ret = $query->row();
		return $ret->sum_total;
	}
	public function insert_event_details($data){
		$this->db->insert('ag_event',$data);
	}
	public function view_event_overview($user_type_id){
		/*if ($client_id == null or $client_id == '0') {
			$queryParam = 'ag_project_mst.client_id = ag_client_details.client_id';
		} else {
			$queryParam = 'ag_project_mst.client_id = ag_client_details.client_id and ag_client_details.client_id=' . $client_id;
		}*/		
		$this->db->select('event_title,event_startdate,event_enddate,event_description');
		$this->db->from('ag_event');		
		$this->db->where('created_by',$user_type_id);		
		$query = $this->db->get();
		return $query->result();
	}
	public function view_messages_overview($message_send_to){		
		$this->db->select('message_id,message_title,message_descrription,message_attachment, 	message_send_to,message_status,message_send_by,message_created_on,message_created_by');
		$this->db->from('ag_message');	
		$this->db->where('message_send_to',$message_send_to);
		$this->db->where('message_status','S');
		$query = $this->db->get();
		return $query->result();
	}
	public function view_messages_sent($user_id){		
		$this->db->select('message_id,message_title,message_descrription,message_attachment, 	message_send_to,message_status,message_send_by,message_created_on,message_created_by');
		$this->db->from('ag_message');	
		$this->db->where('message_send_by',$user_id);
		$this->db->where('message_status','S');
		$query = $this->db->get();
		return $query->result();
	}
	public function view_messages_trash($user_id){		
		$this->db->select('message_id,message_title,message_descrription,message_attachment, 	message_send_to,message_status,message_send_by,message_created_on,message_created_by');
		$this->db->from('ag_message');	
		$this->db->where('message_send_to',$user_id);
		$this->db->where('message_status','D');
		$query = $this->db->get();
		return $query->result();
	}
	public function view_messages_details($message_id){
		$sql = "select * from ag_message where message_id='$message_id'";
		//echo "[$sql]";exit;	
		$result = $this->db->query($sql)->row_array();	
		return $result;
	}
	public function update_messages_count($message_id){
		$datam = array('message_view'=>'Y');
		$this->db->where('message_id',$message_id);		
		$this->db->update('ag_message',$datam);		
	}
	public function view_events_details($created_by){		
		$this->db->select('*');
		$this->db->from('ag_event');	
		$this->db->where('created_by',$created_by);		
		$query = $this->db->get();
		return $query->result();
	}
	public function update_event_details($data,$eventid){
		$this->db->where('event_id',$eventid);		
		$this->db->update('ag_event',$data);		
	}
	public function get_project($project_id){
		$this->db->select( '*' );
		$this->db->from('ag_project_mst');
		$this->db->where('project_id', $project_id);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_sprint_list($project_id,$sprint_id){
		$this->db->select( '*' );
		$this->db->from('ag_issues_txn');
		$this->db->join ( 'ag_users', 'ag_issues_txn.assigned_to=ag_users.username' );
		//$this->db->where('project_id', $project_id);
		$this->db->where('sprint_id', $sprint_id);
		$query = $this->db->get();
		//echo($this->db->last_query());
		return $query->result();
	}	
	public function get_note_list($created_by){
		$this->db->select('*');
		$this->db->from('ag_notes');
		$this->db->where('created_by', $created_by);
		$query = $this->db->get();
		return $query->result();
	}	
	public function add_notes($data)
	{
		$this->db->insert('ag_notes',$data);
	}
	public function display_notes($note_id)
	{
		$this->db->select('notes_id,notes_title,priority,note_description');
		$this->db->from('ag_notes');
		$this->db->where('notes_id',$note_id);
		$query = $this->db->get();
		//$kk = $this->db->last_query();echo "[".$kk."]";exit;		
		return $query->result();
	}
	public function update_notes($datam,$notes_id)
	{
		$this->db->where('notes_id',$notes_id);
		$this->db->update('ag_notes',$datam);
	}
	public function delete_notes($notes_id)
	{
		$this->db->where('notes_id',$notes_id);
		$this->db->delete('ag_notes');
	}
	public function get_ticket_list($created_by){
		$this->db->select( '*' );
		$this->db->from('ag_tickets');
		$this->db->where('created_by', $created_by);
		$query = $this->db->get();
		//$kk = $this->db->last_query();echo "[".$kk."]";exit;
		return $query->result();
	}
	public function add_ticket($data)
	{
		$this->db->insert('ag_tickets',$data);
	}
	public function display_ticket($ticket_id)
	{
		$this->db->select('*');
		$this->db->from('ag_tickets');
		$this->db->where('ticket_id',$ticket_id);
		$query = $this->db->get();
		//$kk = $this->db->last_query();echo "[".$kk."]";exit;		
		return $query->result();
	}
	public function update_ticket($datam,$ticket_id)
	{
		$this->db->where('ticket_id',$ticket_id);
		$this->db->update('ag_tickets',$datam);
	}
	public function delete_ticket($ticket_id)
	{
		$this->db->where('ticket_id',$ticket_id);
		$this->db->delete('ag_tickets');
	}
	public function display_status()
	{
		$this->db->select('*');
		$this->db->from('ag_status');		
		$query = $this->db->get();
		//$kk = $this->db->last_query();echo "[".$kk."]";exit;		
		return $query->result();
	}
	public function count_ticket_status($status_id,$user_id){		
		$this->db->select('count(*) as status_count');
		$this->db->from('ag_tickets');	
		$this->db->where('created_by',$user_id);		
		$this->db->where('status_id',$status_id);		
		$query = $this->db->get();
		return $query->result();
	}
	public function del_tax_details(){
		$this->db->truncate('ag_tax');		
	}
	public function insert_tax_details($taxdata){		
		$this->db->insert('ag_tax',$taxdata);
	}
	public function get_tax_details()
	{
		$this->db->select('*');
		$this->db->from('ag_tax');		
		$query = $this->db->get();
		return $query->result();
	}
	public function get_expenses_list($created_by){
		$this->db->select( '*' );
		$this->db->from('ag_expenses');
		$this->db->where('created_by', $created_by);
		$query = $this->db->get();
		//$kk = $this->db->last_query();echo "[".$kk."]";exit;
		return $query->result();
	}
	public function add_expenses($data)
	{
		$this->db->insert('ag_expenses',$data);
	}
	public function display_expenses($exp_id)
	{
		$this->db->select('*');
		$this->db->from('ag_expenses');
		$this->db->where('exp_id',$exp_id);
		$query = $this->db->get();
		//$kk = $this->db->last_query();echo "[".$kk."]";exit;		
		return $query->result();
	}
	public function update_expenses($datam,$exp_id)
	{
		$this->db->where('exp_id',$exp_id);
		$this->db->update('ag_expenses',$datam);
	}
	public function delete_expenses($exp_id)
	{
		$this->db->where('exp_id',$exp_id);
		$this->db->delete('ag_expenses');
	}
	public function all_ticket_list(){
		$this->db->select('*');
		$this->db->from('ag_tickets');
		$this->db->order_by("ticket_id", "desc");
		$this->db->limit(5, 0);
		$query = $this->db->get();
		return $query->result();
	}
	public function all_issue_list(){
		$this->db->select('*');
		$this->db->from('ag_issues_txn');
		$this->db->order_by("issue_id", "desc");
		$this->db->limit(5, 0);
		$query = $this->db->get();
		return $query->result();
	}
	public function all_project_list(){
		$this->db->select('*');
		$this->db->from('ag_project_mst');
		$this->db->order_by("project_id", "desc");
		/*$this->db->limit(5, 0);*/
		$query = $this->db->get();
		return $query->result();
	}
	public function all_sprint_list(){
		$this->db->select('*');
		$this->db->from('ag_sprint');
		$this->db->order_by("sprint_id", "desc");
		$this->db->limit(5, 0);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function save_dor_details($dordata){
	    $this->db->insert('ag_dor',$dordata);
	}
	
	public function get_dor_details(){
	    $this->db->select('*');
	    $this->db->from('ag_dor');
	    $this->db->order_by("position");
	    $query = $this->db->get();
	    return $query->result();
	}
	public function save_dod_details($doddata){
	    $this->db->insert('ag_dod',$doddata);
	}
	
	public function get_dod_details(){
	    $this->db->select('*');
	    $this->db->from('ag_dod');
	    $this->db->order_by("position");
	    $query = $this->db->get();
	    return $query->result();
	}
	
	public function sum_expense($user_id){
	    $this->db->select('sum(exp_amount) as tot_exp');
	    $this->db->from('ag_expenses');
	    $this->db->where('created_by',$user_id);
	    $query = $this->db->get();
	    return $query->result();
	}
	public function sum_income($user_id){
	    $this->db->select('sum(paid_amt) as tot_income');
	    $this->db->from('ag_invoice_txn');
	    $this->db->where('client_id',$user_id);
	    $query = $this->db->get();
	    return $query->result();
	}
	public function get_retro($project_id){
	    $this->db->select('*');
	    $this->db->from('ag_retrospective');
	    
	    if($project_id!= null)
	    {
	    
	    $this->db->where('project_id',$project_id);
	    
	    }
	   
	    $this->db->order_by("position");
	    // $this->db->order_by("insert_time","desc");
	    $query = $this->db->get();
	    return $query->result();
	}
	/**
	 * Get Language properties from the selected one
	 * @param  $lang_name
	 * @return 
	 */
	public function get_lang($lang_name){
	    $this->db->select('label_prop,'.$lang_name.'_lang');
	    $this->db->from('ag_lang');
	    	    	    
	    //$this->db->order_by("position");
	    // $this->db->order_by("insert_time","desc");
	    $query = $this->db->get();
	    return $query->result();
	}
	
	public function set_dor_position($divids,$position)
	{
	   	    
	    $divids_arr = explode ("~", $divids);
	    $position_arr=explode ("~", $position);
	    
	    for ($x = 0; $x < (count($divids_arr)-1); $x++) {
	        
	        
	        $data = array(
	            'position' => $position_arr[$x]
	       );
	      	        
	        $this->db->where('dor_id', $divids_arr[$x]);
	        $this->db->update('ag_dor', $data);
	    }
	}
	
	public function update_dor($dor_id,$description)
	{
	    
	       $data = array(
	           'description' => $description
	        );
	        
	       $this->db->where('dor_id', $dor_id);
	        $this->db->update('ag_dor', $data);
	    }
	
	public function delete_dor($dor_id)
	
	{
	        $this->db->where('dor_id', $dor_id);
	        $this->db->delete('ag_dor');
	    }
	
	    
	    public function update_dod($dod_id,$description)
	    {
	        
	        $data = array(
	            'description' => $description
	        );
	        
	        $this->db->where('dod_id', $dod_id);
	        $this->db->update('ag_dod', $data);
	    }
	    
	  /*  public function save_dod_details($doddata){
	        $this->db->insert('ag_dod',$doddata);
	    }*/
	    
	  /*  public function get_dod_details(){
	        $this->db->select('*');
	        $this->db->from('ag_dod');
	        $this->db->order_by("position");
	        $query = $this->db->get();
	        return $query->result();
	    }*/
	   /* public function save_dod_details($doddata){
	        $this->db->insert('ag_dod',$doddata);
	    }*/
	    
	    public function set_dod_position($divids,$position)
	    {
	        
	        $divids_arr = explode ("~", $divids);
	        $position_arr=explode ("~", $position);
	        
	        for ($x = 0; $x < (count($divids_arr)-1); $x++) {
	            
	            
	            $data = array(
	                'position' => $position_arr[$x]
	            );
	            
	            $this->db->where('dod_id', $divids_arr[$x]);
	            $this->db->update('ag_dod', $data);
	        }
	    }
	    
	 /*   public function update_dod($dod_id,$description)
	    {
	        
	        $data = array(
	            'description' => $description
	        );
	        
	        $this->db->where('dod_id', $dod_id);
	        $this->db->update('ag_dod', $data);
	    }
	    */
	    public function delete_dod($dod_id)
	    
	    {
	        $this->db->where('dod_id', $dod_id);
	        $this->db->delete('ag_dod');
	    }
	    
	    public function save_project_files($filedata){
	        $this->db->insert('ag_project_files',$filedata);
	    }
	    public function get_project_files($project_id){
	        $this->db->select('*');
	        $this->db->from('ag_project_files');
	        $this->db->where('project_id',$project_id);
	       // $this->db->order_by("position");
	        // $this->db->order_by("insert_time","desc");
	        $query = $this->db->get();
	        return $query->result();
	    }
	    
	    public function delete_files($file_name)
	    
	    {
	        $this->db->where('file_name', $file_name);
	        $this->db->delete('ag_project_files');
	    }
	    
	    
	    
}