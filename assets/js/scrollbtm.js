    (function($){
      $(window).load(function(){
        
        $("#scroll_btn").mCustomScrollbar({
          scrollButtons:{enable:true,scrollType:"stepped"},
          keyboard:{scrollType:"stepped"},
          mouseWheel:{scrollAmount:188},
          theme:"rounded-dark",
          autoExpandScrollbar:true,
          snapAmount:188,
          snapOffset:65
        });
        
      });
    })(jQuery);