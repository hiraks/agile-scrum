--
-- Paste your SQL dump into this file
--

-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2018 at 05:12 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `agile`
--

-- --------------------------------------------------------

--
-- Table structure for table `ag_client_details`
--

CREATE TABLE IF NOT EXISTS `ag_client_details` (
  `client_id` int(11) NOT NULL,
  `organization_name` varchar(100) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `phone_no` bigint(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `skype` varchar(100) NOT NULL,
  `c_logo` varchar(255) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_client_details`
--

INSERT INTO `ag_client_details` (`client_id`, `organization_name`, `contact_person`, `phone_no`, `email`, `address`, `skype`, `c_logo`, `created_on`) VALUES
(1, 'SR Indusrties pvt. Ltd.', 'Sumon Roy', 8837749123, 'sumon@gmail.com', '', 'sumon123', '1517503899lighthouse.jpg', '2017-11-06 08:54:41'),
(2, 'SKR Pvt. Ltd.', 'Sudipa Sarkar', 5678901234, 'sudipa@gmail.com', '9/6/7 G.T. Road', 'sudipa789', '1517504050zender_logo.png', '2017-11-06 08:56:20'),
(3, 'SC Infotech', 'Suvo Chatterjee', 1904236787, 'sc@gmail.com', 'setstrt', 'sc345', '1517504071angular_logo.png', '2017-11-06 11:04:03'),
(4, 'Test ssss', 'Test fdsgsg', 576585685, 'ilmiya1123@gmail.com', 'tryryrty', 'utu788', '', '2017-11-06 11:12:58'),
(5, 'Test Organization Name', 'kk', 5765960, 'ss@gmail.com', '', 'yyry', '', '2017-12-13 10:44:30'),
(7, 'Sudipta Solution', 'Sujit', 9748837749, 'sudipta.suvo@gmail.com', 'Rishra', 'sudipta.chakraborty@gmail.com', '1517502538hydrangeas.jpg', '2018-02-01 16:28:58');

-- --------------------------------------------------------

--
-- Table structure for table `ag_color`
--

CREATE TABLE IF NOT EXISTS `ag_color` (
  `color_id` int(11) NOT NULL,
  `color_name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_color`
--

INSERT INTO `ag_color` (`color_id`, `color_name`) VALUES
(1, 'Red'),
(2, 'Green'),
(3, 'Blue'),
(4, 'Yellow'),
(5, 'Pink'),
(6, 'Black'),
(7, 'Orange');

-- --------------------------------------------------------

--
-- Table structure for table `ag_company_mst`
--

CREATE TABLE IF NOT EXISTS `ag_company_mst` (
  `company_id` int(10) unsigned DEFAULT NULL,
  `company_name` varchar(200) DEFAULT NULL,
  `company_address` varchar(1000) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `registration` varchar(50) DEFAULT NULL,
  `vat` varchar(50) DEFAULT NULL,
  `vat_percent` int(11) DEFAULT NULL,
  `logo` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_company_mst`
--

INSERT INTO `ag_company_mst` (`company_id`, `company_name`, `company_address`, `zipcode`, `city`, `state`, `country`, `email`, `phone`, `registration`, `vat`, `vat_percent`, `logo`) VALUES
(1, 'Agile Technology', '12/ K P Roy', '712250', NULL, NULL, NULL, 'sudipta.chakraborty@gmail.com', '123456', '45457', '6', 4, 'agile-crm.png');

-- --------------------------------------------------------

--
-- Table structure for table `ag_event`
--

CREATE TABLE IF NOT EXISTS `ag_event` (
  `event_id` int(11) NOT NULL,
  `event_title` varchar(255) NOT NULL,
  `event_startdate` datetime NOT NULL,
  `event_enddate` datetime NOT NULL,
  `event_description` varchar(255) NOT NULL,
  `event_color` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_event`
--

INSERT INTO `ag_event` (`event_id`, `event_title`, `event_startdate`, `event_enddate`, `event_description`, `event_color`, `created_by`, `created_on`) VALUES
(1, 'First Event', '2017-11-10 00:00:00', '0000-00-00 00:00:00', '', 'Red', 1, '2017-11-29 23:09:05'),
(2, 'Second Event', '2017-12-11 00:00:00', '0000-00-00 00:00:00', '', 'Green', 1, '2017-11-29 23:09:10'),
(3, 'Event Three', '2017-12-14 00:00:00', '0000-00-00 00:00:00', '', 'Blue', 1, '2017-11-29 23:09:17'),
(4, '4th event', '2018-01-03 00:00:00', '0000-00-00 00:00:00', '', 'Yellow', 1, '2017-11-30 06:12:44'),
(5, 'BDay party', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'Green', 1, '2017-11-30 06:12:51'),
(6, 'Meeting with CEO', '2017-11-29 00:00:00', '2017-11-30 00:00:00', 'Meeting with CEO for work', 'Yellow', 1, '2017-11-29 23:09:23'),
(7, 'Meeting with MIS', '2017-11-15 00:00:00', '2017-11-15 00:00:00', 'Meeting with MIS work', 'Red', 1, '2017-11-30 06:13:19'),
(8, 'Meeting with Admin', '2017-11-07 00:00:00', '2017-11-08 00:00:00', 'Meeting with Admin', 'Orange', 1, '2017-11-29 23:09:31'),
(9, 'Meeting with HR', '2017-12-12 00:00:00', '2017-12-14 00:00:00', 'Meeting with HR', 'Black', 1, '2017-11-29 23:09:36'),
(10, 'picnic', '2017-11-29 00:00:00', '2017-11-30 00:00:00', 'picnic', 'Pink', 1, '2017-11-30 06:13:05'),
(11, 'paper work', '2017-11-29 00:00:00', '2017-11-30 00:00:00', 'paper work', 'Green', 1, '2017-11-30 06:12:56'),
(12, 'new joining', '2018-01-02 00:00:00', '2018-01-02 00:00:00', 'new joining', 'Orange', 1, '2017-11-30 06:13:14'),
(13, 'Journey Meeting', '2017-11-17 00:00:00', '2017-11-18 00:00:00', 'Description Journey Meeting', 'Pink', 1, '2017-11-30 06:13:01'),
(14, 'Journey Meeting', '2017-11-17 00:00:00', '2017-11-18 00:00:00', 'Description Journey Meeting', 'Green', 1, '2017-11-30 06:12:53'),
(15, 'Doctor Checkup', '2017-11-30 00:00:00', '2017-11-30 00:00:00', 'Doctor Checkup', 'Pink', 1, '2017-11-30 06:13:00'),
(16, 'Doctor Checkup', '2017-11-24 00:00:00', '2017-11-24 00:00:00', 'Doctor Checkup for health', 'Pink', 1, '2017-11-29 23:15:19'),
(17, 'Journey Meeting', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Description Journey Meeting', 'Yellow', 1, '2017-11-29 23:32:56'),
(18, 'Office cleaning', '2017-11-27 00:00:00', '2017-11-29 00:00:00', 'Office cleaning', 'Green', 1, '2017-11-30 06:09:51'),
(19, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', 1, '2017-12-27 07:16:03');

-- --------------------------------------------------------

--
-- Table structure for table `ag_expenses`
--

CREATE TABLE IF NOT EXISTS `ag_expenses` (
  `exp_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `exp_purpose` varchar(255) NOT NULL DEFAULT '0',
  `exp_amount` double(12,2) NOT NULL DEFAULT '0.00',
  `exp_description` varchar(255) DEFAULT '0',
  `exp_date` date NOT NULL,
  `created_by` int(11) DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_expenses`
--

INSERT INTO `ag_expenses` (`exp_id`, `project_id`, `exp_purpose`, `exp_amount`, `exp_description`, `exp_date`, `created_by`, `created_date`) VALUES
(1, 0, 'Bill Payment', 1001.00, 'Test payment', '2018-01-04', 1, '2018-02-14 06:48:38'),
(2, 0, 'New Bill Payment', 2000.50, 'New Bill Payment New Bill Payment', '2018-02-01', 1, '2018-02-14 06:57:14'),
(3, 0, 'New Bill Payment2', 2500.50, 'New Bill Payment2 New Bill Payment2', '2018-02-05', 1, '2018-02-14 06:58:23');

-- --------------------------------------------------------

--
-- Table structure for table `ag_invoice_list_details`
--

CREATE TABLE IF NOT EXISTS `ag_invoice_list_details` (
  `list_id` int(11) NOT NULL,
  `invoice_id` int(11) DEFAULT '0',
  `items` varchar(500) DEFAULT '0',
  `Quantity` int(11) DEFAULT '0',
  `unit_price` float DEFAULT '0',
  `tax_id` int(11) NOT NULL,
  `tax` float DEFAULT '0',
  `row_total` float DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_invoice_list_details`
--

INSERT INTO `ag_invoice_list_details` (`list_id`, `invoice_id`, `items`, `Quantity`, `unit_price`, `tax_id`, `tax`, `row_total`) VALUES
(1, 1, 'item1', 2, 5.6, 1, 5, 11.872),
(2, 1, 'item2', 5, 4, 2, 6, 21.2),
(3, 2, 'Soap', 2, 76.9057, 1, 5, 163.04),
(4, 2, 'Cement', 10, 700, 2, 6, 7420),
(5, 1, 'ee', 43, 3, 3, 8, 136.74),
(6, 2, 'dfyghdf', 4, 5, 3, 8, 20.8),
(7, 1, 'Test Tax', 7, 5, 4, 12, 39.2),
(8, 1, 'test', 5, 10, 4, 12, 56);

-- --------------------------------------------------------

--
-- Table structure for table `ag_invoice_txn`
--

CREATE TABLE IF NOT EXISTS `ag_invoice_txn` (
  `invoice_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `pay_status` varchar(10) NOT NULL DEFAULT 'Unpaid',
  `due_date` date NOT NULL,
  `total_amt` int(11) NOT NULL,
  `paid_amt` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `default_tax` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `notes` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_invoice_txn`
--

INSERT INTO `ag_invoice_txn` (`invoice_id`, `client_id`, `pay_status`, `due_date`, `total_amt`, `paid_amt`, `created_on`, `default_tax`, `discount`, `currency`, `notes`) VALUES
(1, 1, 'Paid', '0000-00-00', 265, 265, '2018-02-27 15:44:20', 5, 1, 'USD', 'Test'),
(2, 2, 'Paid', '2017-12-26', 7604, 300, '2018-02-27 15:50:59', 5, 1, 'USD', 'qwrqewr'),
(3, 0, 'Unpaid', '0000-00-00', 0, 0, '2017-11-09 16:48:18', 0, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ag_issues_mst`
--

CREATE TABLE IF NOT EXISTS `ag_issues_mst` (
  `issue_type_id` int(100) NOT NULL,
  `issue_type` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_issues_mst`
--

INSERT INTO `ag_issues_mst` (`issue_type_id`, `issue_type`) VALUES
(1, 'Bug'),
(2, 'Story'),
(3, 'Epic'),
(4, 'Change Request');

-- --------------------------------------------------------

--
-- Table structure for table `ag_issues_txn`
--

CREATE TABLE IF NOT EXISTS `ag_issues_txn` (
  `issue_id` int(11) NOT NULL,
  `issue_type_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `summary` varchar(500) NOT NULL DEFAULT '0',
  `priority` varchar(50) NOT NULL DEFAULT '0',
  `description` varchar(500) DEFAULT '0',
  `attachment` varchar(500) DEFAULT '0',
  `filename` varchar(500) DEFAULT '0',
  `status` varchar(500) DEFAULT '0',
  `reported_by` varchar(500) DEFAULT '0',
  `assigned_to` varchar(500) DEFAULT '0',
  `epic_id` varchar(500) DEFAULT '0',
  `sprint_id` varchar(500) DEFAULT '0',
  `original_estimate` int(11) DEFAULT '0',
  `logged_estimate` int(11) DEFAULT '0',
  `remaining_estimate` int(11) DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_issues_txn`
--

INSERT INTO `ag_issues_txn` (`issue_id`, `issue_type_id`, `project_id`, `summary`, `priority`, `description`, `attachment`, `filename`, `status`, `reported_by`, `assigned_to`, `epic_id`, `sprint_id`, `original_estimate`, `logged_estimate`, `remaining_estimate`, `created_date`) VALUES
(1, 4, 1, 'Design Modification', 'Minor', 'Design Modification description', './uploads/', '', 'Not Started', 'admin', 'user', '0', '6', 10, 0, 0, '2017-11-06 11:58:31'),
(2, 2, 1, 'New Story', 'Major', 'New Story test', './uploads/', '', 'Closed', 'admin', 'sudipa', '0', '1', 5, 0, 0, '2017-11-08 07:29:10'),
(3, 1, 3, 'Test Bug', 'Minor', 'fjfgj', './uploads/', '', 'Not Started', 'admin', 'sudipa', '0', '0', 6, 0, 0, '2017-11-16 10:35:07'),
(4, 4, 1, 'Heading Change', 'Trivial', 'Heading Change', './uploads/', '', 'Progress', 'admin', 'sumon', '0', '1', 2, 0, 0, '2017-11-25 06:29:20'),
(5, 4, 1, 'Add New Section', 'Critical', 'Add New Section', './uploads/', '', 'Progress', 'admin', 'sudipa', '0', '0', 0, 0, 0, '2017-12-01 11:55:03'),
(6, 2, 1, 'Write New Story', 'Trivial', 'Write New Story on css training', './uploads/', '', 'Progress', 'admin', 'sumon', '0', '0', 23, 0, 0, '2017-12-01 11:56:22'),
(7, 3, 1, 'New epic', 'Trivial', 'New epic', './uploads/', '', 'Progress', 'admin', 'sumon', '0', '4', 13, 0, 0, '2017-12-01 11:57:25'),
(8, 1, 1, 'New Bug', 'Trivial', 'New Bug change', './uploads/', '', 'Progress', 'admin', 'sumon', '0', '6', 14, 0, 0, '2017-12-01 11:58:40'),
(9, 1, 1, 'Color Bug', 'Critical', 'Color Bug', './uploads/', '', 'Testing', 'admin', 'sudipa', '0', '4', 0, 0, 0, '2017-12-01 12:00:50'),
(10, 4, 1, 'Add new picture', 'Minor', 'add new picture in list', './uploads/', '', 'Progress', 'admin', 'sumon', '0', '6', 19, 1, 0, '2017-12-01 12:04:03');

-- --------------------------------------------------------

--
-- Table structure for table `ag_menu_mst`
--

CREATE TABLE IF NOT EXISTS `ag_menu_mst` (
  `menu_id` int(11) NOT NULL,
  `menu_name` varchar(100) NOT NULL DEFAULT '0',
  `sub_menu_name` varchar(100) NOT NULL DEFAULT '0',
  `sub_menu_flag` int(1) NOT NULL DEFAULT '0' COMMENT '0=no submenu and 1= submenu',
  `menu_url` varchar(1000) NOT NULL DEFAULT '#',
  `icon` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_menu_mst`
--

INSERT INTO `ag_menu_mst` (`menu_id`, `menu_name`, `sub_menu_name`, `sub_menu_flag`, `menu_url`, `icon`) VALUES
(1, 'DASHBOARD', '0', 0, 'Menus/Dashboard', 'fa fa-th-large'),
(2, 'CLIENTS', '0', 0, 'Menus/Clients', 'fa fa-group icon'),
(3, 'PROJECTS', '0', 0, 'Menus/Projects', 'fa fa-pencil'),
(4, 'Projects', 'Scrum Dashboard', 1, '#', NULL),
(5, 'Projects', 'Issues', 1, '#', NULL),
(6, 'Projects', 'Epic', 1, '#', NULL),
(7, 'Projects', 'Story', 1, '#', NULL),
(8, 'Projects', 'Sprint', 1, '#', NULL),
(9, 'Projects', 'Timesheets', 1, '#', NULL),
(10, 'Projects', 'Reports', 1, '#', NULL),
(11, 'USERS', '0', 0, 'Menus/Users', 'fa fa-user'),
(12, 'INVOICES', '0', 0, 'Menus/Invoices', 'fa fa-file'),
(13, 'PAYMENTS', '0', 0, 'Menus/Payments', 'fa fa-dollar'),
(14, 'EVENTS', '0', 0, 'Menus/Events', 'fa fa-calendar'),
(15, 'MESSAGES', '0', 0, 'Menus/Messages', 'fa fa-comment'),
(16, 'TICKETS', '0', 0, 'Menus/Tickets', 'fa fa-ticket'),
(17, 'NOTES', '0', 0, 'Menus/Notes', 'fa fa-comments-o'),
(18, 'EXPENSES', '0', 0, 'Menus/Expenses', 'fa fa-money'),
(19, 'SETTINGS', 'Settings', 0, 'Menus/Settings', 'fa fa-cog');

-- --------------------------------------------------------

--
-- Table structure for table `ag_message`
--

CREATE TABLE IF NOT EXISTS `ag_message` (
  `message_id` int(11) NOT NULL,
  `message_title` varchar(255) NOT NULL,
  `message_descrription` text NOT NULL,
  `message_attachment` varchar(255) NOT NULL,
  `message_send_to` int(11) NOT NULL,
  `message_send_by` int(11) NOT NULL,
  `message_created_by` int(11) NOT NULL,
  `message_status` enum('S','D') NOT NULL COMMENT 'S->Send,D->Deleted',
  `message_view` enum('Y','N') NOT NULL DEFAULT 'N',
  `message_send_ip` varchar(255) NOT NULL,
  `message_send_device` varchar(255) NOT NULL,
  `message_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_message`
--

INSERT INTO `ag_message` (`message_id`, `message_title`, `message_descrription`, `message_attachment`, `message_send_to`, `message_send_by`, `message_created_by`, `message_status`, `message_view`, `message_send_ip`, `message_send_device`, `message_created_on`) VALUES
(1, 'Greatings', 'Hi admin This is Sumon', 'Koala.jpg', 1, 4, 4, 'S', 'Y', '::1', 'Firefox 57.0/Windows 7', '2018-02-22 16:11:12'),
(2, 'Greatings to sudipa', 'hiii  sudipa', '', 3, 4, 4, 'S', 'N', '::1', 'Firefox 57.0/Windows 7', '2017-11-29 12:06:38'),
(3, 'Test Message', 'Goood morning admin I am Sudipa', '', 1, 3, 3, 'S', 'Y', '::1', 'Firefox 57.0/Windows 7', '2018-02-22 16:05:45'),
(4, 'Reply of Greatings', 'Reply to sumon how r u?', '', 0, 1, 1, 'S', 'N', '::1', 'Firefox 57.0/Windows 7', '2017-11-29 12:19:20'),
(5, '', 'Hi Sumon what r u doing', '', 3, 1, 1, 'S', 'N', '::1', 'Firefox 57.0/Windows 7', '2017-11-29 16:53:20'),
(6, 'Reply ofTest Message', 'Hi Sudipa I am Admin', '', 3, 1, 1, 'S', 'N', '::1', 'Firefox 57.0/Windows 7', '2017-11-29 16:55:21'),
(7, 'Reply ofGreatings', 'Hi sumon good ni8', '', 4, 1, 1, 'S', 'N', '::1', 'Firefox 57.0/Windows 7', '2017-11-29 16:58:45');

-- --------------------------------------------------------

--
-- Table structure for table `ag_notes`
--

CREATE TABLE IF NOT EXISTS `ag_notes` (
  `notes_id` int(11) NOT NULL,
  `notes_title` varchar(255) NOT NULL DEFAULT '0',
  `priority` varchar(50) NOT NULL DEFAULT '0',
  `note_description` varchar(500) DEFAULT '0',
  `created_by` int(11) DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_notes`
--

INSERT INTO `ag_notes` (`notes_id`, `notes_title`, `priority`, `note_description`, `created_by`, `created_date`) VALUES
(1, 'This is First Note', 'Minor', 'This is First Note Description', 1, '2017-12-27 07:21:54'),
(2, 'Test Note', 'Major', ' TeTest Note descriptionTest Note descriptionTest Note ', 1, '2017-12-27 07:22:57');

-- --------------------------------------------------------

--
-- Table structure for table `ag_project_mst`
--

CREATE TABLE IF NOT EXISTS `ag_project_mst` (
  `project_id` int(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `project_lead` varchar(100) NOT NULL,
  `client_id` int(50) NOT NULL,
  `progress` int(20) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `resources_allocatted` varchar(255) NOT NULL,
  `fixed_rate` int(11) NOT NULL,
  `hourly_rate` int(11) NOT NULL,
  `estimated_hours` int(11) NOT NULL,
  `hours_spent` int(11) NOT NULL,
  `Notes` varchar(500) NOT NULL,
  `billed_amt` int(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_project_mst`
--

INSERT INTO `ag_project_mst` (`project_id`, `Name`, `project_lead`, `client_id`, `progress`, `start_date`, `end_date`, `resources_allocatted`, `fixed_rate`, `hourly_rate`, `estimated_hours`, `hours_spent`, `Notes`, `billed_amt`) VALUES
(1, 'Test Sumon', '', 1, 0, '0000-00-00', '2017-11-21', '35', 5, 6, 9, 0, 'test Sumon kkkkk', 0),
(2, 'Test Sumon2', '', 1, 0, '0000-00-00', '2017-11-30', '6', 100, 11, 7, 0, 'Test Sumon2 vvvvvv', 0),
(3, 'Test Sudipa', '', 3, 0, '0000-00-00', '2017-11-30', '9', 400, 15, 10, 0, 'fghjgjvhj', 0),
(4, 'Test Sudipa', '', 3, 0, '0000-00-00', '2017-11-14', '45', 700, 30, 30, 0, 'dfydud', 0),
(5, 'Test Sudipa3', '', 3, 0, '0000-00-00', '2017-11-21', '5', 6, 54, 4, 0, 'fhdh', 0),
(6, 'Test Sumon3', '', 1, 0, '0000-00-00', '2017-11-30', '89', 900, 30, 30, 0, 'dryey', 0),
(7, 'Admin Project', '', 1, 0, '2017-11-01', '2017-11-28', '6', 500, 25, 7, 0, 'erytuy', 0),
(8, 'December Project Test', '', 3, 0, '2017-12-28', '2018-01-30', '2,3,6', 1040, 12, 27, 0, 'Test note december test', 0),
(11, 'Work on january', '', 2, 0, '2018-01-01', '2018-01-25', '1,4,5', 5000, 50, 10, 0, 'test note jan', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ag_role_menu_mapping`
--

CREATE TABLE IF NOT EXISTS `ag_role_menu_mapping` (
  `user_type_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'Admin'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_role_menu_mapping`
--

INSERT INTO `ag_role_menu_mapping` (`user_type_id`, `menu_id`, `created_on`, `created_by`) VALUES
(1, 1, '2015-12-08 02:32:30', 'Admin'),
(1, 2, '2015-12-08 02:33:00', 'Admin'),
(1, 3, '2015-12-08 02:33:15', 'Admin'),
(1, 4, '2015-12-08 02:33:32', 'Admin'),
(1, 5, '2015-12-08 02:33:51', 'Admin'),
(1, 6, '2015-12-08 02:34:02', 'Admin'),
(1, 7, '2015-12-08 02:34:19', 'Admin'),
(1, 8, '2015-12-08 02:34:30', 'Admin'),
(1, 9, '2015-12-08 02:34:39', 'Admin'),
(1, 10, '2015-12-08 02:34:50', 'Admin'),
(1, 11, '2015-12-08 02:35:11', 'Admin'),
(1, 12, '2015-12-08 02:35:27', 'Admin'),
(1, 13, '2015-12-08 02:35:37', 'Admin'),
(1, 15, '2015-12-08 02:36:06', 'Admin'),
(2, 3, '2016-08-08 04:04:34', 'Admin'),
(3, 1, '2016-08-14 10:23:59', 'Admin'),
(3, 3, '2016-08-14 10:24:44', 'Admin'),
(3, 13, '2016-08-14 10:31:17', 'Admin'),
(1, 1, '2015-12-08 02:32:30', 'Admin'),
(1, 2, '2015-12-08 02:33:00', 'Admin'),
(1, 3, '2015-12-08 02:33:15', 'Admin'),
(1, 4, '2015-12-08 02:33:32', 'Admin'),
(1, 5, '2015-12-08 02:33:51', 'Admin'),
(1, 6, '2015-12-08 02:34:02', 'Admin'),
(1, 7, '2015-12-08 02:34:19', 'Admin'),
(1, 8, '2015-12-08 02:34:30', 'Admin'),
(1, 9, '2015-12-08 02:34:39', 'Admin'),
(1, 10, '2015-12-08 02:34:50', 'Admin'),
(1, 11, '2015-12-08 02:35:11', 'Admin'),
(1, 12, '2015-12-08 02:35:27', 'Admin'),
(1, 13, '2015-12-08 02:35:37', 'Admin'),
(1, 15, '2015-12-08 02:36:06', 'Admin'),
(2, 3, '2016-08-08 04:04:34', 'Admin'),
(3, 1, '2016-08-14 10:23:59', 'Admin'),
(3, 3, '2016-08-14 10:24:44', 'Admin'),
(3, 13, '2016-08-14 10:31:17', 'Admin'),
(1, 1, '2015-12-08 02:32:30', 'Admin'),
(1, 2, '2015-12-08 02:33:00', 'Admin'),
(1, 3, '2015-12-08 02:33:15', 'Admin'),
(1, 4, '2015-12-08 02:33:32', 'Admin'),
(1, 5, '2015-12-08 02:33:51', 'Admin'),
(1, 6, '2015-12-08 02:34:02', 'Admin'),
(1, 7, '2015-12-08 02:34:19', 'Admin'),
(1, 8, '2015-12-08 02:34:30', 'Admin'),
(1, 9, '2015-12-08 02:34:39', 'Admin'),
(1, 10, '2015-12-08 02:34:50', 'Admin'),
(1, 11, '2015-12-08 02:35:11', 'Admin'),
(1, 12, '2015-12-08 02:35:27', 'Admin'),
(1, 13, '2015-12-08 02:35:37', 'Admin'),
(1, 15, '2015-12-08 02:36:06', 'Admin'),
(2, 3, '2016-08-08 04:04:34', 'Admin'),
(3, 1, '2016-08-14 10:23:59', 'Admin'),
(3, 3, '2016-08-14 10:24:44', 'Admin'),
(3, 13, '2016-08-14 10:31:17', 'Admin'),
(1, 14, '2017-11-09 03:50:22', 'Admin'),
(1, 16, '2017-11-09 04:47:43', 'Admin'),
(2, 14, '2017-11-17 12:23:40', 'Admin'),
(3, 14, '2017-11-17 12:23:40', 'Admin'),
(2, 15, '2017-11-17 12:24:40', 'Admin'),
(3, 15, '2017-11-17 12:24:40', 'Admin'),
(1, 16, '2017-12-26 09:27:55', 'Admin'),
(1, 17, '2017-12-26 09:27:55', 'Admin'),
(1, 18, '2017-12-26 09:29:22', 'Admin'),
(2, 16, '2017-12-26 09:31:10', 'Admin'),
(2, 17, '2017-12-26 09:31:10', 'Admin'),
(3, 17, '2017-12-26 09:31:56', 'Admin'),
(3, 17, '2017-12-26 09:31:56', 'Admin'),
(1, 19, '2018-02-13 09:55:32', 'Admin'),
(3, 12, '2018-02-17 11:31:20', 'Admin'),
(3, 16, '2018-02-20 16:37:33', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `ag_sprint`
--

CREATE TABLE IF NOT EXISTS `ag_sprint` (
  `sprint_id` int(11) NOT NULL,
  `sprint_title` varchar(500) DEFAULT '0',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_sprint`
--

INSERT INTO `ag_sprint` (`sprint_id`, `sprint_title`, `start_date`, `end_date`, `created_by`, `created_on`) VALUES
(1, 'First Sprint', '2017-11-08', '2017-11-28', 'admin', '2017-11-08 18:23:29'),
(2, 'Design Modification', '2017-11-01', '2017-11-02', 'admin', '2017-11-25 06:23:41'),
(3, 'Pls change heading', '2017-11-29', '2017-11-30', 'admin', '2017-11-25 06:30:09'),
(4, 'Test Sprint Sumon', '2017-12-04', '2017-12-08', 'admin', '2017-12-04 06:10:30'),
(5, 'Test Sprint', '2017-12-17', '2017-12-23', 'admin', '2017-12-21 11:45:57'),
(6, 'Test drag Drop Sprint', '2017-12-24', '2017-12-30', 'admin', '2017-12-22 12:11:32');

-- --------------------------------------------------------

--
-- Table structure for table `ag_status`
--

CREATE TABLE IF NOT EXISTS `ag_status` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_status`
--

INSERT INTO `ag_status` (`status_id`, `status_name`) VALUES
(1, 'Open'),
(2, 'In Progress'),
(3, 'Completed');

-- --------------------------------------------------------

--
-- Table structure for table `ag_tax`
--

CREATE TABLE IF NOT EXISTS `ag_tax` (
  `tax_id` int(11) NOT NULL,
  `tax_name` varchar(255) NOT NULL,
  `tax_percentage` double(10,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_tax`
--

INSERT INTO `ag_tax` (`tax_id`, `tax_name`, `tax_percentage`) VALUES
(1, 'CGST', 5.00),
(2, 'GGST', 6.00),
(3, 'VAT', 8.00),
(4, 'KGST', 12.00);

-- --------------------------------------------------------

--
-- Table structure for table `ag_tickets`
--

CREATE TABLE IF NOT EXISTS `ag_tickets` (
  `ticket_id` int(11) NOT NULL,
  `ticket_type_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `ticket_title` varchar(255) NOT NULL DEFAULT '0',
  `priority` varchar(50) NOT NULL DEFAULT '0',
  `description` varchar(500) DEFAULT '0',
  `estimated_hours` int(11) NOT NULL,
  `reported_by` int(11) DEFAULT '0',
  `assigned_to` int(11) DEFAULT '0',
  `status_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_tickets`
--

INSERT INTO `ag_tickets` (`ticket_id`, `ticket_type_id`, `project_id`, `ticket_title`, `priority`, `description`, `estimated_hours`, `reported_by`, `assigned_to`, `status_id`, `created_by`, `created_date`) VALUES
(2, 4, 0, 'Test Ticket1', 'Critical', 'Test Ticket1Test Ticket1Test Ticket1', 10, 1, 4, 2, 1, '2017-12-28 11:45:00'),
(3, 4, 0, 'Test ticket', 'Critical', 'Test ticket Description', 3, 3, 4, 2, 1, '2017-12-29 05:09:29'),
(4, 4, 0, 'Test tittle 4', 'Critical', 'Test tittle 4Test tittle 4', 7, 2, 5, 3, 1, '2017-12-29 09:39:35'),
(5, 4, 0, 'Test tittle 4', 'Critical', 'Test tittle 4Test tittle 4', 7, 2, 5, 1, 1, '2017-12-29 09:39:35');

-- --------------------------------------------------------

--
-- Table structure for table `ag_timesheet_details`
--

CREATE TABLE IF NOT EXISTS `ag_timesheet_details` (
  `timesheet_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `logged_date` date DEFAULT NULL,
  `logged_hours` int(11) DEFAULT NULL,
  `logged_user` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `issue_id` int(11) DEFAULT NULL,
  `sprint_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_timesheet_details`
--

INSERT INTO `ag_timesheet_details` (`timesheet_id`, `project_id`, `logged_date`, `logged_hours`, `logged_user`, `description`, `issue_id`, `sprint_id`) VALUES
(3, 1, '2017-12-18', 0, 'admin', 'ghkg', 10, 5),
(4, 1, '2017-12-19', 1, 'admin', 'wrwrwr', 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ag_users`
--

CREATE TABLE IF NOT EXISTS `ag_users` (
  `username` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_role` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_type_id` int(11) NOT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `organization` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `filename` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_users`
--

INSERT INTO `ag_users` (`username`, `user_id`, `password`, `user_role`, `created_date`, `user_type_id`, `fullname`, `organization`, `email`, `client_id`, `filename`) VALUES
('admin', 1, 'admin', 'admin', '2016-09-21 21:03:02', 1, 'John Doe', 'AgileTech', 'info@agiletech.com', NULL, 'a4.jpg'),
('client', 2, 'client', 'Client', '2016-09-21 21:03:02', 3, 'RSA', 'Roins Financial', 'info@agiletech.com', 1, 'a1.jpg'),
('sudipa', 3, 'sudipa', 'Client', '2017-11-06 09:05:53', 3, 'Sudipa Sarkar', 'SKR Pvt. Ltd.', 'sudipa@gmail.com', 3, 'a311.jpg'),
('sumon', 4, 'sumon', 'Client', '2017-11-06 09:06:56', 3, 'Sumon Roy', 'SR Indusrties pvt. Ltd.', 'sumon@gmail.com', 1, 'a1.jpg'),
('user', 5, 'user', 'User', '2016-09-21 21:03:02', 2, 'Elliot', 'Agile Tech', 'info@agiletech.com', 0, 'm1.jpg'),
('user1', 6, 'user', 'User', '2016-09-21 21:03:02', 2, 'Carm Legg', 'lbsk', 'info@agiletech.com', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `ag_user_type_mst`
--

CREATE TABLE IF NOT EXISTS `ag_user_type_mst` (
  `user_type_id` int(11) NOT NULL,
  `user_type_desc` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ag_user_type_mst`
--

INSERT INTO `ag_user_type_mst` (`user_type_id`, `user_type_desc`) VALUES
(1, 'admin'),
(2, 'users'),
(3, 'client');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ag_client_details`
--
ALTER TABLE `ag_client_details`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `ag_color`
--
ALTER TABLE `ag_color`
  ADD PRIMARY KEY (`color_id`);

--
-- Indexes for table `ag_event`
--
ALTER TABLE `ag_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `ag_expenses`
--
ALTER TABLE `ag_expenses`
  ADD PRIMARY KEY (`exp_id`), ADD KEY `FK__ag_project_mst` (`project_id`);

--
-- Indexes for table `ag_invoice_list_details`
--
ALTER TABLE `ag_invoice_list_details`
  ADD PRIMARY KEY (`list_id`), ADD KEY `FK__ag_invoice_txn` (`invoice_id`);

--
-- Indexes for table `ag_invoice_txn`
--
ALTER TABLE `ag_invoice_txn`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `ag_issues_mst`
--
ALTER TABLE `ag_issues_mst`
  ADD PRIMARY KEY (`issue_type_id`);

--
-- Indexes for table `ag_issues_txn`
--
ALTER TABLE `ag_issues_txn`
  ADD PRIMARY KEY (`issue_id`), ADD KEY `FK__ag_issues_mst` (`issue_type_id`), ADD KEY `FK__ag_project_mst` (`project_id`), ADD KEY `FK__ag_users` (`reported_by`), ADD KEY `FK__ag_users_2` (`assigned_to`);

--
-- Indexes for table `ag_menu_mst`
--
ALTER TABLE `ag_menu_mst`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `ag_message`
--
ALTER TABLE `ag_message`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `ag_notes`
--
ALTER TABLE `ag_notes`
  ADD PRIMARY KEY (`notes_id`);

--
-- Indexes for table `ag_project_mst`
--
ALTER TABLE `ag_project_mst`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `ag_role_menu_mapping`
--
ALTER TABLE `ag_role_menu_mapping`
  ADD KEY `FK__ag_user_type_mst` (`user_type_id`), ADD KEY `FK__ag_menu_mst` (`menu_id`);

--
-- Indexes for table `ag_sprint`
--
ALTER TABLE `ag_sprint`
  ADD PRIMARY KEY (`sprint_id`);

--
-- Indexes for table `ag_status`
--
ALTER TABLE `ag_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `ag_tax`
--
ALTER TABLE `ag_tax`
  ADD PRIMARY KEY (`tax_id`);

--
-- Indexes for table `ag_tickets`
--
ALTER TABLE `ag_tickets`
  ADD PRIMARY KEY (`ticket_id`), ADD KEY `FK__ag_issues_mst` (`ticket_type_id`), ADD KEY `FK__ag_project_mst` (`project_id`), ADD KEY `FK__ag_users` (`reported_by`), ADD KEY `FK__ag_users_2` (`assigned_to`);

--
-- Indexes for table `ag_timesheet_details`
--
ALTER TABLE `ag_timesheet_details`
  ADD PRIMARY KEY (`timesheet_id`), ADD KEY `FK_ag_timesheet_details_ag_project_mst` (`project_id`), ADD KEY `FK_ag_timesheet_details_ag_users` (`logged_user`), ADD KEY `FK_ag_timesheet_details_ag_issues_txn` (`issue_id`);

--
-- Indexes for table `ag_users`
--
ALTER TABLE `ag_users`
  ADD PRIMARY KEY (`username`), ADD KEY `FK_ag_users_ag_user_type_mst` (`user_type_id`);

--
-- Indexes for table `ag_user_type_mst`
--
ALTER TABLE `ag_user_type_mst`
  ADD PRIMARY KEY (`user_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ag_client_details`
--
ALTER TABLE `ag_client_details`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ag_color`
--
ALTER TABLE `ag_color`
  MODIFY `color_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ag_event`
--
ALTER TABLE `ag_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `ag_expenses`
--
ALTER TABLE `ag_expenses`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ag_invoice_list_details`
--
ALTER TABLE `ag_invoice_list_details`
  MODIFY `list_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ag_invoice_txn`
--
ALTER TABLE `ag_invoice_txn`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ag_issues_mst`
--
ALTER TABLE `ag_issues_mst`
  MODIFY `issue_type_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ag_issues_txn`
--
ALTER TABLE `ag_issues_txn`
  MODIFY `issue_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `ag_menu_mst`
--
ALTER TABLE `ag_menu_mst`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `ag_message`
--
ALTER TABLE `ag_message`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ag_notes`
--
ALTER TABLE `ag_notes`
  MODIFY `notes_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ag_project_mst`
--
ALTER TABLE `ag_project_mst`
  MODIFY `project_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `ag_sprint`
--
ALTER TABLE `ag_sprint`
  MODIFY `sprint_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ag_status`
--
ALTER TABLE `ag_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ag_tax`
--
ALTER TABLE `ag_tax`
  MODIFY `tax_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ag_tickets`
--
ALTER TABLE `ag_tickets`
  MODIFY `ticket_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ag_timesheet_details`
--
ALTER TABLE `ag_timesheet_details`
  MODIFY `timesheet_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ag_user_type_mst`
--
ALTER TABLE `ag_user_type_mst`
  MODIFY `user_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ag_invoice_list_details`
--
ALTER TABLE `ag_invoice_list_details`
ADD CONSTRAINT `FK__ag_invoice_txn` FOREIGN KEY (`invoice_id`) REFERENCES `ag_invoice_txn` (`invoice_id`);

--
-- Constraints for table `ag_issues_txn`
--
ALTER TABLE `ag_issues_txn`
ADD CONSTRAINT `FK__ag_issues_mst` FOREIGN KEY (`issue_type_id`) REFERENCES `ag_issues_mst` (`issue_type_id`),
ADD CONSTRAINT `FK__ag_project_mst` FOREIGN KEY (`project_id`) REFERENCES `ag_project_mst` (`project_id`),
ADD CONSTRAINT `FK__ag_users` FOREIGN KEY (`reported_by`) REFERENCES `ag_users` (`username`),
ADD CONSTRAINT `FK__ag_users_2` FOREIGN KEY (`assigned_to`) REFERENCES `ag_users` (`username`);

--
-- Constraints for table `ag_timesheet_details`
--
ALTER TABLE `ag_timesheet_details`
ADD CONSTRAINT `FK_ag_timesheet_details_ag_issues_txn` FOREIGN KEY (`issue_id`) REFERENCES `ag_issues_txn` (`issue_id`),
ADD CONSTRAINT `FK_ag_timesheet_details_ag_project_mst` FOREIGN KEY (`project_id`) REFERENCES `ag_project_mst` (`project_id`),
ADD CONSTRAINT `FK_ag_timesheet_details_ag_users` FOREIGN KEY (`logged_user`) REFERENCES `ag_users` (`username`);

--
-- Constraints for table `ag_users`
--
ALTER TABLE `ag_users`
ADD CONSTRAINT `FK_ag_users_ag_user_type_mst` FOREIGN KEY (`user_type_id`) REFERENCES `ag_user_type_mst` (`user_type_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
