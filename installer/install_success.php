<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Install | Agile</title>
		<style type="text/css">
		  body {
		    font-size: 100%;
		    font-family: Helvetica,Arial,sans-serif;
		    width: 50%;
		    margin: 0 auto;
		    background: #ffffff;
		  }
		  input, label {
		    display: block;
		    font-size: 18px;
		    margin: 0;
		    padding: 5px;
		    border-radius:10px;
		  }
		  label {
		    margin-top: 1%;
		    color:#ffffff;
		  }
		  input.input_text {
		    width: 95%;
		  }
		  input#submit {
		    margin: 10px auto 0;
		    font-size: 25px;
		  }
		  fieldset {
		    padding: 15px;
		    border-radius:10px;
		    background: #006400;
		    color:#ffffff;
		  }
		  legend {
		    font-size: 18px;
		    font-weight: bold;
		  }
		  p{
		  	font-size: 14px;
		    font-weight: 800;
		    color:#2C659F;
		  }
		  .error {
		    background: #ffd1d1;
		    border: 1px solid #ff5858;
        	padding: 4px;
		  }
		</style>
		<style>
		#myProgress {
		  width: 100%;
		  background-color: #ddd;
		}

		#myBar {
		  width: 1%;
		  height: 30px;
		  background-color: #4CAF50;
		}
		</style>
	</head>
	<body>
    <center>
    <h1  style="background-color: #006400; color: #ffffff">Install Wizard</h1>
    </center>
    <div>
    <fieldset>
   <div ><img src="../assets/img/green-tick-checkmark-icon.jpg" height="50" width="50"></img> 
   <div><h2> Installation successfully completed.</h2></div> 
   
   <div><h3><a style="color: #ffffff" href="">Click here</a> to get going.</h3></div>
   </div>
   </fieldset>
	</body>
</html>