<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
  <script>
    $(function() {
      $(".meter > span").each(function() {
        $(this)
          .data("origWidth", $(this).width())
          .width(0)
          .animate({
            width: $(this).data("origWidth")
             //alert('Software Install Successfully')
          }, 200);
      });
    });
  </script>
  <style>
    .meter { 
      height: 20px;  /* Can be anything */
      position: relative;
      margin: 60px 0 20px 0; /* Just for demo spacing */
      background: #555;
      -moz-border-radius: 25px;
      -webkit-border-radius: 25px;
      border-radius: 25px;
      padding: 10px;
      -webkit-box-shadow: inset 0 -1px 1px rgba(255,255,255,0.3);
      -moz-box-shadow   : inset 0 -1px 1px rgba(255,255,255,0.3);
      box-shadow        : inset 0 -1px 1px rgba(255,255,255,0.3);
    }
    .meter > span {
      display: block;
      height: 100%;
         -webkit-border-top-right-radius: 8px;
      -webkit-border-bottom-right-radius: 8px;
             -moz-border-radius-topright: 8px;
          -moz-border-radius-bottomright: 8px;
                 border-top-right-radius: 8px;
              border-bottom-right-radius: 8px;
          -webkit-border-top-left-radius: 20px;
       -webkit-border-bottom-left-radius: 20px;
              -moz-border-radius-topleft: 20px;
           -moz-border-radius-bottomleft: 20px;
                  border-top-left-radius: 20px;
               border-bottom-left-radius: 20px;
      background-color: rgb(43,194,83);
      background-image: -webkit-gradient(
        linear,
        left bottom,
        left top,
        color-stop(0, rgb(43,194,83)),
        color-stop(1, rgb(84,240,84))
       );
      background-image: -moz-linear-gradient(
        center bottom,
        rgb(43,194,83) 37%,
        rgb(84,240,84) 69%
       );
      -webkit-box-shadow: 
        inset 0 2px 9px  rgba(255,255,255,0.3),
        inset 0 -2px 6px rgba(0,0,0,0.4);
      -moz-box-shadow: 
        inset 0 2px 9px  rgba(255,255,255,0.3),
        inset 0 -2px 6px rgba(0,0,0,0.4);
      box-shadow: 
        inset 0 2px 9px  rgba(255,255,255,0.3),
        inset 0 -2px 6px rgba(0,0,0,0.4);
      position: relative;
      overflow: hidden;
    }
    .meter > span:after, .animate > span > span {
      content: "";
      position: absolute;
      top: 0; left: 0; bottom: 0; right: 0;
      background-image: 
         -webkit-gradient(linear, 0 0, 100% 100%, 
            color-stop(.25, rgba(255, 255, 255, .2)), 
            color-stop(.25, transparent), color-stop(.5, transparent), 
            color-stop(.5, rgba(255, 255, 255, .2)), 
            color-stop(.75, rgba(255, 255, 255, .2)), 
            color-stop(.75, transparent), to(transparent)
         );
      background-image: 
        -moz-linear-gradient(
          -45deg, 
            rgba(255, 255, 255, .2) 25%, 
            transparent 25%, 
            transparent 50%, 
            rgba(255, 255, 255, .2) 50%, 
            rgba(255, 255, 255, .2) 75%, 
            transparent 75%, 
            transparent
         );
      z-index: 1;
      -webkit-background-size: 50px 50px;
      -moz-background-size: 50px 50px;
      -webkit-animation: move 2s linear infinite;
         -webkit-border-top-right-radius: 8px;
      -webkit-border-bottom-right-radius: 8px;
             -moz-border-radius-topright: 8px;
          -moz-border-radius-bottomright: 8px;
                 border-top-right-radius: 8px;
              border-bottom-right-radius: 8px;
          -webkit-border-top-left-radius: 20px;
       -webkit-border-bottom-left-radius: 20px;
              -moz-border-radius-topleft: 20px;
           -moz-border-radius-bottomleft: 20px;
                  border-top-left-radius: 20px;
               border-bottom-left-radius: 20px;
      overflow: hidden;
    }    
    .animate > span:after {
      display: none;
    }    
    @-webkit-keyframes move {
        0% {
           background-position: 0 0;
        }
        100% {
           background-position: 50px 50px;
        }
    }    
    .orange > span {
      background-color: #f1a165;
      background-image: -moz-linear-gradient(top, #f1a165, #f36d0a);
      background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #f1a165),color-stop(1, #f36d0a));
      background-image: -webkit-linear-gradient(#f1a165, #f36d0a); 
    }    
    .red > span {
      background-color: #f0a3a3;
      background-image: -moz-linear-gradient(top, #f0a3a3, #f42323);
      background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #f0a3a3),color-stop(1, #f42323));
      background-image: -webkit-linear-gradient(#f0a3a3, #f42323);
    }    
    .nostripes > span > span, .nostripes > span:after {
      -webkit-animation: none;
      background-image: none;
    }
</style>
<?php
//error_reporting(0); //Setting this to E_ALL showed that that cause of not redirecting were few blank lines added in some php files.
$db_config_path = '../application/config/database.php';

// Only load the classes in case the user submitted the form
if($_POST) {

	// Load the classes and create the new objects
	require_once('includes/core_class.php');
	require_once('includes/database_class.php');

	$core = new Core();
	$database = new Database();


	// Validate the post data
	if($core->validate_post($_POST) == true)
	{

		if($_POST['database'] != "agile")
		{
			$message = $core->show_message('error',"The database name is wrong");
		}
		// First create the database, then create tables, then write config file
		else if($database->create_database($_POST) == false) {
			$message = $core->show_message('error',"The database could not be created, please verify your hostname,username and password");
		} else if ($database->create_tables($_POST) == false) {
			$message = $core->show_message('error',"The database tables could not be created, please verify your settings.");
		} else if ($core->write_config($_POST) == false) {
			$message = $core->show_message('error',"The database configuration file could not be written, please chmod application/config/database.php file to 777");
		}

		// If no errors, redirect to registration page
		if(!isset($message)) {					
		  $redir = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
      	  $redir .= "://".$_SERVER['HTTP_HOST'];
          $redir .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
          $redir = str_replace('install/','',$redir); 
         ?>

        <h1 style="background-color: #006400;color: #ffffff">Installing Software... Please wait....</h1>
		<div class="meter animate">
		  <span style="width: 100%"><span></span></span>
		</div>		
		<script>
			//alert('Software Install Successfully'); 			
			window.location.href="install_success.php";
		</script>
		<?php		
		//header( 'Location: ' . $redir . '../login' );			
		}
	}
	else {
		$message = $core->show_message('error','Not all fields have been filled in correctly. The host, username, password and database name are required.');
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Install | Agile</title>
		<style type="text/css">
		  body {
		    font-size: 100%;
		    font-family: Helvetica,Arial,sans-serif;
		    width: 50%;
		    margin: 0 auto;
		    background: #ffffff;
		  }
		  input, label {
		    display: block;
		    font-size: 18px;
		    margin: 0;
		    padding: 5px;
		    border-radius:10px;
		  }
		  label {
		    margin-top: 1%;
		    color:#ffffff;
		  }
		  input.input_text {
		    width: 95%;
		  }
		  input#submit {
		    margin: 10px auto 0;
		    font-size: 25px;
		  }
		  fieldset {
		    padding: 15px;
		    border-radius:10px;
		    background: #006400;
		    color:#ffffff;
		  }
		  legend {
		    font-size: 18px;
		    font-weight: bold;
		    color: #000;
		  }
		  p{
		  	font-size: 14px;
		    font-weight: 800;
		    color:#2C659F;
		  }
		  .error {
		    background: #ffd1d1;
		    border: 1px solid #ff5858;
        	padding: 4px;
		  }
		</style>
		<style>
		#myProgress {
		  width: 100%;
		  background-color: #ddd;
		}

		#myBar {
		  width: 1%;
		  height: 30px;
		  background-color: #4CAF50;
		}
		</style>
	</head>
	<body>
    <center>
    <h1  style="background-color: #006400; color: #ffffff;">Install Wizard</h1>
    </center>
    <p>Installation Instructions</p>
    <ul>
    <li>Host Name:- localhost</li>
    <li>User Name:- root (if your mysql username is other than root please put that name here)</li>
    <li>Password:-  (Generaly mysql Password is blank if your mysql have password put it here)</li>
    <li>Database Name:- (Give Database Name agile)</li>
</ul>
    
    <?php if(is_writable($db_config_path)){?>

		  <?php if(isset($message)) {echo '<p class="error">' . $message . '</p>';}?>

		<form id="install_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
	        <fieldset>
	          <legend>Database settings</legend>
	          <label for="hostname">Hostname <span style="color:#FF0000;">*</span></label>
	          <input type="text" id="hostname" value="localhost" class="input_text" name="hostname" required="required" />
	          <label for="username">Username <span style="color:#FF0000;">*</span></label>
	          <input type="text" id="username" class="input_text" name="username" required="required"/>
	          <label for="password">Password</label>
	          <input type="password" id="password" class="input_text" name="password"/>
	          <label for="database">Database Name <span style="color:#FF0000;">*</span></label>
	          <input type="text" id="database" class="input_text" name="database" required="required"/>
	          <input type="submit" value="Install" id="submit"/>
	        </fieldset>
		</form>
	  <?php } else { ?>
      <p class="error">Please make the application/config/database.php file writable. <strong>Example</strong>:<br /><br /><code>chmod 777 application/config/database.php</code></p>
	  <?php } ?>
	</body>
</html>