-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.25-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for scrum
CREATE DATABASE IF NOT EXISTS `scrum` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `scrum`;

-- Dumping structure for table scrum.ag_client_details
CREATE TABLE IF NOT EXISTS `ag_client_details` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_name` varchar(100) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `phone_no` bigint(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `skype` varchar(100) NOT NULL,
  `c_logo` varchar(255) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_client_details: ~6 rows (approximately)
/*!40000 ALTER TABLE `ag_client_details` DISABLE KEYS */;
INSERT INTO `ag_client_details` (`client_id`, `organization_name`, `contact_person`, `phone_no`, `email`, `address`, `skype`, `c_logo`, `created_on`) VALUES
	(1, 'SR Indusrties pvt. Ltd.', 'Sumon Roy', 8837749123, 'sumon@gmail.com', '', 'sumon123', '1517503899lighthouse.jpg', '2017-11-06 14:24:41'),
	(2, 'SKR Pvt. Ltd.', 'Sudipa Sarkar', 5678901234, 'sudipa@gmail.com', '9/6/7 G.T. Road', 'sudipa789', '1517504050zender_logo.png', '2017-11-06 14:26:20'),
	(3, 'SC Infotech', 'Suvo Chatterjee', 1904236787, 'sc@gmail.com', 'setstrt', 'sc345', '1517504071angular_logo.png', '2017-11-06 16:34:03'),
	(4, 'Test ssss', 'Test fdsgsg', 576585685, 'ilmiya1123@gmail.com', 'tryryrty', 'utu788', '', '2017-11-06 16:42:58'),
	(5, 'Test Organization Name', 'kk', 5765960, 'ss@gmail.com', '', 'yyry', '', '2017-12-13 16:14:30'),
	(7, 'Sudipta Solution', 'Sujit', 9748837749, 'sudipta.suvo@gmail.com', 'Rishra', 'sudipta.chakraborty@gmail.com', '1517502538hydrangeas.jpg', '2018-02-01 21:58:58');
/*!40000 ALTER TABLE `ag_client_details` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_color
CREATE TABLE IF NOT EXISTS `ag_color` (
  `color_id` int(11) NOT NULL AUTO_INCREMENT,
  `color_name` varchar(255) NOT NULL,
  PRIMARY KEY (`color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_color: ~7 rows (approximately)
/*!40000 ALTER TABLE `ag_color` DISABLE KEYS */;
INSERT INTO `ag_color` (`color_id`, `color_name`) VALUES
	(1, 'Red'),
	(2, 'Green'),
	(3, 'Blue'),
	(4, 'Yellow'),
	(5, 'Pink'),
	(6, 'Black'),
	(7, 'Orange');
/*!40000 ALTER TABLE `ag_color` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_company_mst
CREATE TABLE IF NOT EXISTS `ag_company_mst` (
  `company_id` int(10) unsigned DEFAULT NULL,
  `company_name` varchar(200) DEFAULT NULL,
  `company_address` varchar(1000) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `registration` varchar(50) DEFAULT NULL,
  `vat` varchar(50) DEFAULT NULL,
  `vat_percent` int(11) DEFAULT NULL,
  `logo` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_company_mst: ~0 rows (approximately)
/*!40000 ALTER TABLE `ag_company_mst` DISABLE KEYS */;
INSERT INTO `ag_company_mst` (`company_id`, `company_name`, `company_address`, `zipcode`, `city`, `state`, `country`, `email`, `phone`, `registration`, `vat`, `vat_percent`, `logo`) VALUES
	(1, 'Agile Technology', '12/ K P Roy', '712250', NULL, NULL, NULL, 'sudipta.chakraborty@gmail.com', '123456', '45457', '6', 4, 'jboss-as-standalone1.sh');
/*!40000 ALTER TABLE `ag_company_mst` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_dod
CREATE TABLE IF NOT EXISTS `ag_dod` (
  `dod_id` int(11) NOT NULL AUTO_INCREMENT,
  `definition` varchar(5000) NOT NULL DEFAULT '0',
  `description` varchar(5000) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `created_by` varchar(50) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `Index 1` (`dod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_dod: ~7 rows (approximately)
/*!40000 ALTER TABLE `ag_dod` DISABLE KEYS */;
INSERT INTO `ag_dod` (`dod_id`, `definition`, `description`, `position`, `created_by`, `created_date`) VALUES
	(1, 'test', ' test sdfsdfsdf', 5, '1', '2019-05-02 01:03:21'),
	(2, '0', 'dfgdfgfdgfdg', 6, '1', '2019-05-02 01:03:21'),
	(10, '0', ' dfgdfgdfgfdgdfgdfgfdgdf \ngfhgfhgfh\nfghgfhfghgfhfgh', 0, '1', '2019-05-04 16:54:34'),
	(12, '0', 'dfgdfgdfg', 0, '1', '2019-05-04 16:47:15'),
	(13, '0', 'tyujtyjhg', 0, '1', '2019-05-04 16:48:04'),
	(14, '0', 'cxvxcvxcv', 0, '1', '2019-05-04 16:50:37');
/*!40000 ALTER TABLE `ag_dod` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_dor
CREATE TABLE IF NOT EXISTS `ag_dor` (
  `dor_id` int(11) NOT NULL AUTO_INCREMENT,
  `definition` varchar(5000) NOT NULL DEFAULT '0',
  `description` varchar(5000) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `created_by` varchar(50) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `Index 1` (`dor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_dor: ~2 rows (approximately)
/*!40000 ALTER TABLE `ag_dor` DISABLE KEYS */;
INSERT INTO `ag_dor` (`dor_id`, `definition`, `description`, `position`, `created_by`, `created_date`) VALUES
	(1, 'test', '    test  edit fdgdfgdfg  ', 5, '1', '2019-06-22 12:59:12'),
	(2, 'Meets acceptance criteria', 'Meets acceptance criteria', 10, '1', '2019-05-08 08:39:45'),
	(68, '0', 'xvxcvxcvxcvcxvcxvcxvcx', 4, '1', '2019-06-22 12:59:12'),
	(69, '0', 'ghjhgjhgjgh', 7, '1', '2019-06-22 12:59:12');
/*!40000 ALTER TABLE `ag_dor` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_event
CREATE TABLE IF NOT EXISTS `ag_event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_title` varchar(255) NOT NULL,
  `event_startdate` datetime NOT NULL,
  `event_enddate` datetime NOT NULL,
  `event_description` varchar(255) NOT NULL,
  `event_color` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_event: ~19 rows (approximately)
/*!40000 ALTER TABLE `ag_event` DISABLE KEYS */;
INSERT INTO `ag_event` (`event_id`, `event_title`, `event_startdate`, `event_enddate`, `event_description`, `event_color`, `created_by`, `created_on`) VALUES
	(1, 'First Event', '2017-11-10 00:00:00', '0000-00-00 00:00:00', '', 'Red', 1, '2017-11-30 04:39:05'),
	(2, 'Second Event', '2017-12-11 00:00:00', '0000-00-00 00:00:00', '', 'Green', 1, '2017-11-30 04:39:10'),
	(3, 'Event Three', '2017-12-14 00:00:00', '0000-00-00 00:00:00', '', 'Blue', 1, '2017-11-30 04:39:17'),
	(4, '4th event', '2018-01-03 00:00:00', '0000-00-00 00:00:00', '', 'Yellow', 1, '2017-11-30 11:42:44'),
	(5, 'BDay party', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'Green', 1, '2017-11-30 11:42:51'),
	(6, 'Meeting with CEO', '2017-11-29 00:00:00', '2017-11-30 00:00:00', 'Meeting with CEO for work', 'Yellow', 1, '2017-11-30 04:39:23'),
	(7, 'Meeting with MIS', '2017-11-15 00:00:00', '2017-11-15 00:00:00', 'Meeting with MIS work', 'Red', 1, '2017-11-30 11:43:19'),
	(8, 'Meeting with Admin', '2017-11-07 00:00:00', '2017-11-08 00:00:00', 'Meeting with Admin', 'Orange', 1, '2017-11-30 04:39:31'),
	(9, 'Meeting with HR', '2017-12-12 00:00:00', '2017-12-14 00:00:00', 'Meeting with HR', 'Black', 1, '2017-11-30 04:39:36'),
	(10, 'picnic', '2017-11-29 00:00:00', '2017-11-30 00:00:00', 'picnic', 'Pink', 1, '2017-11-30 11:43:05'),
	(11, 'paper work', '2017-11-29 00:00:00', '2017-11-30 00:00:00', 'paper work', 'Green', 1, '2017-11-30 11:42:56'),
	(12, 'new joining', '2018-01-02 00:00:00', '2018-01-02 00:00:00', 'new joining', 'Orange', 1, '2017-11-30 11:43:14'),
	(13, 'Journey Meeting', '2017-11-17 00:00:00', '2017-11-18 00:00:00', 'Description Journey Meeting', 'Pink', 1, '2017-11-30 11:43:01'),
	(14, 'Journey Meeting', '2017-11-17 00:00:00', '2017-11-18 00:00:00', 'Description Journey Meeting', 'Green', 1, '2017-11-30 11:42:53'),
	(15, 'Doctor Checkup', '2017-11-30 00:00:00', '2017-11-30 00:00:00', 'Doctor Checkup', 'Pink', 1, '2017-11-30 11:43:00'),
	(16, 'Doctor Checkup', '2017-11-24 00:00:00', '2017-11-24 00:00:00', 'Doctor Checkup for health', 'Pink', 1, '2017-11-30 04:45:19'),
	(17, 'Journey Meeting', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Description Journey Meeting', 'Yellow', 1, '2017-11-30 05:02:56'),
	(18, 'Office cleaning', '2017-11-27 00:00:00', '2017-11-29 00:00:00', 'Office cleaning', 'Green', 1, '2017-11-30 11:39:51'),
	(19, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', 1, '2017-12-27 12:46:03'),
	(20, 'test', '2018-09-18 00:00:00', '2018-09-19 00:00:00', 'test', 'Green', 1, '2018-09-22 21:27:15'),
	(21, 'test event', '2018-09-22 05:30:00', '2018-09-25 05:30:00', 'test event', 'Red', 1, '2019-01-06 21:17:16'),
	(22, 'test event', '2019-01-03 05:30:00', '2019-01-07 05:30:00', 'Test event', 'Orange', 1, '2019-01-06 22:38:22');
/*!40000 ALTER TABLE `ag_event` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_expenses
CREATE TABLE IF NOT EXISTS `ag_expenses` (
  `exp_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `exp_purpose` varchar(255) NOT NULL DEFAULT '0',
  `exp_amount` double(12,2) NOT NULL DEFAULT '0.00',
  `exp_description` varchar(255) DEFAULT '0',
  `exp_date` date NOT NULL,
  `created_by` int(11) DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`exp_id`),
  KEY `FK__ag_project_mst` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_expenses: ~3 rows (approximately)
/*!40000 ALTER TABLE `ag_expenses` DISABLE KEYS */;
INSERT INTO `ag_expenses` (`exp_id`, `project_id`, `exp_purpose`, `exp_amount`, `exp_description`, `exp_date`, `created_by`, `created_date`) VALUES
	(1, 0, 'Bill Payment', 1001.00, 'Test payment', '2018-01-04', 1, '2018-02-14 12:18:38'),
	(2, 0, 'New Bill Payment', 2000.50, 'New Bill Payment New Bill Payment', '2018-02-01', 1, '2018-02-14 12:27:14'),
	(3, 0, 'New Bill Payment2', 2500.50, 'New Bill Payment2 New Bill Payment2', '2018-02-05', 1, '2018-02-14 12:28:23');
/*!40000 ALTER TABLE `ag_expenses` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_invoice_list_details
CREATE TABLE IF NOT EXISTS `ag_invoice_list_details` (
  `list_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) DEFAULT '0',
  `items` varchar(500) DEFAULT '0',
  `Quantity` int(11) DEFAULT '0',
  `unit_price` float DEFAULT '0',
  `tax_id` int(11) NOT NULL,
  `tax` float DEFAULT '0',
  `row_total` float DEFAULT '0',
  PRIMARY KEY (`list_id`),
  KEY `FK__ag_invoice_txn` (`invoice_id`),
  CONSTRAINT `FK__ag_invoice_txn` FOREIGN KEY (`invoice_id`) REFERENCES `ag_invoice_txn` (`invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_invoice_list_details: ~8 rows (approximately)
/*!40000 ALTER TABLE `ag_invoice_list_details` DISABLE KEYS */;
INSERT INTO `ag_invoice_list_details` (`list_id`, `invoice_id`, `items`, `Quantity`, `unit_price`, `tax_id`, `tax`, `row_total`) VALUES
	(1, 1, 'item1', 2, 5.6, 1, 5, 11.872),
	(2, 1, 'item2', 5, 4, 2, 6, 21.2),
	(3, 2, 'Soap', 2, 76.9057, 1, 5, 163.04),
	(4, 2, 'Cement', 10, 700, 2, 6, 7420),
	(5, 1, 'ee', 43, 3, 3, 8, 136.74),
	(6, 2, 'dfyghdf', 4, 5, 3, 8, 20.8),
	(7, 1, 'Test Tax', 7, 5, 4, 12, 39.2),
	(8, 1, 'test', 5, 10, 4, 12, 56);
/*!40000 ALTER TABLE `ag_invoice_list_details` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_invoice_txn
CREATE TABLE IF NOT EXISTS `ag_invoice_txn` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `pay_status` varchar(10) NOT NULL DEFAULT 'Unpaid',
  `due_date` date NOT NULL,
  `total_amt` int(11) NOT NULL,
  `paid_amt` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `default_tax` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `notes` varchar(500) NOT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_invoice_txn: ~3 rows (approximately)
/*!40000 ALTER TABLE `ag_invoice_txn` DISABLE KEYS */;
INSERT INTO `ag_invoice_txn` (`invoice_id`, `client_id`, `pay_status`, `due_date`, `total_amt`, `paid_amt`, `created_on`, `default_tax`, `discount`, `currency`, `notes`) VALUES
	(1, 1, 'Paid', '0000-00-00', 265, 265, '2018-02-27 21:14:20', 5, 1, 'USD', 'Test'),
	(2, 2, 'Paid', '2017-12-26', 7604, 300, '2018-02-27 21:20:59', 5, 1, 'USD', 'qwrqewr'),
	(3, 0, 'Unpaid', '0000-00-00', 0, 0, '2017-11-09 22:18:18', 0, 0, '', ''),
	(4, 1, 'Unpaid', '2018-12-12', 0, 0, '2018-12-30 00:33:03', 0, 0, 'EURO', '');
/*!40000 ALTER TABLE `ag_invoice_txn` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_issues_mst
CREATE TABLE IF NOT EXISTS `ag_issues_mst` (
  `issue_type_id` int(100) NOT NULL AUTO_INCREMENT,
  `issue_type` varchar(100) NOT NULL,
  PRIMARY KEY (`issue_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_issues_mst: ~2 rows (approximately)
/*!40000 ALTER TABLE `ag_issues_mst` DISABLE KEYS */;
INSERT INTO `ag_issues_mst` (`issue_type_id`, `issue_type`) VALUES
	(1, 'Bug'),
	(2, 'Story');
/*!40000 ALTER TABLE `ag_issues_mst` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_issues_txn
CREATE TABLE IF NOT EXISTS `ag_issues_txn` (
  `issue_id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_type_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `summary` varchar(500) NOT NULL DEFAULT '0',
  `priority` varchar(50) NOT NULL DEFAULT '0',
  `description` varchar(500) DEFAULT '0',
  `attachment` varchar(500) DEFAULT '0',
  `filename` varchar(500) DEFAULT '0',
  `status` varchar(500) DEFAULT '0',
  `reported_by` varchar(500) DEFAULT '0',
  `assigned_to` varchar(500) DEFAULT '0',
  `epic_id` varchar(500) DEFAULT '0',
  `sprint_id` int(11) DEFAULT '0',
  `original_estimate` int(11) DEFAULT '0',
  `logged_estimate` int(11) DEFAULT '0',
  `remaining_estimate` int(11) DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`issue_id`),
  KEY `FK__ag_issues_mst` (`issue_type_id`),
  KEY `FK__ag_project_mst` (`project_id`),
  KEY `FK__ag_users` (`reported_by`),
  KEY `FK__ag_users_2` (`assigned_to`),
  CONSTRAINT `FK__ag_issues_mst` FOREIGN KEY (`issue_type_id`) REFERENCES `ag_issues_mst` (`issue_type_id`),
  CONSTRAINT `FK__ag_project_mst` FOREIGN KEY (`project_id`) REFERENCES `ag_project_mst` (`project_id`),
  CONSTRAINT `FK__ag_users` FOREIGN KEY (`reported_by`) REFERENCES `ag_users` (`username`),
  CONSTRAINT `FK__ag_users_2` FOREIGN KEY (`assigned_to`) REFERENCES `ag_users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_issues_txn: ~15 rows (approximately)
/*!40000 ALTER TABLE `ag_issues_txn` DISABLE KEYS */;
INSERT INTO `ag_issues_txn` (`issue_id`, `issue_type_id`, `project_id`, `summary`, `priority`, `description`, `attachment`, `filename`, `status`, `reported_by`, `assigned_to`, `epic_id`, `sprint_id`, `original_estimate`, `logged_estimate`, `remaining_estimate`, `created_date`) VALUES
	(1, 2, 1, 'Design Modification', 'Minor', 'Design Modification description', './uploads/', '', 'Closed', 'admin', 'user', '0', 6, 10, 0, 0, '2019-06-30 17:44:03'),
	(2, 2, 1, 'New Story', 'Major', 'New Story test', './uploads/', '', 'Not Started', 'admin', 'sudipa', '0', 1, 5, 0, 0, '2019-06-30 16:57:37'),
	(3, 1, 3, 'Test Bug', 'Minor', 'fjfgjfjfgjfjfgjfjfgjfjfgj\r\nfjfgjfjfgj\r\nfjfgjfjfgjfjfgj\r\nfjfgjfjfgj', './uploads/', '', 'Progress', 'admin', 'admin', '0', 0, 6, 0, 0, '2019-06-29 19:52:10'),
	(4, 1, 1, 'Heading Change', 'Trivial', 'Heading Change', './uploads/', '', 'Progress', 'admin', 'sumon', '0', 7, 2, 0, 0, '2017-11-25 11:59:20'),
	(5, 1, 1, 'Add New Section', 'Critical', 'Add New Section', './uploads/', '', 'Testing', 'admin', 'sudipa', '0', 0, 10, 0, 0, '2019-05-18 13:57:26'),
	(6, 2, 1, 'Write New Story', 'Trivial', 'Write New Story on css training', './uploads/', '', 'Progress', 'admin', 'sumon', '0', 0, 23, 0, 0, '2017-12-01 17:26:22'),
	(7, 1, 1, 'New epic', 'Trivial', 'New epic', './uploads/', '', 'Progress', 'admin', 'sumon', '0', 4, 13, 0, 0, '2017-12-01 17:27:25'),
	(8, 1, 1, 'New Bug', 'Trivial', 'New Bug change', './uploads/', '', 'Progress', 'admin', 'sumon', '0', 7, 14, 0, 0, '2017-12-01 17:28:40'),
	(9, 1, 1, 'Color Bug', 'Critical', 'Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug Color Bug ', './uploads/', '', 'Testing', 'admin', 'admin', '0', 14, 10, 0, 0, '2019-06-27 08:19:35'),
	(10, 2, 1, 'Add new picture', 'Minor', 'add new picture in list', './uploads/', '', 'Closed', 'admin', 'sumon', '0', 7, 19, 1, 0, '2019-06-29 19:35:25'),
	(11, 2, 1, 'test story', 'Major', 'test story', './uploads/', '', 'Not Started', 'admin', 'sudipa', '0', 14, 5, 0, 0, '2019-06-29 16:18:13'),
	(12, 1, 1, 'new xml file is requirted ', 'Minor', 'new xml file is requirted new xml file is requirted ', './uploads/', '', 'Progress', 'admin', 'sumon', '0', 0, 5, 0, 0, '2019-06-30 16:57:36'),
	(13, 1, 1, 'gdfgfdgfdgfdgfdgfdgfdg', 'Major', 'dfgdfgfdgdfg', './uploads/', '', 'Progress', 'admin', 'sumon', '0', 0, 5, 0, 0, '2019-06-30 16:57:40'),
	(14, 1, 1, 'gfhhhhhhhhhhhhhhhhhh', 'Major', 'gfhhhhhhhhhhhhhhhhhhhhhhhhhghfg fghfh fghgf hgfhgfhgfhfghgf hgfhgfhfghgfhfghgfh fghfghgfhgfhgfhfghfghfdg fdgdfgdfgdffg dfgfdgdgdfgdf gdfgdfg', './uploads/', '', 'Closed', 'admin', 'sudipa', '0', 0, 5, 0, 0, '2019-05-16 09:26:35'),
	(15, 2, 1, 'Sprint10-Radar Issue', 'Major', 'Sprint10-Radar Issue', './uploads/', '', 'Not Started', 'client', 'sumon', '0', 0, 5, 0, 0, '2019-06-30 17:32:19');
/*!40000 ALTER TABLE `ag_issues_txn` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_menu_mst
CREATE TABLE IF NOT EXISTS `ag_menu_mst` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(100) NOT NULL DEFAULT '0',
  `sub_menu_name` varchar(100) NOT NULL DEFAULT '0',
  `sub_menu_flag` int(1) NOT NULL DEFAULT '0' COMMENT '0=no submenu and 1= submenu',
  `menu_url` varchar(1000) NOT NULL DEFAULT '#',
  `icon` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_menu_mst: ~21 rows (approximately)
/*!40000 ALTER TABLE `ag_menu_mst` DISABLE KEYS */;
INSERT INTO `ag_menu_mst` (`menu_id`, `menu_name`, `sub_menu_name`, `sub_menu_flag`, `menu_url`, `icon`) VALUES
	(1, 'DASHBOARD', '0', 0, 'Menus/Dashboard', 'fa fa-th-large'),
	(2, 'CLIENTS', '0', 0, 'Menus/Clients', 'fa fa-group icon'),
	(3, 'AGILE PROJECTS', '0', 0, 'Menus/Projects', 'fa fa-pencil'),
	(4, 'DEFINITION OF READY', '0', 0, 'Menus/Dor', 'fa fa-cog'),
	(5, 'DEFINITION OF DONE', '0', 0, 'Menus/Dod', 'fa fa-cog'),
	(6, 'PRODUCT BACKLOG', '0', 0, 'Menus/ProductBacklog', 'fa fa-reorder'),
	(7, 'RETROSPECTIVE', '0', 0, 'Menus/ViewRetrospective', 'fa fa-calendar-o'),
	(11, 'USERS', '0', 0, 'Menus/Users', 'fa fa-user'),
	(12, 'INVOICES', '0', 0, 'Menus/Invoices', 'fa fa-file'),
	(13, 'PAYMENTS', '0', 0, 'Menus/Payments', 'fa fa-dollar'),
	(14, 'EVENTS', '0', 0, 'Menus/Events', 'fa fa-calendar'),
	(15, 'MESSAGES', '0', 0, 'Menus/Messages', 'fa fa-comment'),
	(16, 'TICKETS', '0', 0, 'Menus/Tickets', 'fa fa-ticket'),
	(17, 'NOTES', '0', 0, 'Menus/Notes', 'fa fa-comments-o'),
	(18, 'EXPENSES', '0', 0, 'Menus/Expenses', 'fa fa-money'),
	(19, 'SETTINGS', 'Settings', 0, 'Menus/Settings', 'fa fa-cog'),
	(21, 'Projects', 'Project Overview', 1, '#', NULL),
	(22, 'Projects', 'Issues', 1, '#', NULL),
	(23, 'Projects', 'Epic', 1, '#', NULL),
	(24, 'Projects', 'Story', 1, '#', NULL),
	(25, 'Projects', 'Sprint', 1, '#', NULL),
	(26, 'Projects', 'Timesheets', 1, '#', NULL),
	(27, 'Projects', 'Reports', 1, '#', NULL);
/*!40000 ALTER TABLE `ag_menu_mst` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_message
CREATE TABLE IF NOT EXISTS `ag_message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_title` varchar(255) NOT NULL,
  `message_descrription` text NOT NULL,
  `message_attachment` varchar(255) NOT NULL,
  `message_send_to` int(11) NOT NULL,
  `message_send_by` int(11) NOT NULL,
  `message_created_by` int(11) NOT NULL,
  `message_status` enum('S','D') NOT NULL COMMENT 'S->Send,D->Deleted',
  `message_view` enum('Y','N') NOT NULL DEFAULT 'N',
  `message_send_ip` varchar(255) NOT NULL,
  `message_send_device` varchar(255) NOT NULL,
  `message_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_message: ~7 rows (approximately)
/*!40000 ALTER TABLE `ag_message` DISABLE KEYS */;
INSERT INTO `ag_message` (`message_id`, `message_title`, `message_descrription`, `message_attachment`, `message_send_to`, `message_send_by`, `message_created_by`, `message_status`, `message_view`, `message_send_ip`, `message_send_device`, `message_created_on`) VALUES
	(1, 'Greatings', 'Hi admin This is Sumon', 'Koala.jpg', 1, 4, 4, 'S', 'Y', '::1', 'Firefox 57.0/Windows 7', '2018-02-22 21:41:12'),
	(2, 'Greatings to sudipa', 'hiii  sudipa', '', 3, 4, 4, 'S', 'N', '::1', 'Firefox 57.0/Windows 7', '2017-11-29 17:36:38'),
	(3, 'Test Message', 'Goood morning admin I am Sudipa', '', 1, 3, 3, 'S', 'Y', '::1', 'Firefox 57.0/Windows 7', '2018-02-22 21:35:45'),
	(4, 'Reply of Greatings', 'Reply to sumon how r u?', '', 0, 1, 1, 'S', 'Y', '::1', 'Firefox 57.0/Windows 7', '2019-01-01 12:51:06'),
	(5, '', 'Hi Sumon what r u doing', '', 3, 1, 1, 'S', 'N', '::1', 'Firefox 57.0/Windows 7', '2017-11-29 22:23:20'),
	(6, 'Reply ofTest Message', 'Hi Sudipa I am Admin', '', 3, 1, 1, 'S', 'N', '::1', 'Firefox 57.0/Windows 7', '2017-11-29 22:25:21'),
	(7, 'Reply ofGreatings', 'Hi sumon good ni8', '', 4, 1, 1, 'S', 'Y', '::1', 'Firefox 57.0/Windows 7', '2019-01-01 12:50:48'),
	(8, 'Reply of Greatings', 'bvvcb\r\nvcb\r\ncvb\r\nvcb\r\nvcbvcbvc\r\nbcvbcvb\r\nvcbvc\r\nb', '', 0, 1, 1, 'S', 'N', '::1', 'Chrome 73.0.3683.103/Windows 7', '2019-04-19 18:11:50');
/*!40000 ALTER TABLE `ag_message` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_notes
CREATE TABLE IF NOT EXISTS `ag_notes` (
  `notes_id` int(11) NOT NULL AUTO_INCREMENT,
  `notes_title` varchar(255) NOT NULL DEFAULT '0',
  `priority` varchar(50) NOT NULL DEFAULT '0',
  `note_description` varchar(500) DEFAULT '0',
  `created_by` int(11) DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`notes_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_notes: ~2 rows (approximately)
/*!40000 ALTER TABLE `ag_notes` DISABLE KEYS */;
INSERT INTO `ag_notes` (`notes_id`, `notes_title`, `priority`, `note_description`, `created_by`, `created_date`) VALUES
	(1, 'This is First Note', 'Minor', 'This is First Note Description', 1, '2017-12-27 12:51:54'),
	(2, 'Test Note', 'Major', ' TeTest Note descriptionTest Note descriptionTest Note ', 1, '2017-12-27 12:52:57');
/*!40000 ALTER TABLE `ag_notes` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_project_files
CREATE TABLE IF NOT EXISTS `ag_project_files` (
  `project_id` int(11) NOT NULL,
  `file_name` varchar(4000) NOT NULL,
  `user_name` varchar(4000) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_project_files: ~2 rows (approximately)
/*!40000 ALTER TABLE `ag_project_files` DISABLE KEYS */;
INSERT INTO `ag_project_files` (`project_id`, `file_name`, `user_name`, `created_date`) VALUES
	(1, 'stories1.txt', '1', '2019-06-23 17:43:19'),
	(1, 'Project.docx', '1', '2019-06-23 18:31:15');
/*!40000 ALTER TABLE `ag_project_files` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_project_mst
CREATE TABLE IF NOT EXISTS `ag_project_mst` (
  `project_id` int(100) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `project_lead` varchar(100) NOT NULL,
  `client_id` int(50) NOT NULL,
  `progress` int(20) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `resources_allocatted` varchar(255) NOT NULL,
  `fixed_rate` int(11) NOT NULL,
  `hourly_rate` int(11) NOT NULL,
  `estimated_hours` int(11) NOT NULL,
  `hours_spent` int(11) NOT NULL,
  `Notes` varchar(4000) NOT NULL,
  `billed_amt` int(50) NOT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_project_mst: ~9 rows (approximately)
/*!40000 ALTER TABLE `ag_project_mst` DISABLE KEYS */;
INSERT INTO `ag_project_mst` (`project_id`, `Name`, `project_lead`, `client_id`, `progress`, `start_date`, `end_date`, `resources_allocatted`, `fixed_rate`, `hourly_rate`, `estimated_hours`, `hours_spent`, `Notes`, `billed_amt`) VALUES
	(1, 'Test Sumon First', '', 1, 0, '0000-00-00', '2017-11-21', '2,3,6', 5, 6, 9, 0, 'Miusov, as a man man of breeding and deilcacy, could not but feel some inwrd qualms, when he reached the Father Superior\'s with Ivan: he felt ashamed of havin lost his temper. He felt that he ought to have disdaimed that despicable wretch, Fyodor Pavlovitch, too much to have been upset by him in Father Zossima\'s cell, and so to have forgotten himself. "Teh monks were not to blame, in any case," he reflceted, on the steps. "And if they\'re decent people here (and the Father Superior, I understand, is a nobleman) why not be friendly and courteous withthem? I won\'t argue, I\'ll fall in with everything, I\'ll win them by politness, and show them that I\'ve nothing to do with that Aesop, thta buffoon, that Pierrot, and have merely been takken in over this affair, just as they have."', 0),
	(2, 'Test Sumon2', '', 1, 0, '0000-00-00', '2017-11-30', '6', 100, 11, 7, 0, 'Test Sumon2 vvvvvv', 0),
	(3, 'Test Sudipa', '', 3, 0, '0000-00-00', '2017-11-30', '9', 400, 15, 10, 0, 'fghjgjvhj', 0),
	(4, 'Test Sudipa', '', 3, 0, '0000-00-00', '2017-11-14', '45', 700, 30, 30, 0, 'dfydud', 0),
	(5, 'Test Sudipa3', '', 3, 0, '0000-00-00', '2017-11-21', '5', 6, 54, 4, 0, 'fhdh', 0),
	(6, 'Test Sumon3', '', 1, 0, '0000-00-00', '2017-11-30', '89', 900, 30, 30, 0, 'dryey', 0),
	(7, 'Admin Project', '', 1, 0, '2017-11-01', '2017-11-28', '6', 500, 25, 7, 0, 'erytuy', 0),
	(8, 'December Project Test', '', 3, 0, '2017-12-28', '2018-01-30', '2,3,6', 1040, 12, 27, 0, 'Test note december test', 0),
	(11, 'Work on january', '', 2, 0, '2018-01-01', '2018-01-25', '1,4,5', 5000, 50, 10, 0, 'test note jan', 0);
/*!40000 ALTER TABLE `ag_project_mst` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_retrospective
CREATE TABLE IF NOT EXISTS `ag_retrospective` (
  `retro_id` int(11) NOT NULL AUTO_INCREMENT,
  `retro_type_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `sprint_id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(4000) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`retro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_retrospective: ~7 rows (approximately)
/*!40000 ALTER TABLE `ag_retrospective` DISABLE KEYS */;
INSERT INTO `ag_retrospective` (`retro_id`, `retro_type_id`, `project_id`, `sprint_id`, `description`, `position`, `insert_time`) VALUES
	(53, 1, 1, 1, '  asdasdasdasd', 7, '2019-06-02 19:23:18'),
	(56, 2, 1, 1, '                          test+ green  +yellow +blue +tre +yt gdg \nkjghkjg  vxcvxcv bvxcbcvb fhfghgfhgfhgfh \ndsgfdfgdfg dsfhshfsf jishnu is a good boy . Rumani is very good girl .jishniu koyyy gfhgfh \n\nghsdifghusdgf\nsdjfjksdhfjkh\nsdfhsdgfg        ', 15, '2019-05-09 08:02:16'),
	(65, 1, 1, 1, '  blue 1 jishnu adasdasd', 4, '2019-06-02 19:23:18'),
	(66, 3, 1, 1, '  red2 blue rumani', 24, '2019-05-05 12:15:14'),
	(67, 3, 1, 1, 'blue2', 23, '2019-05-05 12:15:14'),
	(68, 2, 1, 1, '     green2  jish sadasdasd adasdasd\nasdasdsad\nrtyertret ', 12, '2019-05-09 08:02:16'),
	(69, 1, 1, 1, 'fghfgh', 6, '2019-06-02 19:23:18');
/*!40000 ALTER TABLE `ag_retrospective` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_retrospective_mst
CREATE TABLE IF NOT EXISTS `ag_retrospective_mst` (
  `retro_type_id` int(11) NOT NULL,
  `retro_description` varchar(1000) NOT NULL,
  PRIMARY KEY (`retro_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_retrospective_mst: ~2 rows (approximately)
/*!40000 ALTER TABLE `ag_retrospective_mst` DISABLE KEYS */;
INSERT INTO `ag_retrospective_mst` (`retro_type_id`, `retro_description`) VALUES
	(1, 'Good Points'),
	(2, 'Bad Points'),
	(3, 'Action Items');
/*!40000 ALTER TABLE `ag_retrospective_mst` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_role_menu_mapping
CREATE TABLE IF NOT EXISTS `ag_role_menu_mapping` (
  `user_type_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'Admin',
  KEY `FK__ag_user_type_mst` (`user_type_id`),
  KEY `FK__ag_menu_mst` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_role_menu_mapping: ~70 rows (approximately)
/*!40000 ALTER TABLE `ag_role_menu_mapping` DISABLE KEYS */;
INSERT INTO `ag_role_menu_mapping` (`user_type_id`, `menu_id`, `created_on`, `created_by`) VALUES
	(1, 1, '2015-12-08 08:02:30', 'Admin'),
	(1, 2, '2015-12-08 08:03:00', 'Admin'),
	(1, 3, '2015-12-08 08:03:15', 'Admin'),
	(1, 4, '2015-12-08 08:03:32', 'Admin'),
	(1, 5, '2015-12-08 08:03:51', 'Admin'),
	(1, 6, '2015-12-08 08:04:02', 'Admin'),
	(1, 7, '2015-12-08 08:04:19', 'Admin'),
	(1, 8, '2015-12-08 08:04:30', 'Admin'),
	(1, 9, '2015-12-08 08:04:39', 'Admin'),
	(1, 10, '2015-12-08 08:04:50', 'Admin'),
	(1, 11, '2015-12-08 08:05:11', 'Admin'),
	(1, 12, '2015-12-08 08:05:27', 'Admin'),
	(1, 13, '2015-12-08 08:05:37', 'Admin'),
	(1, 15, '2015-12-08 08:06:06', 'Admin'),
	(2, 3, '2016-08-08 09:34:34', 'Admin'),
	(3, 1, '2016-08-14 15:53:59', 'Admin'),
	(3, 3, '2016-08-14 15:54:44', 'Admin'),
	(3, 13, '2016-08-14 16:01:17', 'Admin'),
	(1, 1, '2015-12-08 08:02:30', 'Admin'),
	(1, 2, '2015-12-08 08:03:00', 'Admin'),
	(1, 3, '2015-12-08 08:03:15', 'Admin'),
	(1, 4, '2015-12-08 08:03:32', 'Admin'),
	(1, 5, '2015-12-08 08:03:51', 'Admin'),
	(1, 6, '2015-12-08 08:04:02', 'Admin'),
	(1, 7, '2015-12-08 08:04:19', 'Admin'),
	(1, 8, '2015-12-08 08:04:30', 'Admin'),
	(1, 9, '2015-12-08 08:04:39', 'Admin'),
	(1, 10, '2015-12-08 08:04:50', 'Admin'),
	(1, 11, '2015-12-08 08:05:11', 'Admin'),
	(1, 12, '2015-12-08 08:05:27', 'Admin'),
	(1, 13, '2015-12-08 08:05:37', 'Admin'),
	(1, 15, '2015-12-08 08:06:06', 'Admin'),
	(2, 3, '2016-08-08 09:34:34', 'Admin'),
	(3, 1, '2016-08-14 15:53:59', 'Admin'),
	(3, 3, '2016-08-14 15:54:44', 'Admin'),
	(3, 13, '2016-08-14 16:01:17', 'Admin'),
	(1, 1, '2015-12-08 08:02:30', 'Admin'),
	(1, 2, '2015-12-08 08:03:00', 'Admin'),
	(1, 3, '2015-12-08 08:03:15', 'Admin'),
	(1, 4, '2015-12-08 08:03:32', 'Admin'),
	(1, 5, '2015-12-08 08:03:51', 'Admin'),
	(1, 6, '2015-12-08 08:04:02', 'Admin'),
	(1, 7, '2015-12-08 08:04:19', 'Admin'),
	(1, 8, '2015-12-08 08:04:30', 'Admin'),
	(1, 9, '2015-12-08 08:04:39', 'Admin'),
	(1, 10, '2015-12-08 08:04:50', 'Admin'),
	(1, 11, '2015-12-08 08:05:11', 'Admin'),
	(1, 12, '2015-12-08 08:05:27', 'Admin'),
	(1, 13, '2015-12-08 08:05:37', 'Admin'),
	(1, 15, '2015-12-08 08:06:06', 'Admin'),
	(2, 3, '2016-08-08 09:34:34', 'Admin'),
	(3, 1, '2016-08-14 15:53:59', 'Admin'),
	(3, 3, '2016-08-14 15:54:44', 'Admin'),
	(3, 13, '2016-08-14 16:01:17', 'Admin'),
	(1, 14, '2017-11-09 09:20:22', 'Admin'),
	(1, 16, '2017-11-09 10:17:43', 'Admin'),
	(2, 14, '2017-11-17 17:53:40', 'Admin'),
	(3, 14, '2017-11-17 17:53:40', 'Admin'),
	(2, 15, '2017-11-17 17:54:40', 'Admin'),
	(3, 15, '2017-11-17 17:54:40', 'Admin'),
	(1, 16, '2017-12-26 14:57:55', 'Admin'),
	(1, 17, '2017-12-26 14:57:55', 'Admin'),
	(1, 18, '2017-12-26 14:59:22', 'Admin'),
	(2, 16, '2017-12-26 15:01:10', 'Admin'),
	(2, 17, '2017-12-26 15:01:10', 'Admin'),
	(3, 17, '2017-12-26 15:01:56', 'Admin'),
	(3, 17, '2017-12-26 15:01:56', 'Admin'),
	(1, 19, '2018-02-13 15:25:32', 'Admin'),
	(3, 12, '2018-02-17 17:01:20', 'Admin'),
	(3, 16, '2018-02-20 22:07:33', 'Admin'),
	(1, 20, '2018-09-24 14:10:48', 'Admin');
/*!40000 ALTER TABLE `ag_role_menu_mapping` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_sprint
CREATE TABLE IF NOT EXISTS `ag_sprint` (
  `sprint_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `sprint_title` varchar(500) DEFAULT '0',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sprint_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_sprint: ~14 rows (approximately)
/*!40000 ALTER TABLE `ag_sprint` DISABLE KEYS */;
INSERT INTO `ag_sprint` (`sprint_id`, `project_id`, `sprint_title`, `start_date`, `end_date`, `created_by`, `created_on`) VALUES
	(1, 0, 'First Sprint', '2017-11-08', '2017-11-28', 'admin', '2017-11-08 23:53:29'),
	(2, 0, 'Design Modification', '2017-11-01', '2017-11-02', 'admin', '2017-11-25 11:53:41'),
	(3, 0, 'Pls change heading', '2017-11-29', '2017-11-30', 'admin', '2017-11-25 12:00:09'),
	(4, 0, 'Test Sprint Sumon', '2017-12-04', '2017-12-08', 'admin', '2017-12-04 11:40:30'),
	(5, 0, 'Test Sprint', '2017-12-17', '2017-12-23', 'admin', '2017-12-21 17:15:57'),
	(6, 0, 'Test drag Drop Sprint', '2017-12-24', '2017-12-30', 'admin', '2017-12-22 17:41:32'),
	(7, 0, 'Test Sprint', '2018-09-16', '2018-09-30', 'admin', '2018-09-16 00:28:32'),
	(8, 0, 'sample', '2018-10-01', '2018-10-05', 'admin', '2018-09-16 00:37:20'),
	(9, 0, 'Design Modification', '2019-05-08', '2019-05-19', 'admin', '2019-05-05 00:39:33'),
	(10, 1, 'Sprint10', '2019-06-13', '2019-06-13', 'admin', '2019-06-13 08:26:51'),
	(11, 1, 'sprint-11 Design Modification', '2019-06-14', '2019-06-14', 'admin', '2019-06-14 09:38:41'),
	(12, 1, 'sprint-12', '2019-06-15', '2019-06-15', 'admin', '2019-06-15 17:48:07'),
	(13, 1, 'sprint-13', '2019-06-16', '2019-06-15', 'admin', '2019-06-17 09:55:28'),
	(14, 1, 'sprint14', '2019-06-18', '2019-06-18', 'admin', '2019-06-18 08:14:56');
/*!40000 ALTER TABLE `ag_sprint` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_status
CREATE TABLE IF NOT EXISTS `ag_status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(255) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_status: ~3 rows (approximately)
/*!40000 ALTER TABLE `ag_status` DISABLE KEYS */;
INSERT INTO `ag_status` (`status_id`, `status_name`) VALUES
	(1, 'Open'),
	(2, 'In Progress'),
	(3, 'Completed');
/*!40000 ALTER TABLE `ag_status` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_tax
CREATE TABLE IF NOT EXISTS `ag_tax` (
  `tax_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_name` varchar(255) NOT NULL,
  `tax_percentage` double(10,2) NOT NULL,
  PRIMARY KEY (`tax_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_tax: ~4 rows (approximately)
/*!40000 ALTER TABLE `ag_tax` DISABLE KEYS */;
INSERT INTO `ag_tax` (`tax_id`, `tax_name`, `tax_percentage`) VALUES
	(1, 'CGST', 5.00),
	(2, 'GGST', 6.00),
	(3, 'VAT', 8.00),
	(4, 'KGST', 12.00);
/*!40000 ALTER TABLE `ag_tax` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_tickets
CREATE TABLE IF NOT EXISTS `ag_tickets` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_type_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `ticket_title` varchar(255) NOT NULL DEFAULT '0',
  `priority` varchar(50) NOT NULL DEFAULT '0',
  `description` varchar(500) DEFAULT '0',
  `estimated_hours` int(11) NOT NULL,
  `reported_by` int(11) DEFAULT '0',
  `assigned_to` int(11) DEFAULT '0',
  `status_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ticket_id`),
  KEY `FK__ag_issues_mst` (`ticket_type_id`),
  KEY `FK__ag_project_mst` (`project_id`),
  KEY `FK__ag_users` (`reported_by`),
  KEY `FK__ag_users_2` (`assigned_to`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_tickets: ~4 rows (approximately)
/*!40000 ALTER TABLE `ag_tickets` DISABLE KEYS */;
INSERT INTO `ag_tickets` (`ticket_id`, `ticket_type_id`, `project_id`, `ticket_title`, `priority`, `description`, `estimated_hours`, `reported_by`, `assigned_to`, `status_id`, `created_by`, `created_date`) VALUES
	(2, 4, 0, 'Test Ticket1', 'Critical', 'Test Ticket1Test Ticket1Test Ticket1', 10, 1, 4, 2, 1, '2017-12-28 17:15:00'),
	(3, 4, 0, 'Test ticket', 'Critical', 'Test ticket Description', 3, 3, 4, 2, 1, '2017-12-29 10:39:29'),
	(4, 4, 0, 'Test tittle 4', 'Critical', 'Test tittle 4Test tittle 4', 7, 2, 5, 3, 1, '2017-12-29 15:09:35'),
	(5, 4, 0, 'Test tittle 4', 'Critical', 'Test tittle 4Test tittle 4', 7, 2, 5, 1, 1, '2017-12-29 15:09:35');
/*!40000 ALTER TABLE `ag_tickets` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_timesheet_details
CREATE TABLE IF NOT EXISTS `ag_timesheet_details` (
  `timesheet_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `logged_date` date DEFAULT NULL,
  `logged_hours` int(11) DEFAULT NULL,
  `logged_user` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `issue_id` int(11) DEFAULT NULL,
  `sprint_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`timesheet_id`),
  KEY `FK_ag_timesheet_details_ag_project_mst` (`project_id`),
  KEY `FK_ag_timesheet_details_ag_users` (`logged_user`),
  KEY `FK_ag_timesheet_details_ag_issues_txn` (`issue_id`),
  CONSTRAINT `FK_ag_timesheet_details_ag_issues_txn` FOREIGN KEY (`issue_id`) REFERENCES `ag_issues_txn` (`issue_id`),
  CONSTRAINT `FK_ag_timesheet_details_ag_project_mst` FOREIGN KEY (`project_id`) REFERENCES `ag_project_mst` (`project_id`),
  CONSTRAINT `FK_ag_timesheet_details_ag_users` FOREIGN KEY (`logged_user`) REFERENCES `ag_users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_timesheet_details: ~2 rows (approximately)
/*!40000 ALTER TABLE `ag_timesheet_details` DISABLE KEYS */;
INSERT INTO `ag_timesheet_details` (`timesheet_id`, `project_id`, `logged_date`, `logged_hours`, `logged_user`, `description`, `issue_id`, `sprint_id`) VALUES
	(3, 1, '2017-12-18', 0, 'admin', 'ghkg', 10, 5),
	(4, 1, '2017-12-19', 1, 'admin', 'wrwrwr', 10, 0);
/*!40000 ALTER TABLE `ag_timesheet_details` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_users
CREATE TABLE IF NOT EXISTS `ag_users` (
  `username` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_role` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_type_id` int(11) NOT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `organization` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `filename` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`username`),
  KEY `FK_ag_users_ag_user_type_mst` (`user_type_id`),
  CONSTRAINT `FK_ag_users_ag_user_type_mst` FOREIGN KEY (`user_type_id`) REFERENCES `ag_user_type_mst` (`user_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_users: ~6 rows (approximately)
/*!40000 ALTER TABLE `ag_users` DISABLE KEYS */;
INSERT INTO `ag_users` (`username`, `user_id`, `password`, `user_role`, `created_date`, `user_type_id`, `fullname`, `organization`, `email`, `client_id`, `filename`) VALUES
	('admin', 1, 'admin', 'admin', '2016-09-22 02:33:02', 1, 'John Doe', 'AgileTech', 'info@agiletech.com', NULL, 'a4.jpg'),
	('client', 2, 'client', 'Client', '2016-09-22 02:33:02', 3, 'RSA', 'Roins Financial', 'info@agiletech.com', 1, 'a1.jpg'),
	('sudipa', 3, 'sudipa', 'Client', '2017-11-06 14:35:53', 3, 'Sudipa Sarkar', 'SKR Pvt. Ltd.', 'sudipa@gmail.com', 3, 'a311.jpg'),
	('sumon', 4, 'sumon', 'Client', '2017-11-06 14:36:56', 3, 'Sumon Roy', 'SR Indusrties pvt. Ltd.', 'sumon@gmail.com', 1, 'a1.jpg'),
	('user', 5, 'user', 'User', '2016-09-22 02:33:02', 2, 'Elliot', 'Agile Tech', 'info@agiletech.com', 0, 'm1.jpg'),
	('user1', 6, 'user', 'User', '2016-09-22 02:33:02', 2, 'Carm Legg', 'lbsk', 'info@agiletech.com', NULL, '');
/*!40000 ALTER TABLE `ag_users` ENABLE KEYS */;

-- Dumping structure for table scrum.ag_user_type_mst
CREATE TABLE IF NOT EXISTS `ag_user_type_mst` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_desc` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table scrum.ag_user_type_mst: ~3 rows (approximately)
/*!40000 ALTER TABLE `ag_user_type_mst` DISABLE KEYS */;
INSERT INTO `ag_user_type_mst` (`user_type_id`, `user_type_desc`) VALUES
	(1, 'admin'),
	(2, 'users'),
	(3, 'client');
/*!40000 ALTER TABLE `ag_user_type_mst` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
